// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MyTDS/FuncLibrary/Types.h"
#include "UObject/Interface.h"
#include "MyTDS/StateEffects/MyTDS_StateEffect.h"
#include "MyTDS_IGameActor.generated.h"

// This class does not need to be modified.
//В Редакторе - класс Interface.
//Это сам интрефейс. Запомни, Важно!  Родитель  - это U - класс, а методы I классы
UINTERFACE(MinimalAPI)
class UMyTDS_IGameActor : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
//А это методы интерфейса
class MYTDS_API IMyTDS_IGameActor
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	// 0)
	//Чтобы нацепить интерфейс -
	//Переходим туда, где нам нужен этот интерфейс в ЧАР! и инклудим: #include "MyTDS/Interface/MyTDS_IGameActor.h"
	// и в после всех инклюдов хедера .h ЧАРА добавляем наш класс интерфейса:
	// UCLASS(Blueprintable)
	// class AMyTDSCharacter : public ACharacter, public IMyTDS_IGameActor
	
	// 1)
	// UFUNCTION(BlueprintCallable,BlueprintImplementableEvent,Category = "Event")
	// 	void AviableForEffectsOnlyBP();
	// - Этот метод BlueprintImplementableEvent существует только для блюпринтов: Использование функций
	// BlueprintImplementableEventне может быть переопределено в C++, но может быть переопределено
	// в любом классе Blueprint, который реализует или наследует ваш интерфейс.


	// 2)
	// UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Event")
	// 	bool AviableForEffectsAll();
	
	//- BlueprintNativeEvent - может вызываться как в блюпритах, так и в с++
	//- Если есть описание UFUNCTION - не требует реализации в СРР
	//- нужно сразу перейти туда, куда мы  вешаем интрефейс и там в описании h добавить override, если хотим переопределить
	// - Можно добавить override - если будем переписывать
	//- создать реализацию в срр с const в конце
	// bool AMyTDSCharacter::AviableForEffectsAll_implementation() const


	//Особенности: Из-за того, что мы прописали макросы UFUNCTION - нам не надо создавать функции в cpp



	// 3)
	//Следующий метод  -только для плюсов! объявляется без UFUNCTION!!! Поэтому обязательно virtual
	//Только для этого виртуального метода нужно тело функции в срр
		// virtual bool AviableForEffectsOnlyCPP();
	// - не видна в срр
	//в cpp обязательно прописать implementation() override:  AviableForEffectsOnlyCPP_implementation() override


	//????? Кажется implementation прописывается  -если мы берем ф-ии родителя, а не полностью оверрайдим

	//Эти ф-ии только в плюсах
	//Вытаскиваем тип поверхности 
		virtual EPhysicalSurface GetSurfuceType();

	//Массив эффектов -возвращает все навешанные эффекты - вызываем в чаре и AMyTDS_EnvironmentStructure
		virtual TArray<UMyTDS_StateEffect*> GetAllCurrentEffects();

	//функции для управления эффектами - добавление и удаление
	//делаем использование в блюпринтах: добавили UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	//и убрали virtual
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
    	 void RemoveEffect(UMyTDS_StateEffect* RemoveEffect);
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
    	 void AddEffect(UMyTDS_StateEffect* newEffect);

	//Эти ф-ии в плюсах и в БП - у нас только в БП в Чаре - DropWeaponToWorld
	//DropAmmoToWorld - не используется пока

	//inv
	//Наши разрушанмые объекты, либо враги, которые должны что-то дропать будуть это дропать через интерфейс
	//BlueprintNativeEvent - значит может вызываться как в блюпритах, так и в с++
	///Переходим туда, где нам нужен этот интерфейс -  в UMyTDSInventoryComponent в DropWeaponbByIndex

	//BlueprintNativeEvent - мы не исполюзум в плюсах - только в блюпринтах
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void DropWeaponToWorld(FDropItem DropItemInfo);
	//Не используется пока
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void DropAmmoToWorld(EWeaponType TypeAmmo, int32 Cout);
};
