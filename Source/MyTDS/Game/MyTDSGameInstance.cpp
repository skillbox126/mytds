// Fill out your copyright notice in the Description page of Project Settings.


#include "MyTDSGameInstance.h"

bool UMyTDSGameInstance::GetWeaponInfoByName(FName NameWeapon, FWeaponInfo& OutInfo)
{
	bool bIsFind = false;
	FWeaponInfo* WeaponInfoRow;

	if (WeaponInfoTable)//если таблица существует
	{
		//ищем строку с именем NameWeapon без контекста и игнорируем ворнинги
		WeaponInfoRow = WeaponInfoTable->FindRow<FWeaponInfo>(NameWeapon, "", false);
		//работа с таблицей - ищем в таблице по строкам. NameWeapon -возвращает ссылку, хотя по идее -это структура.
		//такой синтаксис - к сожалению)) - поэтому надо переписать выходы нашей функции: FWeaponInfo& OutInfo
		// bool - пусть просто выкидывает
		if (WeaponInfoRow)
		{
			bIsFind = true; //если нашли нужную нам строку
			OutInfo = *WeaponInfoRow;
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("UTPSGameInstance::GetWeaponInfoByName - WeaponTable -NULL"));
	}
		
	return bIsFind;
}

////получаем инфу об  оружии по имени -  ПЕРЕБИРАЕМ СТРОКИ В МАССИВЕ И ЗАТЕМ ПЕРЕБИРАИМ ИНФУ В КАЖДОЙ СТРОКЕ
///И НАХОДИМ СООТВЕТСТВУЮЩЕЕ ИМЯ ОРУЖИЯ. КОРОЧЕ  - ЗДЕСЬ ИЩЕМ В ТАБЛИЦЕ DT_Dropiteminfo
///само оружие Rifle_v1 из структуры WeaponInfo
//OutInfo -это структура с мешами и инфой про оружие из слота
bool UMyTDSGameInstance::GetDropItemInfoByWeaponName(FName NameItem, FDropItem& OutInfo)
{
	bool bIsFind = false;

	if (DropItemInfoTable)//если таблица существует
	{
		//Объявляем  переменную типа FDropItem* - адрес ячейки памяти строки массива сбрасываемого оружия
		FDropItem* DropItemInfoRow;
		//возвращает в массив RowNames все строки из нашей таблицы
		//RowNames - массив из строк- каждая строка - это структура FDropItem для 1 оружия
		TArray<FName>RowNames = DropItemInfoTable->GetRowNames();

		int8 i = 0;
		//Берем все строки  RowNames
		while (i < RowNames.Num() && !bIsFind)
		{
			//перебираем каждую строку. Т.к. мы работаем с таблицей -нам возвращается найденный адрес ячейки памяти
			DropItemInfoRow = DropItemInfoTable->FindRow<FDropItem>(RowNames[i], "");
			if (DropItemInfoRow->WeaponInfo.NameItem == NameItem)
			{
				//Разименование!
				OutInfo = (*DropItemInfoRow); //по этому адресу содержится вся инфа из этой строки==структуры
				bIsFind = true;
			}
			i++;
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("UTPSGameInstance::GetDropItemInfoByName - DropItemInfoRow -NULL"));
	}
		
	return bIsFind;
}

//получаем инфу об  оружии по имени -  ПЕРЕБИРАЕМ ТОЛЬКО СТРОКИ В МАССИВЕ ИЗ СТРОК  - ПОЛУЧАЕМ АДРЕС СТРОКИ С ИНФОЙ
//А здесь, короче в таблице DT_Dropiteminfo ищем по началу строки -по названию дропа DropRifle_v1
bool UMyTDSGameInstance::GetDropItemInfoByName(FName NameItem, FDropItem& OutInfo)
{
	bool bIsFind = false;
	FDropItem* DropItemInfoRow;
		//возвращает в массив RowNames все строки из нашей таблицы
		//RowNames - массив из строк- каждая строка - это структура FDropItem для 1 оружия
	if (DropItemInfoTable)//если таблица существует
	{
		//перебираем каждую строку. Т.к. мы работаем с таблицей -нам возвращается найденный адрес ячейки памяти
		DropItemInfoRow = DropItemInfoTable->FindRow<FDropItem>(NameItem, "", false);
		if (DropItemInfoRow)
		{
			OutInfo = *DropItemInfoRow; //по этому адресу содержится вся инфа из этой строки==структуры об оружии
			bIsFind = true;
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("UTPSGameInstance::GetDropItemInfoByName - DropItemInfoRow -NULL"));
	}
		
	return bIsFind;
}