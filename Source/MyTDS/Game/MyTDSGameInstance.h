// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "MyTDS/FuncLibrary/Types.h"
#include "Engine/DataTable.h"
#include "MyTDS/Weapons/WeaponDefault.h"
#include "MyTDSGameInstance.generated.h"

UCLASS()
class MYTDS_API UMyTDSGameInstance : public UGameInstance
{
	GENERATED_BODY()
	
public:
	//table
	//создаем BP_GameInstance от нашего класса и внем уже наша таблица
	//продключаем BP_GameInstance в ProjetailsSetting чтобы он был доступен на каждом уровне
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = " WeaponSetting ") //создаем таблицу в блюпринте
	UDataTable* WeaponInfoTable = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = " WeaponSetting ") //создаем таблицу в блюпринте
	UDataTable* DropItemInfoTable = nullptr;


	UFUNCTION(BlueprintCallable)
	bool GetWeaponInfoByName(FName NameWeapon, FWeaponInfo& OutInfo);

	UFUNCTION(BlueprintCallable)
	bool GetDropItemInfoByWeaponName(FName NameItem, FDropItem& OutInfo);
	
	UFUNCTION(BlueprintCallable)
	bool GetDropItemInfoByName(FName NameItem, FDropItem& OutInfo);
};
