// Fill out your copyright notice in the Description page of Project Settings.


#include "MyTDS_EnvironmentStructure.h"
#include "Materials/MaterialInterface.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Net/UnrealNetwork.h"
#include "Engine/ActorChannel.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
AMyTDS_EnvironmentStructure::AMyTDS_EnvironmentStructure()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//SetReplicates(true);
	bReplicates = true;
}

// Called when the game starts or when spawned
void AMyTDS_EnvironmentStructure::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AMyTDS_EnvironmentStructure::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

//Получаем тип материала поверхности через получение физ материала у меша
EPhysicalSurface AMyTDS_EnvironmentStructure::GetSurfuceType()
{
	EPhysicalSurface Result = EPhysicalSurface::SurfaceType_Default; //дефолтный - никокой!
	//GetComponentByClass возвращает UActorComponent - поэтому нужен Cast
	UStaticMeshComponent* myMesh = Cast<UStaticMeshComponent>(GetComponentByClass(UStaticMeshComponent::StaticClass()));
	if (myMesh)
	{
		UMaterialInterface* myMaterial = myMesh->GetMaterial(0);// 0-й - поверхность
		if (myMaterial)
		{
			Result = myMaterial->GetPhysicalMaterial()->SurfaceType;
		}
	}
	return Result; //Поучаем тип материала 
}



TArray<UMyTDS_StateEffect*> AMyTDS_EnvironmentStructure::GetAllCurrentEffects()
{
	return Effects;
} 

void AMyTDS_EnvironmentStructure::RemoveEffect_Implementation(UMyTDS_StateEffect* RemoveEffect)
{
	Effects.Remove(RemoveEffect);
	if (!RemoveEffect->bIsAutoDestroyParticleEffect)//если не бесконечный -внутрь ф-ии
		{
		//Поэтому вызываем SwitchEffect 
		SwitchEffect(RemoveEffect, false);
		//Это происходит только на сервере. И дальше, поскольку знаем, что RepNotify работает только на клиенте
		//Если мы поменяем эту переменную на новую:
		EffectRemove = RemoveEffect; 
		//соответственно отработает нам нужный RepNotify
		}
}

void AMyTDS_EnvironmentStructure::AddEffect_Implementation(UMyTDS_StateEffect* newEffect)
{
	//Добавляем ээфект в массив -  их будет больше одного c каждым выстрелом
	Effects.Add(newEffect);
	//делаем для тех эффектов -которые не бесконечные, (как огонь!) для них bIsAutoDestroyParticleEffect = true
	if (!newEffect->bIsAutoDestroyParticleEffect)
	{
		//старая логика для огня
		//Поэтому вызываем SwitchEffect
		SwitchEffect(newEffect, true); 
		//Это происходит только на сервере. И дальше, поскольку знаем, что RepNotify работает только на клиенте
		//Если мы поменяем эту переменную на новую:
		//В момент вызова этой переменной запускается RepNotify ф-я EffectAdd_OnRep() на клиенте
		//Самое главное волшебство:
		EffectAdd = newEffect;
	}
	else //для небесконечных эффектов
	{
		if (newEffect->ParticleEffect)
		{
			ExecuteEffectAdded_OnServer(newEffect->ParticleEffect);
		}
	}
}

void AMyTDS_EnvironmentStructure::EffectAdd_OnRep()
{
	//Мы уже знаем, что в RepNotify EffectAdd корректно появлась, обновилась и эта функция отработает только тогда,
	//когда на клиенте эта EffectAdd поменяется
	if (EffectAdd)
	{
		SwitchEffect(EffectAdd, true);
	}
}

void AMyTDS_EnvironmentStructure::EffectRemove_OnRep()
{
	if (EffectRemove)
	{
		SwitchEffect(EffectRemove, false);
	}
}


void AMyTDS_EnvironmentStructure::ExecuteEffectAdded_OnServer_Implementation(UParticleSystem* ExecuteFX)
{
	//1 вызов -какой эффект надо заспаунить
	ExecuteEffectAdded_Multicast(ExecuteFX);
}

void AMyTDS_EnvironmentStructure::ExecuteEffectAdded_Multicast_Implementation(UParticleSystem* ExecuteFX)
{
	//а на клиенте определяем - какой объект итд
	UTypes::ExecuteEffectAdded(ExecuteFX, this, OffSetEffect, NAME_None);
}

void AMyTDS_EnvironmentStructure::SwitchEffect(UMyTDS_StateEffect* Effect, bool bIsAdd)
{
	if (bIsAdd)
	{
		if (Effect && Effect->ParticleEffect)
		{
			FName NameBoneToAttached = NAME_None; //прописываем -куда мы хотим прицепить эффект
			FVector Loc = OffSetEffect; //и локацию
			USceneComponent* mySceneComponent = GetRootComponent();
			if (mySceneComponent)
			{
				//спавним
				UParticleSystemComponent* NewParticleSystem = UGameplayStatics::SpawnEmitterAttached
					(
					Effect->ParticleEffect,
					mySceneComponent,
					NameBoneToAttached,
					Loc,
					FRotator::ZeroRotator,
					EAttachLocation::SnapToTarget,
					false
					);
				//Добавляем заспавненный UParticleSystemComponent в массив компонентов
				ParticleSystemEffects.Add(NewParticleSystem);
			}
		}
	}
	else //теперь удаляем эффекты. Если бы был спец по эффектам, который был сделал эффекты по времени,
		//это бы вообще не было нужно, но, т.к. наш огонь горит постоянно, эффект надо удалять.
		//Для этого ввели bIsAdd
		{
		if (Effect && Effect->ParticleEffect)
		{
			int32 i = 0;
			bool bIsFind = false;
			if (ParticleSystemEffects.Num() > 0) 
			{
				//пробегаемся по массиву партиклов-систем-компонентов 
				while (i < ParticleSystemEffects.Num() && !bIsFind)
				{
					//Когда мы назначаем какой-то эффект мы как раз назаначаем переменную Template
					if (ParticleSystemEffects[i] && ParticleSystemEffects[i]->Template
							&& Effect->ParticleEffect && Effect->ParticleEffect == ParticleSystemEffects[i]->Template)
					{
						bIsFind = true;
						ParticleSystemEffects[i]->DeactivateSystem();
						ParticleSystemEffects[i]->DestroyComponent();
						ParticleSystemEffects.RemoveAt(i);
					}
					i++;
				}
			}			
		}
		}
}

bool AMyTDS_EnvironmentStructure::ReplicateSubobjects(UActorChannel* Channel, FOutBunch* Bunch, FReplicationFlags* RepFlags)
{
	//Wrote - это битовое сравнение
	bool Wrote = Super::ReplicateSubobjects(Channel,Bunch,RepFlags);

	for (int32 i = 0; i < Effects.Num(); i++)
	{
		if (Effects[i])
		{
			Wrote |= Channel->ReplicateSubobject(Effects[i], *Bunch, *RepFlags);
		}
	}
	return Wrote;
}

//Для UPROPERTY(Replicated)
void AMyTDS_EnvironmentStructure::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	//Макросы
	//Говорят о том что (например) в нашем объекте в классе AMyTDSCharacter - MovementState должна быть реплицируемой
	DOREPLIFETIME(AMyTDS_EnvironmentStructure, Effects);
	DOREPLIFETIME(AMyTDS_EnvironmentStructure, EffectAdd);
	DOREPLIFETIME(AMyTDS_EnvironmentStructure, EffectRemove);
}

// bool AMyTDS_EnvironmentStructure::AviableForEffects_Implementation()
// {
// 	UE_LOG(LogTemp, Warning, TEXT("AMyTDS_EnvironmentStructure::AviableForEffects_Implementation"));
// 	return true;
// }

// bool AMyTDS_EnvironmentStructure::AviableForEffectsOnlyCPP()
// {
// 	UE_LOG(LogTemp, Warning, TEXT("AMyTDS_EnvironmentStructure::AviableForEffectsOnlyCPP()"));
// 	return true;
// }

