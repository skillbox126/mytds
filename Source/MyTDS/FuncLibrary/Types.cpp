// Fill out your copyright notice in the Description page of Project Settings.
#include "Types.h"
#include "MyTDS/MyTDS.h"
#include "MyTDS/Interface/MyTDS_IGameActor.h"
#include "Kismet/GameplayStatics.h"
#include "MyTDS/Character/MyTDSCharacter.h"

//TakeEffectActor - тот Актор к оторому добавляем эффект  - c которым был хит - (Hit.GetActor()),
//AddEffectClass (ProjectileInfo.Effect) - сам блюпринт эффекта и SurfaceType - поверхность для эффекта
//SurfaceType_Default - с этими поверхностями ни чего неделаем
void UTypes::AddEffectBySurfaceType(AActor* TakeEffectActor, FName NameBoneHit, TSubclassOf<UMyTDS_StateEffect> AddEffectClass,
	EPhysicalSurface SurfaceType)
{
	if (SurfaceType != EPhysicalSurface::SurfaceType_Default && TakeEffectActor && AddEffectClass)
    	{
			// Ghbdtltybt rkfccf 'aatrnf r njve rkfcce^ r rjnjhjve ghbktgbv 'aatrn  -,thtv ltajknysq rkfcc!!!!!! yt РШе
    		UMyTDS_StateEffect* myEffect = Cast<UMyTDS_StateEffect>(AddEffectClass->GetDefaultObject());
    		if (myEffect)
    		{
    			bool bIsHavePossibleSurface = false;
    			int8 i = 0;
    			while (i < myEffect->PossibleInteractSurface.Num() && !bIsHavePossibleSurface)
    			{
    				if (myEffect->PossibleInteractSurface[i] == SurfaceType)
    				{			
    					bIsHavePossibleSurface = true; //если нашли - меняем: имеем возможную поверхность 
    					bool bIsCanAddEffect = false; //если нашли - меняем: можем добавить эфффект или нет
    					if (!myEffect->bIsStakable)//проверка на стаковость
    					{
    						int8 j = 0;
    						//Объявили переменную для блюпринта стейтэффекта
    						TArray<UMyTDS_StateEffect*> CurrentEffects;
    						//Запомни, Важно!  Сам  интерфес - это U - класс, а его методы I классы - нам нужен метод!!!
    						//не UMyTDS_IGameActor а IMyTDS_IGameActor!
    						//Делаем каст интерфейса на того с кем взаимодействуем
    						IMyTDS_IGameActor* myInterface = Cast<IMyTDS_IGameActor>(TakeEffectActor);
    						if (myInterface) //если интерфейс доступен получаем массив эффектов
    						{
    							CurrentEffects = myInterface->GetAllCurrentEffects();
    						}
    //при каждом выстреле добавляется ээфект - надо от этого избавиться -если эффекты одинаковые!
    						if (CurrentEffects.Num() > 0)
    						{
    							while (j < CurrentEffects.Num() && !bIsCanAddEffect)
    							{
    								//Если это эффект != нашему AddEffectClass (например есть огонь, но нет яда или дождя)
    								if (CurrentEffects[j]->GetClass() != AddEffectClass)
    								{
    									bIsCanAddEffect = true;
    								}
    								j++;
    							}
    						}
    						else
    						{
    							bIsCanAddEffect = true;
    						}
    						
    					}
    					else
    					{
    						bIsCanAddEffect = true;
    					}
    
    					if (bIsCanAddEffect)
    					{
    						//Прежде чем создавать объект  -мы будем его проверять -для этого используем интерфейс
    						//он (эффет) будет принадлежать Hit.GetActor() и назовем его Effect - но есть трабла
    						//Если уберем FName ("Effect") - т.е. каждый раз имя будет уникальным -то все починится
    						//Самое главное -для чего все делалось -создаем эффект!
    						UMyTDS_StateEffect* NewEffect = NewObject<UMyTDS_StateEffect>(TakeEffectActor, AddEffectClass);
    						if (NewEffect)
    						{
    							NewEffect->InitObject(TakeEffectActor, NameBoneHit);
    						}
    					}
    					
    				}
    				i++;
    			}
    		}
    
    	}
}

void UTypes::ExecuteEffectAdded(UParticleSystem* ExecuteFX, AActor* target, FVector offset, FName Socket)
{
	if (target)
	{
		FName SocketToAttached = Socket;
		FVector Loc = offset;
		ACharacter* myCharacter = Cast<AMyTDSCharacter>(target);
		if (myCharacter && myCharacter->GetMesh()) //если Чар - спауним  к нему
		{
			UGameplayStatics::SpawnEmitterAttached(
				ExecuteFX,
				myCharacter->GetMesh(),
				SocketToAttached, Loc, FRotator::ZeroRotator,
				EAttachLocation::SnapToTarget,
				false);
		}
		else//если не чар -то к рутовуму компоненту с оффсетом
		{
			if (target->GetRootComponent())
			{
				UGameplayStatics::SpawnEmitterAttached(
					ExecuteFX,
					target->GetRootComponent(),
					SocketToAttached, Loc,
					FRotator::ZeroRotator,
					EAttachLocation::SnapToTarget,
					false);
			}			
		}
	}	
}
