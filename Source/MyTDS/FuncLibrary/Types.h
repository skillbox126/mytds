// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "Engine/DataTable.h"
#include "MyTDS/StateEffects/MyTDS_StateEffect.h"
#include "Types.generated.h"

UENUM(BlueprintType)
enum class EMovementState : uint8
{
	Aim_State UMETA(DisplayName = "Aim State"),
	AimWalk_State UMETA(DisplayName = "AimWalk State"),
	Walk_State UMETA(DisplayName = "Walk State"),
	Run_State UMETA(DisplayName = "Run State"),
	SprintRun_State UMETA(DisplayName = "SprintRun State")
};

UENUM(BlueprintType)
enum class EWeaponType : uint8
{
	RifleType UMETA(DisplayName = "Rifle"),
	SniperRifleType UMETA(DisplayName = "SniperRifle"),
	GrenadeLauncherType UMETA(DisplayName = "GrenadeLauncher"),
	ShotGanType UMETA(DisplayName = "ShotGan"),
	PistolType UMETA(DisplayName = "Pistol"), 
};

USTRUCT(BlueprintType)
struct FCharacterSpeed
{
	GENERATED_BODY() 

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float  AimSpeedNormal = 300.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float WalkSpeedNormal = 200.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float RunSpeedNormal = 600.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float AimSpeedWalk = 100.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float SprintRunSpeedRun = 800.0f;
};

USTRUCT(BlueprintType)
struct FProjectileInfo
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
	TSubclassOf<class AProjectileDefault> Projectile = nullptr;//здесь указываем BP_ПУЛИ
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
	UStaticMesh* ProjectileStaticMesh = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
	FTransform ProjectileStaticMeshOffset = FTransform();
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
	UParticleSystem* ProjectileTrailFx = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
	FTransform ProjectileTrailFxOffset = FTransform();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
	float ProjectileDamage = 20.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
	float ProjectileLifeTime = 20.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
	float ProjectileInitSpeed = 20000.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
	float ProjectileMaxSpeed = 20000.0f;
	
	//material to decal on hit
	//TMap - как бы массив для материалов, в котором есть значения и есть ключ к нему
	//Ключ - тип поверхности EPhysicalSurface -это Metall, Wood, Flash итд
	//Значение - UMaterialInterface* - //материал декальки на поверхности, которые соответствуют значению ключа
	//HitDecals - имя переменной для Тмап
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FX")
	TMap<TEnumAsByte<EPhysicalSurface>, UMaterialInterface*> HitDecals;//декальки на поверхности
	//Sound when hit
	//Звук будет один -поэтому так просто без Тмапы
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FX")
	USoundBase* HitSound = nullptr;
	//fx when hit check by surface
	//Еще одна Тмапа по эффектам на физическое тело
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FX")
	TMap<TEnumAsByte<EPhysicalSurface>, UParticleSystem*> HitFXs;

	//к системе бафов и дебафов-  устанавливаем эффект
	// Это например - блюпринт MyTDS_StateEffect_Fire
	//в нем есть Массив EPhysicalSurface- тайпов поверхностей, способных наложиться туда, куда мы попали итд 
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Effect")
	TSubclassOf<UMyTDS_StateEffect> Effect = nullptr;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Explode")
	UParticleSystem* ExploseFX = nullptr; // эффект взрыва
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Explode")
	USoundBase* ExploseSound = nullptr;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Explode")
		float ProjectileMinRadiusDamage = 200.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Explode")
		float ProjectileMaxRadiusDamage = 200.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Explode")
		float ExploseMaxDamage = 40.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Explode")
		float ExplodeFallOff = 1.0f; //хз  -пока не используется
	
	//Timer add
};

USTRUCT(BlueprintType)
struct FWeaponDispersion
{
	GENERATED_BODY()
	//надо доделать!
	// UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
	// 	float DispersionAimStart = 0.5f; //дисперсия, когда начинаем целиться
	// UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
	// 	float DispersionAimMax = 1.0f;
	// UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
	// 	float DispersionAimMin = 0.1f;
	// UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
	// 	float DispersionAimShootCoef = 1.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		float Aim_StateDispersionAimMax = 2.0f; //коэф отдачи
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		float Aim_StateDispersionAimMin = 0.3f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		float Aim_StateDispersionAimRecoil = 1.0f;
	//Отдача - коэффициент изменения дисперсии при выстреле!
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		float Aim_StateDispersionReduction = 0.3f;//коэф изменеия дисперсии НА ТИКЕ!!!
	//при прицеливании отдача уменьшается
	//сведение разброса, в зависимости от состояния
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		float AimWalk_StateDispersionAimMax = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		float AimWalk_StateDispersionAimMin = 0.1f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		float AimWalk_StateDispersionAimRecoil = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		float AimWalk_StateDispersionReduction = 0.4f;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		float Walk_StateDispersionAimMax = 5.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		float Walk_StateDispersionAimMin = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		float Walk_StateDispersionAimRecoil = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		float Walk_StateDispersionReduction = 0.2f;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		float Run_StateDispersionAimMax = 10.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		float Run_StateDispersionAimMin = 4.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		float Run_StateDispersionAimRecoil = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		float Run_StateDispersionReduction = 0.1f;
};

USTRUCT(BlueprintType)
struct FAnimationWeaponInfo 
{
	GENERATED_BODY()
	//Анимации для чара
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim Char")
	UAnimMontage* AnimCharFire = nullptr;//анимация стрельбы
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim Char")
	UAnimMontage* AnimCharFireAim = nullptr;//Добавил из кода: анимация в стрельбе и прицеливании?
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim Char")
	UAnimMontage* AnimCharAim = nullptr;//в прицеливании
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim Char")
	UAnimMontage* AnimCharReload = nullptr; //перезазядка
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim Char")
	UAnimMontage* AnimCharReloadAim = nullptr;//перезарядка в прицеливании

	//Анимации для оружия
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim Char")
	UAnimMontage* AnimWeaponFire = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim Char")
	UAnimMontage* AnimWeaponReload = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim Char")
	UAnimMontage* AnimWeaponReloadAim = nullptr;
};

USTRUCT(BlueprintType)
struct FDropMashInfoInfo//Структурка и для магазина и для гильзы
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMesh")
	UStaticMesh* DropMesh = nullptr;
	//if 0.0 immediatele drop
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMesh")
	float DropMeshTime = -1.0f; //время когда должен появиться статик меш
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMesh")
	float DropMeshLifeTime = 5.0f;//его жизненыный цикл
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMesh")

	FTransform DropMeshOffset = FTransform();	//его локация - смещение
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMesh")
	FVector DropMeshImpulseDir = FVector(0.0f);//направление импульса
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMesh")
	float PowerImpulse = 0.0f;//сила импульса
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMesh")
	float ImpulseRandomDispersion = 0.0f;//разброс импульса - напрвление импульса
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMesh")
	float CastomMass = 0.0f;// кастомная масса
};

//ОСНОВНАЯ ТАБЛИЦА!
USTRUCT(BlueprintType)
struct FWeaponInfo : public FTableRowBase //ТАБЛИЦА с инфой об оружии!
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Class")
	TSubclassOf<class AWeaponDefault> WeaponClass = nullptr;//Здесь BP_оружия
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
	float RateOfFire = 0.5f;//скорострельность
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
	float ReloadTime = 5.0f; //время перезарядки
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
	float ReloadAnimTime = 3.0f; //время анимации перезарядки
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
	int32 MaxRound = 12; //кол-во мах патронов в МАГАЗИНЕ!!!
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
	int32 NumberProjectileByShot = 1; //кол-во патронов, спавнящихся одновременно, т.е кол-во пуль, вылетаемых из дула

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		FWeaponDispersion DispersionWeapon; // вставляем в таблицу структуру FWeaponDispersion - разброса оружия

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sound ")
		USoundBase* SoundFireWeapon = nullptr; //звук при выстреле
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sound ")
		USoundBase* SoundReloadWeapon = nullptr; //звук при перезарядке
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FX ")
		UParticleSystem* EffectFireWeapon = nullptr; //эффект при стрельбе

	//if null use trace logic (TSubclassOf<class AProjectileDefault> Projectile = nullptr)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile ")
		FProjectileInfo ProjectileSetting;//// вставляем в таблицу структуру FProjectileInfo с инфой о пуле

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Trace ")
		float WeaponDamage = 20.0f; //если проджектайла нет
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Trace ")
		float DistacneTrace = 2000.0f; //длина трейса
	//one decal on all?
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HitEffect ")
		UDecalComponent* DecalOnHit = nullptr; //декалька на все хиты трейсов - не используется! и не вижу в таблице!!!

	//перенесли в FAnimationWeaponInfo
	// UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim ")
	// 	UAnimMontage* AnimCharFire = nullptr; //анимация когда стреляет - не используется!
	// UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim ")
	// 	UAnimMontage* AnimCharReload = nullptr; // и перзаряжается

	// UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mesh ")
	// 	UStaticMesh* MagazineDrop = nullptr; //статик меши когда идет перезарядка и мы сбрасываем магазин
	// UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mesh ")
	// 	UStaticMesh* ShellBullets = nullptr; //и при выстреле, когда вылетает болванка пули
	// //можно это сделать в виде эффекта и даже добавить в анимацию, но будем делать через код
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim ")
		FAnimationWeaponInfo AnimWeaponInfo; //для анимаций оружия
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mesh ")
		FDropMashInfoInfo ClipDropMesh; //для магазина
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mesh ")
		FDropMashInfoInfo ShellBullets;//для гильзы

	//Инвентарь
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory ")
	float SwitchTimeToWeapon = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory ")
	UTexture2D* WeaponIcon = nullptr;
	//тип оружия -чтобы можно было понять какое оружие перезарядилось (по умолчанию ставим Rifle)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory ")
	EWeaponType WeaponType = EWeaponType::RifleType;
};
//структура для ОРУЖИЯ, когда в нем что-то меняется, например: ск-ко патронов в данный момент в ОРУЖИИ
//идет в слот FWeaponSlot
USTRUCT(BlueprintType)
struct FAdditionalWeaponInfo
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Stats")
	int32 Round = 10; // остаток патронов в оружии в данный момент -или остаток патронов
	//10 - стартовое число патронов в магазине
};

//структура для массива слотов- тип ОРУЖИЯ и ск-ко патронов осталось
USTRUCT(BlueprintType)
struct FWeaponSlot
{
	GENERATED_BODY() 

	// UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponSlot")
	// int32 IndexSlot = 0; //вряд ли понадобится?
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponSlot")
	FName NameItem; //имя оружия например: Rifle_v1
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponSlot")
	FAdditionalWeaponInfo AdditionalInfo; //ск-ко паторонов осталось в оружии
	
};
// Собственно это инфа об ИНВЕНТАРЕ!!!
// структура для массива слота патронов - тип оружия, для которого подходят патроны,
//  остаток патронов в инвертаре, кот-е подходят и их макс кол-во
USTRUCT(BlueprintType)
struct FAmmoSlot
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AmmoSlot")
	EWeaponType WeaponType = EWeaponType::RifleType; //тип инвентаря
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AmmoSlot")
	int32 Cout = 100;; //остаток  патронов в конкретном инвентаре
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AmmoSlot")
	int32 MaxCout = 100; //и их макс кол-во
	
};


//НОВАЯ ТАБЛИЦА!!! ДЛЯ ДРОПА (-СБРОСА) ОРУЖИЯ
USTRUCT(BlueprintType)
struct FDropItem : public FTableRowBase //доступная в таблице
//Не забыть прописать в BP_GameInstance!!!
{
	GENERATED_BODY()

	///Index Slot by Index Array
	///мы можем инициализировать как статик мэш. так и скелетал мэш
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropItem")
	UStaticMesh* WeaponStaticMesh = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropItem")
	USkeletalMesh* WeaponSkeletMesh = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropItem")
	UParticleSystem* ParticleSystem = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropItem")
	FTransform Offset;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropItem")
	FWeaponSlot WeaponInfo; // какое оружие мы дропаем
	//FAmmoSlot - не добавили в таблицу - настройка в чаре в компоненте!
};

UCLASS()
class MYTDS_API UTypes : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

	public:

	//static -обязательно!
	//Решаем проблему появления эффектов при получении урона мобом -передаем куда попали в скелет Fname NameBoneHit
    	UFUNCTION(BlueprintCallable)
		static void AddEffectBySurfaceType(AActor* TakeEffectActor, FName NameBoneHit, TSubclassOf<UMyTDS_StateEffect> AddEffectClass, EPhysicalSurface SurfaceType);

	//Когда мы создаем ститическую ф-ю static - нельзя использовать Server или NetMulticast!!!
	//в любом случае types -это структура и она мультиплицируется
		UFUNCTION(BlueprintCallable)
	static void ExecuteEffectAdded(UParticleSystem* ExecuteFX, AActor* target, FVector offset, FName Socket);

};
