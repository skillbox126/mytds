// Fill out your copyright notice in the Description page of Project Settings.
#include "WeaponDefault.h"
#include "Animation/AnimInstance.h"
#include "DrawDebugHelpers.h"
//#include "MyTDS/StateEffects/MyTDS_StateEffect.h"
//#include "ShaderCompilerCore.h"
#//include "MovieSceneTracksPropertyTypes.h"
//#include "../../../../../../../../Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/lib/clang/13.1.6/include/stddef.h"
#include "Engine/StaticMeshActor.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Math/UnrealMathUtility.h"
#include "MyTDS/MyTDS.h"
#include "MyTDS/Character/MyTDSInventoryComponent.h"

#include "Net/UnrealNetwork.h"

//#include "MyTDS/Interface/MyTDS_IGameActor.h"

//Избавился??? (я не избавился!) от булевской переменной, которая отвечала за дебаг логику в оружии и сделал это в виде консольной команды
int32 DebugWeaponShow = 0;
FAutoConsoleVariableRef CVarWeaponShow(
	TEXT("MyTDS.DebugWeapon"),
	DebugWeaponShow,
	TEXT("Draw Debug for Weapon"),
	ECVF_Cheat);

// Sets default values
AWeaponDefault::AWeaponDefault()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//NET -Важно!!!
	//SetReplicates(true);
	bReplicates = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	RootComponent = SceneComponent;

	SkeletalMeshWeapon = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Skeletal Mesh"));
	SkeletalMeshWeapon->SetGenerateOverlapEvents(false);
	SkeletalMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	SkeletalMeshWeapon->SetupAttachment(RootComponent);

	StaticMeshWeapon = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh "));
	StaticMeshWeapon->SetGenerateOverlapEvents(false);
	StaticMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	StaticMeshWeapon->SetupAttachment(RootComponent);

	ShootLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("ShootLocation"));
	ShootLocation->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void AWeaponDefault::BeginPlay()
{
	Super::BeginPlay();
	
	WeaponInit(); // проверяем и удаляем меши, котрых нет

}

// Called every frame
void AWeaponDefault::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	//NET  - делаем ТОЛЬКО НА СЕРВЕРЕ!
	if (HasAuthority())
	{
		FireTick(DeltaTime);
		ReloadTick(DeltaTime);
		DispersionTick(DeltaTime);
		ClipDropTick(DeltaTime);
		ShellDropTick(DeltaTime);
	}
}

void AWeaponDefault::FireTick(float DeltaTime)
{
	if (WeaponFiring && GetWeaponRound() > 0 && !WeaponReloading)
	{
		if (FireTimer < 0.f)
		{
			Fire();
		}
		else
			FireTimer -= DeltaTime;
	}
}	

void AWeaponDefault::ReloadTick(float DeltaTime) //перезараядка
{
	if (WeaponReloading)
	{
		if (ReloadTimer < 0.0f) //до тех пор, пока есть перезарядка
			{
			FinishReload();
			}
		else
		{
			ReloadTimer -= DeltaTime;
		}
	}
}

void AWeaponDefault::DispersionTick(float DeltaTime)
{
	if (!WeaponReloading)//если оружие не перезаряжается
	{
		if (!WeaponFiring)//и не стреляет
		{
			if (ShouldReduceDispersion)// ShouldReduceDispersion -см MovementTick -в зависимости от состояния игрока
				//либо уменьшаем разброс в состоянии прицеливания, либо увеличиваем
				CurrentDispersion = CurrentDispersion - CurrentDispersionReduction;
			else
				//если не перезаряжается, не стреляет, не в состоянии прицеливания Aim_State: или AimWalk_State:
				CurrentDispersion = CurrentDispersion + CurrentDispersionReduction;
		}
		//и проверяем на макс и мин
		//и если еще в состоянии стрельбы: срабатывает ChangeDispersionByShot и CurrentDispersion = CurrentDispersion + CurrentDispersionRecoil;
		if (CurrentDispersion < CurrentDispersionMin)// если вышли за границы заданных
		{
			CurrentDispersion = CurrentDispersionMin;
		}
		else
		{
			if (CurrentDispersion > CurrentDispersionMax)
			{
				CurrentDispersion = CurrentDispersionMax;
			}
		}
	}
	if(ShowDebug)
		UE_LOG(LogTemp, Warning, TEXT("Dispersion: MAX = %f. MIN = %f. Current = %f"), CurrentDispersionMax, CurrentDispersionMin, CurrentDispersion);
}

void AWeaponDefault::ClipDropTick(float DeltaTime)
{
	if(DropClipFlag)
	{
		if(DropClipTimer < 0.0f)
		{
			DropClipFlag = false;
			InitDropMesh_OnServer(WeaponSetting.ClipDropMesh.DropMesh,
				WeaponSetting.ClipDropMesh.DropMeshOffset,
				WeaponSetting.ClipDropMesh.DropMeshImpulseDir,
				WeaponSetting.ClipDropMesh.DropMeshLifeTime,
				WeaponSetting.ClipDropMesh.ImpulseRandomDispersion
				,WeaponSetting.ClipDropMesh.PowerImpulse,
				WeaponSetting.ClipDropMesh.CastomMass);
		}
		else
			DropClipTimer -= DeltaTime;
		
	}
}

void AWeaponDefault::ShellDropTick(float DeltaTime)
{
	if (DropShellFlag)
	// CastomMass = WeaponSetting.ShellBullets.CastomMass;
	// PowerImpulse  = WeaponSetting.ShellBullets.PowerImpulse;
	{
		if (DropShellTimer < 0.0f)
		{
			DropShellFlag = false;
			InitDropMesh_OnServer(WeaponSetting.ShellBullets.DropMesh,
				WeaponSetting.ShellBullets.DropMeshOffset,
				WeaponSetting.ShellBullets.DropMeshImpulseDir,
				WeaponSetting.ShellBullets.DropMeshLifeTime,
				WeaponSetting.ShellBullets.ImpulseRandomDispersion,
				WeaponSetting.ShellBullets.PowerImpulse,
				WeaponSetting.ShellBullets.CastomMass);
		}
		else
		{
			DropShellTimer -= DeltaTime;
		}
	}
}

void AWeaponDefault::WeaponInit()
{
	if (SkeletalMeshWeapon && !SkeletalMeshWeapon->SkeletalMesh) //Проверяем: Если у нашего SkeletalMeshWeapon есть SkeletalMesh, то мы с ним ничего не делаем
	{
		SkeletalMeshWeapon->DestroyComponent(true);//Если нет: !SkeletalMeshWeapon - то мы это компонет уничтожаем
	}

	if (StaticMeshWeapon && !StaticMeshWeapon->GetStaticMesh())// Так же и со StaticMeshWeapon проверяем Если есть: StaticMeshWeapon->GetStaticMesh()
	{
		StaticMeshWeapon->DestroyComponent();// если нет: !StaticMeshWeapon->GetStaticMesh() - удаляем
	}

	UpdateStateWeapon_OnServer(EMovementState::Run_State);
	
//	WeaponSetting = WeaponInfo;
	//WeaponInfo.Round = WeaponSetting.MaxRound;
	//НЕ РАБОТАЕТ!!! Надо тянуть из таблицы!???
	// когда оружие инициализируется нада в раунд записать максимальное количество патронов
}

//Net - Клиент переключается в режим стрельбы
void AWeaponDefault::SetWeaponStateFire_OnServer_Implementation(bool bIsFire) //Зажали ЛКМ -приходит тру
{
	if (CheckWeaponCanFire()) // т.е. проверяем -  оружие может стрелять? В состоянии спринта мы не сможем выстрелить
		WeaponFiring = bIsFire;
	else
		WeaponFiring = false;
		FireTimer = 0.01f; //!!!
}
 
//Блокировка стрельбы  -когда бежим - в состоянии SprintRun_State
bool AWeaponDefault::CheckWeaponCanFire()
{
	return !BlockFire; //когда бежим. BlockFire = false по дефолту
}

FProjectileInfo AWeaponDefault::GetProjectile()
{	
	return WeaponSetting.ProjectileSetting; //Здесь  в BP_WeaponDefaultRifle в  WeaponSetting - ProjectileSetting стоит блюпринт BP_WeaponProjectileRifle
}

void AWeaponDefault::Fire()
{
//_OnServer!!! by WeaponFire bool. Т.К WeaponFire вкл на сервере в SetWeaponStateFire_OnServer, то отрабатывать будет на сервере

if (GetNetMode() == NM_Client)
{
	UE_LOG(LogMyTDS_Net, Warning, TEXT("AWeaponDefault::Fire - OnClient "));
}

	UAnimMontage* AnimToPlay = nullptr;
	if(WeaponAiming)
		AnimToPlay = WeaponSetting.AnimWeaponInfo.AnimCharFireAim;
	else
		AnimToPlay = WeaponSetting.AnimWeaponInfo.AnimCharFire;

	//NET
	if(WeaponSetting.AnimWeaponInfo.AnimWeaponFire)//чтобы не тратить РПЦ
	{
		AnimWeaponStart_Multicast(WeaponSetting.AnimWeaponInfo.AnimWeaponFire);
	}
	
	if(WeaponSetting.ShellBullets.DropMesh) //проверка -есть ли вообще этот меш
	{
		if (WeaponSetting.ShellBullets.DropMeshTime < 0.0f)//проверка на таймер. Если таймера нет -
		{
			// CastomMass = WeaponSetting.ShellBullets.CastomMass;
			// PowerImpulse  = WeaponSetting.ShellBullets.PowerImpulse;
			//Функция отрабатывает и на магазин Clip и на гильзу Shell
			InitDropMesh_OnServer(WeaponSetting.ShellBullets.DropMesh,
				WeaponSetting.ShellBullets.DropMeshOffset,
				WeaponSetting.ShellBullets.DropMeshImpulseDir,
				WeaponSetting.ShellBullets.DropMeshLifeTime,
				WeaponSetting.ShellBullets.ImpulseRandomDispersion,
				WeaponSetting.ShellBullets.PowerImpulse,
				WeaponSetting.ShellBullets.CastomMass);
		}
		else //то мы дропаем сразу
		{
			DropShellFlag = true;
			DropShellTimer = WeaponSetting.ShellBullets.DropMeshTime;
		}
	}
//	OnWeaponFireStart.AddDynamic(this, &AMyTDSCharacter::WeaponReloadStart);
//	OnWeaponFireStart.Broadcast(AnimToPlay);
	


	FireTimer = WeaponSetting.RateOfFire; // скорострельность - в Type.h  - UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponSetting") float RateOfFire = 0.5f; -> В BP преопределена на 0.05
	// И У оружия сделали в хедере переменную WeaponSetting для доступа к структуре FWeaponInfo: UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FireLogic") FWeaponInfo WeaponSetting;
	AdditionalWeaponInfo.Round = AdditionalWeaponInfo.Round - 1; // когда выстрел - колв-во патронов -1
	ChangeDispersionByShot(); //когда мы делаем выстрел -наша дисперсия будет увеличиваться на CurrentDispersionRecoil 

	OnWeaponFireStart.Broadcast(AnimToPlay);

	//Multicast
	FXWeaponFire_Multicast(WeaponSetting.EffectFireWeapon, WeaponSetting.SoundFireWeapon);

	// //Sound -спавним звуки эффект стрельбы
	// UGameplayStatics::SpawnSoundAtLocation(GetWorld(), WeaponSetting.SoundFireWeapon, ShootLocation->GetComponentLocation());
	// UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), WeaponSetting.EffectFireWeapon, ShootLocation->GetComponentLocation(), ShootLocation->GetComponentRotation());

	int8 NumberProjectile = GetNumberProjectileByShot(); //устанавливаем кол-во патронов, спавнящихся одновременно
	
	if (ShootLocation) // стрелка, напраление выстрела - лучше лишний раз поставить проверку
	{
		FVector SpawnLocation = ShootLocation->GetComponentLocation(); //находим - где пулька должна заспаунится от этого компонента
		FRotator SpawnRotation = ShootLocation->GetComponentRotation();
		FProjectileInfo ProjectileInfo; // берем из Type.h и создаем переменную
		ProjectileInfo = GetProjectile();  // получаем BP пули

		FVector EndLocation;
		for (int i = 0; i < NumberProjectile; i++)//ShotGun
		{
			// пуля летит по GetFireEndLocation()
			EndLocation = GetFireEndLocation();
		//	EndLocation = ShootEndLocation;
			
			if (ProjectileInfo.Projectile) //если  класс BP существует - делаем балистичекий огонь
				{
				//Projectile init ballistic fire

				//надо определить  -в какую сторону отправлять нашу пульку, т.е. ее ротацию
				//FVector Dir = /*EndLocation*/GetFireEndLocation() - SpawnLocation; //определяем направление. Берем положение курсора и отнимаем
				// от вектора, где находится arm компонет нашего оружия
				FVector Dir = EndLocation - SpawnLocation; 
				Dir.Normalize(); //Если бы вектор был (100, 100, 100), то после нормализации: (1,1,1)
				FMatrix myMatrix(Dir, FVector(0, 0, 0), FVector(0, 0, 0), FVector::ZeroVector); //строим матрицу по плоскости у/z
				SpawnRotation = myMatrix.Rotator(); //получаем ротацию для пульки
				
				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = GetOwner();
				SpawnParams.Instigator = GetInstigator();

				//Когда происходит спавн - он уже появился и на клиенте, т.к. SpawnActor гарантирует, что дальше логика не пойдет, пока объект не появится
				AProjectileDefault* myProjectile = Cast<AProjectileDefault>(GetWorld()->SpawnActor(ProjectileInfo.Projectile, &SpawnLocation, &SpawnRotation, SpawnParams));
					if (myProjectile) // 
					{
						//ToDo Init Projectile settings by id in table row(or keep in weapon table)
						//myProjectile->InitialLifeSpan = 20.0f; // наш Projectile должен жить 20 сек
						//Projectile->BulletProjectileMovement->InitialSpeed = 2500.0f;
						myProjectile->InitProjectile(WeaponSetting.ProjectileSetting);
					}
				}
			else // если нет пуль - делаем трейсовую логику
			{
				//ToDo Multicast TraseFX
				FHitResult Hit;//переменная типа FHitResult . Она будет наполнена различной информацией, связанной с найденным объектом.
				// TArray<AActor*>Actors;
				// UKismetSystemLibrary::LineTraceSingle(GetWorld(),
				// SpawnLocation,
				// 	EndLocation * WeaponSetting.DistacneTrace,
				// 	ETraceTypeQuery::TraceTypeQuery4,//тип трассировки столкновений  -почему 4?
				// 	false,
				// 	Actors,
				// 	EDrawDebugTrace::ForDuration,
				// 	Hit,
				// 	true,
				// 	FLinearColor::Red,
				// 	FLinearColor::Green,
				// 	5.0f
				// 	);

				LineTrace_Multicast(Hit, SpawnLocation, EndLocation);

				
				if (ShowDebug)
					DrawDebugLine(GetWorld(), SpawnLocation, SpawnLocation + ShootLocation->GetForwardVector() * WeaponSetting.DistacneTrace, FColor:: Blue, false, 5.0f, 0, 5.0f);

				if(Hit.GetActor() && Hit.PhysMaterial.IsValid())//если есть актор, с котрым хит - получем актор и у него есть материал -получаем валидный материал
				{
					EPhysicalSurface mySurfacetype = UGameplayStatics::GetSurfaceType(Hit); //получаем mySurfacetype - -тип поверхности с которым был Hit - это Metall, Wood, Flash итд итд
					// Но он же является и ключом для ТМап Projectille - пули!!! Идем в пулю!
					if (WeaponSetting.ProjectileSetting.HitDecals.Contains(mySurfacetype))//проверяем  - содержит ли наша Тмапа такой Surfacetype - 
					{
						//если да -мы берем ключ на Т-мапу mySurfacetype и вытаскиваем соответствующий ключу материал декали
						UMaterialInterface* myMaterial = WeaponSetting.ProjectileSetting.HitDecals[mySurfacetype]; 

						if (myMaterial && Hit.GetComponent())//проверяем  -есть ли материал и  физический компонент у материала
							{
							// UGameplayStatics::SpawnDecalAttached(myMaterial, //спауним декаль из материала
							// 	FVector(20.0f),
							// 			Hit.GetComponent(),//куда аттачим,
							// 			NAME_None,// NAME_None -без кости,
							// 	Hit.ImpactPoint,//-туда, куда попали,
							// 	Hit.ImpactNormal.Rotation(), // разворачиваем ротацию по нормали
							// 	EAttachLocation::KeepWorldPosition,//-условия аттача
							// 	10.0f); //10.0 - сколько сек живет декаль
							
							//Multicast
							SpawnTraceHitDecal_Multicast(myMaterial, Hit);
							UE_LOG(LogTemp, Warning, TEXT("myMaterial : %p"), myMaterial);
							}
						
						if (WeaponSetting.ProjectileSetting.HitFXs.Contains(mySurfacetype))//то же самое с частицами HitFXs
							{
								UParticleSystem* MyParticle = WeaponSetting.ProjectileSetting.HitFXs[mySurfacetype];
								if (MyParticle)
								{
									// UGameplayStatics::SpawnEmitterAtLocation(GetWorld(),
									// 	MyParticle,
									// 	FTransform(Hit.ImpactNormal.Rotation(),
									// 		Hit.ImpactPoint,
									// 		FVector(1.0f)));
									SpawnTraceHitFX_Multicast(MyParticle, Hit);
									UE_LOG(LogTemp, Warning, TEXT("MyParticle : %p"), MyParticle);
								}
							}
						if (WeaponSetting.ProjectileSetting.HitSound)
							{
								//UGameplayStatics::PlaySoundAtLocation(GetWorld(), WeaponSetting.ProjectileSetting.HitSound, Hit.ImpactPoint);
								SpawnTraceHitSound_Multicast(WeaponSetting.ProjectileSetting.HitSound, Hit);
							}

						//Функция из Tipes.cpp Интерфейс  Hit.GetActor() - Актор, с которым столкнулись
						UTypes::AddEffectBySurfaceType(Hit.GetActor(), Hit.BoneName, ProjectileInfo.Effect, mySurfacetype);

						UGameplayStatics::ApplyPointDamage(Hit.GetActor(), WeaponSetting.ProjectileSetting.ProjectileDamage, Hit.TraceStart,Hit, GetInstigatorController(),this,NULL);
						//UGameplayStatics::ApplyDamage(Hit.GetActor(), WeaponSetting.ProjectileSetting.ProjectileDamage, GetInstigatorController(), this, NULL);
					}
				}
			}
		}
	}

	//После того, как наше оружие отработает и посчитает, ск-ко патронов потрачено -инициализируем проверку
	// что м.б. оружию пора перезаряжаться и оно не в состоянии перзарядки то мы попытаемся его перезарядить
	// (перенесено из тика!)
	if (GetWeaponRound() <= 0 && !WeaponReloading)
	{
		//init Reload
		if(CheckCanWeaponReload())
			InitReload();
	}
}

// Т.е. в зависимости от того, какой приходит MovementState будем менять дисперсию для оружия
//ChangeDispersion();
void AWeaponDefault::UpdateStateWeapon_OnServer_Implementation(EMovementState NewMovementState)
{
	//ToDo Dispersion
	BlockFire = false; //блочим, когда передвигаемся в стрельбе
    
    	switch (NewMovementState)
    	{
    	case EMovementState::Aim_State:
    		
    		CurrentDispersionMax = WeaponSetting.DispersionWeapon.Aim_StateDispersionAimMax;
    		CurrentDispersionMin = WeaponSetting.DispersionWeapon.Aim_StateDispersionAimMin;
    		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Aim_StateDispersionAimRecoil;
    		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Aim_StateDispersionReduction;
    		WeaponAiming = true;
    		break;
    	case EMovementState::AimWalk_State:
    		
    		CurrentDispersionMax = WeaponSetting.DispersionWeapon.AimWalk_StateDispersionAimMax;
    		CurrentDispersionMin = WeaponSetting.DispersionWeapon.AimWalk_StateDispersionAimMin;
    		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.AimWalk_StateDispersionAimRecoil;
    		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Aim_StateDispersionReduction;
    		WeaponAiming = true;
    		break;
    	case EMovementState::Walk_State:
    		
    		CurrentDispersionMax = WeaponSetting.DispersionWeapon.Walk_StateDispersionAimMax;
    		CurrentDispersionMin = WeaponSetting.DispersionWeapon.Walk_StateDispersionAimMin;
    		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Walk_StateDispersionAimRecoil;
    		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Aim_StateDispersionReduction;
    		break;
    	case EMovementState::Run_State:
    		
    		CurrentDispersionMax = WeaponSetting.DispersionWeapon.Run_StateDispersionAimMax;
    		CurrentDispersionMin = WeaponSetting.DispersionWeapon.Run_StateDispersionAimMin;
    		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Run_StateDispersionAimRecoil;
    		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Aim_StateDispersionReduction;
    		break;
    	case EMovementState::SprintRun_State:
    		BlockFire = true; //когда мы в спринте -тру  -не стреляем
    		SetWeaponStateFire_OnServer(false);//set fire trigger to false - когда игрок зажал стрельбу и переходит
    		//в сосояние спринта - сбросит стрельбу на фолс и сбросит фйртаймер - когда у нас винтовка стреляет, мы отжали курок,
    		// а таймер остался на 2 сек и следующий выстрел произойдет некорректно
    		//Block Fire
    		break;
    	default:
    		break;
    	}
}

//когда мы делаем выстрел -наша дисперсия будет меняться на парметр из таблицы состояний перса
//функция вызывается  -когда происходит выстрел
void AWeaponDefault::ChangeDispersionByShot()
{
		CurrentDispersion = CurrentDispersion + CurrentDispersionRecoil;
}

float AWeaponDefault::GetCurrentDispersion() const
{
	float Result = CurrentDispersion;
	return Result;
}


FVector AWeaponDefault::ApplyDispersionToShoot(FVector DirectionShoot) const
{
	return FMath::VRandCone(DirectionShoot, GetCurrentDispersion() * PI / 180.f);
	//VRandCone берет направление вектора и в зависимости от напрвления оружия и угла  GetCurrentDispersion, которая выдаст разброс, Умножаем на PI / 180.f, чтобы получить радианы
	//GetCurrentDispersion - работает от табличных данных в стейтах в Type.h
}

FVector AWeaponDefault::GetFireEndLocation() const
{
//	bool bShootDirectional = false;
	FVector EndLocation = FVector(0.f);
	
	FVector tmpV = (ShootLocation->GetComponentLocation() - ShootEndLocation);
	//вектор направления -от дула, где спавнится стрелка до столкновения курсора с какой-то поверхностью
	//+ смещение ResultHit.Location + Displacement;
//	DrawDebugSphere(GetWorld(), ShootLocation->GetComponentLocation() + ShootEndLocation, 10.f, 8, FColor::Red, false, 4.0f);

	// UE_LOG(LogTemp, Warning, TEXT("Vector: X = %f, Y = %f, Z = %f, Size = %f")) tmpV.X, tmpV.Y, tmpV.Z, tmpV.Size();
	if (tmpV.Size() > SizeVectorToChangeShootDirectionLogic) //если длина вектора > 100 -т.е. рядом с пешкой дисперсии нет
		//в противном случае меняем точку конечной локации пули
//	if (byBarell)//GetComponentLocation -локация возле дула
	{
		EndLocation = ShootLocation->GetComponentLocation() + ApplyDispersionToShoot((ShootLocation->GetComponentLocation() - ShootEndLocation).GetSafeNormal()) * -20000.0f;
		if(ShowDebug)
		DrawDebugCone(GetWorld(), ShootLocation->GetComponentLocation(), -(ShootLocation->GetComponentLocation() - ShootEndLocation), WeaponSetting.DistacneTrace, GetCurrentDispersion()* PI / 180.f, GetCurrentDispersion()* PI / 180.f, 32, FColor::Emerald, false, .1f, (uint8)'\000', 1.0f);
	}
	else
	{
		EndLocation = ShootLocation->GetComponentLocation() + ApplyDispersionToShoot(ShootLocation->GetForwardVector()) * 20000.0f;
		if(ShowDebug)
		DrawDebugCone(GetWorld(), ShootLocation->GetComponentLocation(), ShootLocation->GetForwardVector(), WeaponSetting.DistacneTrace, GetCurrentDispersion()* PI / 180.f, GetCurrentDispersion()* PI / 180.f, 32, FColor::Emerald, false, .1f, (uint8)'\000', 1.0f);
	}
	
	if (ShowDebug)
		{
		//direct weapon look
		DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), ShootLocation->GetComponentLocation() + ShootLocation->GetForwardVector() * 500.0f, FColor::Cyan, false, 5.f, (uint8)'\000', 0.5f);
		//direction projectile must fly
		DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), ShootEndLocation, FColor::Red, false, 5.f, (uint8)'\000', 0.5f);
		//Direction Projectile Current fly
		DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), EndLocation, FColor::Black, false, 5.f, (uint8)'\000', 0.5f);

		//DrawDebugSphere(GetWorld(), ShootLocation->GetComponentLocation() + ShootLocation->GetForwardVector() * SizeVectorToChangeShootDirectionLogic, 10.f, 8, FColor::Red, false, 4.0f);
		}
	// DrawDebugSphere(GetWorld(), ShootLocation->GetComponentLocation() + ShootLocation->GetForwardVector() * SizeVectorToChangeShootDirectionLogic, 10.f, 8, FColor::Blue, false, 4.0f);
	// DrawDebugSphere(GetWorld(), ShootLocation->GetComponentLocation() + EndLocation, 10.f, 8, FColor::Red, false, 4.0f);

	return EndLocation;
}
	
int8 AWeaponDefault::GetNumberProjectileByShot() const
{
	return WeaponSetting.NumberProjectileByShot;
}

int32 AWeaponDefault::GetWeaponRound()
{
	return AdditionalWeaponInfo.Round;
}

//Инициализация перезагрузки оружия
void AWeaponDefault::InitReload()
{
	//NET  - работает ТОЛЬКО НА СЕРВЕРЕ! (т.к. запуск из скрверной ф-ии)


	WeaponReloading = true;//NET - реплицируемая!
	ReloadTimer = WeaponSetting.ReloadTime;
	//Anim reload
	UAnimMontage* AnimToPlay = nullptr;
	if (WeaponAiming)
		AnimToPlay = WeaponSetting.AnimWeaponInfo.AnimWeaponReloadAim;
	else
		AnimToPlay = WeaponSetting.AnimWeaponInfo.AnimWeaponReload;

	//OnWeaponFireStart.Broadcast(AnimToPlay);//cо стартом огня сообщаем об анимации перезарядки оружия???
	OnWeaponReloadStart.Broadcast(AnimToPlay);

	UAnimMontage* AnimWeaponToPlay = nullptr;

	if (WeaponAiming)
		AnimWeaponToPlay = WeaponSetting.AnimWeaponInfo.AnimWeaponReloadAim;
	else
		AnimWeaponToPlay = WeaponSetting.AnimWeaponInfo.AnimWeaponReload;
	if (WeaponSetting.AnimWeaponInfo.AnimWeaponReload
		&& SkeletalMeshWeapon
		&& SkeletalMeshWeapon->GetAnimInstance())// Плохой код? М.б. лучше иниц. локальную перменну или в функции
	{
		//NET
		//SkeletalMeshWeapon->GetAnimInstance()->Montage_Play(AnimWeaponToPlay);
		AnimWeaponStart_Multicast(AnimWeaponToPlay);
	}

	if(WeaponSetting.ClipDropMesh.DropMesh)//если есть mesh для магазина
	{
		DropClipFlag =true;
		DropClipTimer = WeaponSetting.ClipDropMesh.DropMeshTime;
		// CastomMass = WeaponSetting.ClipDropMesh.CastomMass;
		// PowerImpulse = WeaponSetting.ClipDropMesh.PowerImpulse;
	}

	if(WeaponSetting.AnimWeaponInfo.AnimCharReload)//если есть анимация перезарядки
	 OnWeaponReloadStart.Broadcast(WeaponSetting.AnimWeaponInfo.AnimCharReload); // Мы перезаряжаемся - Broadcast -скажи всем, кто меня слушает
	 //Sound -спавним звуки эффект перезарядки
	 if(WeaponSetting.SoundReloadWeapon)
	 UGameplayStatics::SpawnSoundAtLocation(GetWorld(), WeaponSetting.SoundReloadWeapon, ShootLocation->GetComponentLocation());
}

void AWeaponDefault::FinishReload()
{
	WeaponReloading = false;
	//Мы взяли доступное кол-во патронов из инвентаря. Если инвентаря нет -то возьмется максимальное для этого оружия (9.7) 
	int8 AviableAmmoForInventory = GetAviableAmmoForReload(); // В магазине - в  WeaponSetting.MaxRound = 20

	int8 AmmoNeedTakeFromInv;
	//AdditionalWeaponInfo.Round - ск-ко было патронов до перезарядки, т.е. ск-ко патронов осталось в оружии!
	// WeaponSetting.MaxRound - ск-ко патронов в магазине!
	int8 NeedToReload = WeaponSetting.MaxRound - AdditionalWeaponInfo.Round; //20-5 = 15 //20-0 = 20
	// //делаем проверку например: максимальный размер магазина 20, а нам доступно 100 в инвентаре
	if (NeedToReload > AviableAmmoForInventory)//сработает, когда в инвентаре почти не останется патронов
		//напр. осталось 10. 20 > 10 (если не осталось мало патронов в инвентаре)
	{
		//Добавили += в 15.4
		AdditionalWeaponInfo.Round = +AviableAmmoForInventory;//10 
		AmmoNeedTakeFromInv = AviableAmmoForInventory;//=10
	}
	else
	{
		//AdditionalWeaponInfo.Round += NeedToReload;//5+15 //0 + 20 = 20
		//можно было бы просто так сделать:
		AdditionalWeaponInfo.Round = WeaponSetting.MaxRound;
		AmmoNeedTakeFromInv = NeedToReload;//20
	}

	// //делаем проверку например: максимальный размер магазина 30, а нам доступно 100
	// if(AviableAmmoForInventory > WeaponSetting.MaxRound)
	// 	//мы ограничиваем -возвращаем 30
	// 	AviableAmmoForInventory = WeaponSetting.MaxRound;
	// //Если у нас 29 патронов в магазине осталось, то мы должны взять лишь 1 патрон
	// int32 AmmoNeedTake; //ск-ко было патронов до перезарядки или ск-ко патронов осталось в магазине
	// AmmoNeedTake = AdditionalWeaponInfo.Round - AviableAmmoForInventory;//получаем отрицательное число!
	// AdditionalWeaponInfo.Round = AviableAmmoForInventory;
	
	// и отдаем инвентарю то кол-во, которое реально забрали
	OnWeaponReloadEnd.Broadcast(true, -AmmoNeedTakeFromInv); // передаём AmmoNeedTakeFromInv с минусом!
	//т.к, их надо вычитать в AmmoSlotChangeValue: AmmoSlots[i].Cout += CoutChangeAmmo(=AmmoNeedTakeFromInv);
}

//функция отменяет загрузку патронов  
void AWeaponDefault::CancelReload()
{
	WeaponReloading = false;
	if(SkeletalMeshWeapon && SkeletalMeshWeapon->GetAnimInstance())
		SkeletalMeshWeapon->GetAnimInstance()->StopAllMontages(0.15f);

	OnWeaponReloadEnd.Broadcast(false, 0);
	DropClipFlag = false;// чтобы если еще таймер не дошел до србрасывания магазина - она не отработала
}




//функция проверяет наличие патронов в инвентаре. Если есть -true -можно перезаряжаться
bool AWeaponDefault::CheckCanWeaponReload()
{
	bool result = true;
	//Парадигма: Оружие как инвентарь м.б. у кого угодно! GetOwner() -находим кому принадлежит. В нашем случае - Чару
	//если у этого оружия есть владелец
	if(GetOwner())
	{
		// и у этого оружия есть инвентарь
		UMyTDSInventoryComponent* MyInv = Cast<UMyTDSInventoryComponent>(GetOwner()->GetComponentByClass(UMyTDSInventoryComponent::StaticClass()));
		if (MyInv)
		{
			int32 AviableAmmoForWeapon;
			//и у оружия нет патронов  - false. 
			//м.б. в этом и хитрость, что & амперсанд дает адрес памяти в который записывается  остаток патронов?
	
			//проверяем -есть ли патроны в инвентаре
			if (!MyInv->CheckAmmoForWeapon(WeaponSetting.WeaponType,AviableAmmoForWeapon))
			{				
				result = false; //если нет патронов и перезараядка невозможна - бродкастим, что о что, нет патронов в инвентаре
				//а это значит что мы не можем перезарядится и патронов уже вообще нет в самом оружии

				//NET
				//MyInv->OnWeaponNotHaveRound.Broadcast(MyInv->GetWeaponIndexSlotByName(IdWeaponName));
				MyInv->WeaponNotHaveRoundEvent_Multicast(MyInv->GetWeaponIndexSlotByName(IdWeaponName));


				// UE_LOG(LogTemp, Warning, TEXT("FName IdWeaponName - %s"), *IdWeaponName.ToString());
				// UE_LOG(LogTemp, Warning, TEXT("GetWeaponIndexSlotByName - %d"), MyInv->GetWeaponSlots().Num());

			}
			else
			{
				//MyInv->OnWeaponHaveRound.Broadcast(MyInv->GetWeaponIndexSlotByName(IdWeaponName));
				MyInv->WeaponHaveRoundEvent_Multicast(MyInv->GetWeaponIndexSlotByName(IdWeaponName));
			}
		}
	}
	return result;
}




int8 AWeaponDefault::GetAviableAmmoForReload()
{
	int32 AviableAmmoForWeapon = WeaponSetting.MaxRound;
	//если мы не найдем ни Ownera, ни инвентарь -мы перезарядим оружие по дефолту - в макс кол-во патронов
	if(GetOwner()) 	//если у этого оружия есть владелец   
	{
		//UE_LOG(LogTemp, Error, TEXT("AWeaponDefault::CheckCanWeaponReload -OwnerName - %s"), *GetOwner()->GetName());
		UMyTDSInventoryComponent* MyInv = Cast<UMyTDSInventoryComponent>(GetOwner()->GetComponentByClass(UMyTDSInventoryComponent::StaticClass()));
		if (MyInv) // и у этого оружия есть инвентарь
		{ // и у этого инветаря нет! патронов:  
			if(!MyInv->CheckAmmoForWeapon(WeaponSetting.WeaponType, AviableAmmoForWeapon))
				//получается, что проходим в условие if и считаем остаток патронов и запоминаем по адресу,
				//но в внутрь if провалимся только тогда, когда патронов нет!
			{
				//AviableAmmForWeapon = AviableAmmForWeapon;  // так по уроку! Странно! Не работает!
				//AviableAmmoForWeapon = WeaponSetting.MaxRound;
				AviableAmmoForWeapon = 1; //так в финале -хз почему
			}
		}
	}
	return AviableAmmoForWeapon;
}


//InitDropMesh_OnServer -инициализация гильзы
//Offset - смешение (вектор)
void AWeaponDefault::InitDropMesh_OnServer_Implementation(UStaticMesh* DropMesh, FTransform Offset, FVector DropImpulseDirection,
	float LifeTimeMesh, float ImpulseRandomDispersion, float PowerImpulse, float CastomMass)
{
	if (DropMesh)
	{
		FTransform Transform;
	//наш 0-й пивот блюпринта в оружии - берем его форвард вектор и перномножаем на оффсет (смещение)- получаем локальную позици относительно оружия
		FVector LocalDir = this->GetActorForwardVector() * Offset.GetLocation().X + this->GetActorForwardVector() * Offset.GetLocation().Y + this->GetActorForwardVector() * Offset.GetLocation().Z;

		//Засовываем все в трансформ чтобы заспаунить AStaticMeshActor - NewActor
		Transform.SetLocation(GetActorLocation() + LocalDir);
		Transform.SetScale3D(Offset.GetScale3D());

		Transform.SetRotation((GetActorRotation() + Offset.Rotator()).Quaternion());
		//AStaticMeshActor* NewActor =nullptr;

		// FActorSpawnParameters Param;
		// Param.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
		// Param.Owner = this;

		//Net
		ShellDropFire_Multicast(DropMesh, Transform, DropImpulseDirection, LifeTimeMesh, ImpulseRandomDispersion, PowerImpulse, CastomMass, LocalDir);
	}
}


//Net

void AWeaponDefault::UpdateWeaponByCharacterMovementState_OnServer_Implementation(FVector NewShootEndLocation,
                                                                                  bool NewShouldReduceDispersion)
{
	ShootEndLocation = NewShootEndLocation;
	ShouldReduceDispersion = NewShouldReduceDispersion;
}


void AWeaponDefault::AnimWeaponStart_Multicast_Implementation(UAnimMontage* Anim)
{
	if(Anim
		&& SkeletalMeshWeapon
		&& SkeletalMeshWeapon->GetAnimInstance())
	{
		SkeletalMeshWeapon->GetAnimInstance()->Montage_Play(Anim);
	}
}

void AWeaponDefault::ShellDropFire_Multicast_Implementation(UStaticMesh* DropMesh,
		FTransform Offset,
		FVector DropImpulseDirection,
		float LifeTimeMesh,
		float ImpulseRandomDispersion,
		float PowerImpulse,
		float CastomMass,
		FVector LocalDir)
{
	AStaticMeshActor* NewActor =nullptr;
	FActorSpawnParameters Param;
	Param.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
	Param.Owner = this;

	NewActor = GetWorld()->SpawnActor<AStaticMeshActor>(AStaticMeshActor::StaticClass(), Offset, Param);// StaticClass -значит статичный
		if (NewActor && NewActor->GetStaticMeshComponent())
		{
			NewActor->GetStaticMeshComponent()->SetCollisionProfileName(TEXT ("IgnoreOnlyPawn"));
			NewActor->GetStaticMeshComponent()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);

			//Устанавливаем параметры для newActor
			NewActor->SetActorTickEnabled(false);
			NewActor->InitialLifeSpan = LifeTimeMesh;

			NewActor->GetStaticMeshComponent()->Mobility = EComponentMobility::Movable;
			NewActor->GetStaticMeshComponent()->SetSimulatePhysics(true);
			NewActor->GetStaticMeshComponent()->SetStaticMesh(DropMesh);

			NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_GameTraceChannel1, ECollisionResponse::ECR_Ignore);
			NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_GameTraceChannel2, ECollisionResponse::ECR_Ignore);
			NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_Pawn, ECollisionResponse::ECR_Ignore);
			NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_WorldStatic, ECollisionResponse::ECR_Block);
			NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_WorldDynamic, ECollisionResponse::ECR_Block);
			NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_PhysicsBody, ECollisionResponse::ECR_Block);

			if(CastomMass > 0.0f)
			{
				NewActor->GetStaticMeshComponent()->SetMassOverrideInKg(NAME_None, WeaponSetting.ClipDropMesh.CastomMass , true);
			}
// РАБОТАЕТ!
			if (!DropImpulseDirection.IsNearlyZero())
				{
					FVector FinalDir(0.0f);
					//UE_LOG(LogTemp, Warning, TEXT("LocalDir : %s"), *LocalDir.ToString());
					LocalDir = LocalDir + (DropImpulseDirection * 1000.0f);
					if(!FMath::IsNearlyZero(ImpulseRandomDispersion))
						FinalDir += UKismetMathLibrary::RandomUnitVectorInConeInDegrees(LocalDir, ImpulseRandomDispersion);
					//UE_LOG(LogTemp, Warning, TEXT("FinalDir : %s"), *FinalDir.ToString());
			
					FinalDir.GetSafeNormal(0.0000001f);
					//UE_LOG(LogTemp, Warning, TEXT("NewLocalDir : %s"), *LocalDir.ToString());
					//UE_LOG(LogTemp, Warning, TEXT("NewFinalDir : %s"), *FinalDir.ToString());
					//UE_LOG(LogTemp, Warning, TEXT("DropImpulseDirection : %s"), *DropImpulseDirection.ToString());
					//UE_LOG(LogTemp, Warning, TEXT("PowerImpulse : %f"), PowerImpulse);
			
					NewActor->GetStaticMeshComponent()->AddImpulse(FinalDir* PowerImpulse);
				}
		}
}

void AWeaponDefault::FXWeaponFire_Multicast_Implementation(UParticleSystem* FxFire, USoundBase* SoundFire)
{
	//Sound -спавним звуки эффект стрельбы
	if (SoundFire)
	{
		UGameplayStatics::SpawnSoundAtLocation(GetWorld(), SoundFire, ShootLocation->GetComponentLocation());
	}
	if (FxFire)
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), FxFire, ShootLocation->GetComponentTransform());
	}
}

void AWeaponDefault::SpawnTraceHitDecal_Multicast_Implementation(UMaterialInterface* DecalMaterial, FHitResult HitResult)
{
	UGameplayStatics::SpawnDecalAttached(DecalMaterial, //спауним декаль из материала
								FVector(20.0f),
										HitResult.GetComponent(),//куда аттачим,
										NAME_None,// NAME_None -без кости,
								HitResult.ImpactPoint,//-туда, куда попали,
								HitResult.ImpactNormal.Rotation(), // разворачиваем ротацию по нормали
								EAttachLocation::KeepWorldPosition,//-условия аттача
								10.0f); //10.0 - сколько сек живет декаль
}

void AWeaponDefault::SpawnTraceHitFX_Multicast_Implementation(UParticleSystem* FxTemplate, FHitResult HitResult)
{
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(),
										FxTemplate,
										FTransform(HitResult.ImpactNormal.Rotation(),
											HitResult.ImpactPoint,
											FVector(1.0f)));
} 

void AWeaponDefault::SpawnTraceHitSound_Multicast_Implementation(USoundBase* HitSound, FHitResult HitResult)
{
	UGameplayStatics::PlaySoundAtLocation(GetWorld(), HitSound, HitResult.ImpactPoint);
}

void AWeaponDefault::LineTrace_Multicast_Implementation(FHitResult HitResult, const FVector& SpawnLocation, const FVector& EndLocation)
{
	TArray<AActor*>Actors;
	UKismetSystemLibrary::LineTraceSingle(GetWorld(),
	SpawnLocation,
		EndLocation * WeaponSetting.DistacneTrace,
		ETraceTypeQuery::TraceTypeQuery4,//тип трассировки столкновений  -почему 4?
		false,
		Actors,
		EDrawDebugTrace::ForDuration,
		HitResult,
		true,
		FLinearColor::Red,
		FLinearColor::Green,
		5.0f
		);
}


void AWeaponDefault::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	//Макросы
	//Говорят о том что (например) в нашем объекте в классе AWeaponDefault - AdditionalWeaponInfo должна быть реплицируемой
	DOREPLIFETIME(AWeaponDefault, AdditionalWeaponInfo);
	DOREPLIFETIME(AWeaponDefault, ShootEndLocation);
	DOREPLIFETIME(AWeaponDefault, WeaponReloading);
}