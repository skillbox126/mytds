//Fill out your copyright notice in the Description page of Project Settings.

 #pragma once
 
 #include "CoreMinimal.h"
 #include "GameFramework/Actor.h"
 #include "Components/ArrowComponent.h"
 
 #include "MyTDS/FuncLibrary/Types.h"
 #include "MyTDS/Weapons/Projectiles/ProjectileDefault.h" //не смотря на то, что серый - он используется!!!!!!!
 #include "WeaponDefault.generated.h"
 
 
 //ToDo Delegate on event weapon fire - Anim char, state char...
 DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponFireStart, UAnimMontage*, Animator);
 DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponReloadStart, UAnimMontage*, Anim);
 DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnWeaponReloadEnd, bool, bIsSuccess, int32, AmmoSafe);//AmmoSafe -ск-ко патронов осталось
 
 UCLASS()
 class MYTDS_API AWeaponDefault : public AActor
 
 {
 	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWeaponDefault();

	FOnWeaponFireStart OnWeaponFireStart;
	FOnWeaponReloadStart OnWeaponReloadStart;
	FOnWeaponReloadEnd OnWeaponReloadEnd;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class USceneComponent* SceneComponent = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class USkeletalMeshComponent* SkeletalMeshWeapon = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class UStaticMeshComponent* StaticMeshWeapon = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class UArrowComponent* ShootLocation = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Info")
	FWeaponInfo WeaponSetting;

	//Replicated!
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "Weapon Info")//Replicated
	FAdditionalWeaponInfo AdditionalWeaponInfo; // кол-во патронов, которое осталось в ОРУЖИИ
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FireLogic")
	FName IdWeaponName;
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Tick func
	virtual void Tick(float DeltaTime) override;

	void FireTick(float DeltaTime);
	void ReloadTick(float DeltaTime);
	void DispersionTick(float DeltaTime);
	void ClipDropTick(float DeltaTime);
	void ShellDropTick(float DeltaTime);


	void WeaponInit();
	
	UFUNCTION(Server, Reliable, BlueprintCallable)
	void SetWeaponStateFire_OnServer(bool bIsFire);//OnServer

	bool CheckWeaponCanFire();

	FProjectileInfo GetProjectile(); // функция для доступа к классу пули - BP_WeaponProjectileRifle

	void Fire();

	//NET 
	UFUNCTION(Server, Reliable)
	void UpdateStateWeapon_OnServer(EMovementState NewMovementState);
	
	void ChangeDispersionByShot();
	float GetCurrentDispersion() const;
	FVector ApplyDispersionToShoot(FVector DirectionShoot)const;

	FVector GetFireEndLocation()const;
	int8 GetNumberProjectileByShot() const;

	//Timers'flags
	float FireTimer = 0.0f; //Если timer -значит флаг! 	//Пули это -Round
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ReloadLogic")
	float ReloadTimer = 0.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ReloadLogicDebug")
	//Remove !!! Debug
	float ReloadTime = 0.0f;

	UFUNCTION(BlueprintCallable)
	int32 GetWeaponRound(); // получить количество пуль

	void InitReload();
	void FinishReload();
	void CancelReload();

	UFUNCTION(BlueprintCallable)
	bool CheckCanWeaponReload();
	int8 GetAviableAmmoForReload();

	//Flags
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FireLogic")
	bool WeaponFiring = false;


 	
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "ReloadLogic")
	bool WeaponReloading = false;

	bool WeaponAiming =false;
	bool BlockFire = false;
	
	//Dispersion
	//UFUNCTION(Replicated) -можно и добвалять - поскольку клиенту об этом знать не надо, сервер отвечает за стрельбу
	bool ShouldReduceDispersion = false;
	
	float CurrentDispersion = 0.0f;
	float CurrentDispersionMax = 1.0f;
	float CurrentDispersionMin = 0.1f;
	float CurrentDispersionRecoil = 0.1f;
	float CurrentDispersionReduction = 0.1f;

	//Timer drop magazin on reloud
	bool DropClipFlag = false;
	float DropClipTimer = -1.0f;
	
	// float CastomMass = 0.f;
	// float PowerImpulse = 0.f;

	//Shell flag
	bool DropShellFlag = false;
	float DropShellTimer = -1.0f;

	//Replicated!
	UPROPERTY(Replicated)
	FVector ShootEndLocation = FVector(0);
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
	bool ShowDebug = false;
	// UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
	// bool byBarell = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
	float SizeVectorToChangeShootDirectionLogic = 100.f;
	
	UFUNCTION(Server, Reliable)
	void InitDropMesh_OnServer(UStaticMesh* DropMesh,
		FTransform Offset,
		FVector DropImpulseDirection,
		float LifeTimeMesh,
		float ImpulseRandomDispersion,
		float PowerImpulse,
		float CastomMass);

	//Net
	UFUNCTION(Server, Unreliable)//обязательно - недостоверная
	void UpdateWeaponByCharacterMovementState_OnServer(FVector NewShootEndLocation, bool NewShouldReduceDispersion);

	UFUNCTION(NetMulticast, Unreliable)//обязательно - недостоверная
	void AnimWeaponStart_Multicast(UAnimMontage* Anim);

 	UFUNCTION(NetMulticast, Unreliable)//обязательно - недостоверная
	void ShellDropFire_Multicast(UStaticMesh* DropMesh,
		FTransform Offset,
		FVector DropImpulseDirection,
		float LifeTimeMesh,
		float ImpulseRandomDispersion,
		float PowerImpulse,
		float CastomMass,
		FVector LocalDir);

 	UFUNCTION(NetMulticast, Unreliable)
 	void FXWeaponFire_Multicast(UParticleSystem* FxFire, USoundBase* SoundFire);
 	
 	UFUNCTION(NetMulticast, Reliable)
 	void SpawnTraceHitDecal_Multicast(UMaterialInterface* DecalMaterial, FHitResult HitResult);
 	UFUNCTION(NetMulticast, Reliable)
 	void SpawnTraceHitFX_Multicast(UParticleSystem* FxTemplate, FHitResult HitResult);
 	UFUNCTION(NetMulticast, Reliable)
 	void SpawnTraceHitSound_Multicast(USoundBase* HitSound, FHitResult HitResult);

 	UFUNCTION(NetMulticast,Reliable)
 	void LineTrace_Multicast(FHitResult HitResult, const FVector& SpawnLocation, const FVector& EndLocation);
};