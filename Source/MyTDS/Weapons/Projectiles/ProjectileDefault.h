// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/SphereComponent.h"
#include "Particles/ParticleSystemComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"

#include "MyTDS/FuncLibrary/Types.h"
#include "ProjectileDefault.generated.h"

UCLASS()
class MYTDS_API AProjectileDefault : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AProjectileDefault();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class UStaticMeshComponent* BulletMesh = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class USphereComponent* BulletCollisionSphere = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class UProjectileMovementComponent* BulletProjectileMovement = nullptr; //bullet -пуля
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class UParticleSystemComponent* BulletFX = nullptr;
	UPROPERTY(BlueprintReadOnly)
	FProjectileInfo ProjectileSetting;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
	void InitProjectile(FProjectileInfo InitParam);

	//входные данные описаны эпиками
	UFUNCTION()
	virtual void BulletCollisionSphereHit(class UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);
	UFUNCTION()
	void BulletCollisionSphereBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
	void BulletCollisionSphereEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	UFUNCTION()
	virtual void ImpactProjectile();

	//Multicast
	 UFUNCTION(NetMulticast, Reliable)
	 void InitVisualMeshProjectile_Multicast(UStaticMesh* NewMesh, FTransform MeshRelative);
	 UFUNCTION(NetMulticast, Reliable)
	 void InitVisualTrailProjectile_Multicast(UParticleSystem* NewTamplate, FTransform TemplateRelative);
	
	 UFUNCTION(NetMulticast, Reliable)
	 void SpawnHitDecal_Multicast(UMaterialInterface* DecalMateril, UPrimitiveComponent* OtherComponent, FHitResult HitResult);
	 UFUNCTION(NetMulticast, Reliable)
	 void SpawnHitFX_Multicast(UParticleSystem* FxTemplate, FHitResult HitResult);
	 UFUNCTION(NetMulticast, Reliable)
	 void SpawnHitSound_Multicast(USoundBase* HitSound, FHitResult HitResult);

	//Настраиваем получение скорости из таблицы
	//Если у нас появится базука, которая имеет свойтство ускоряться - для нее нужен параметр MaxSpeed
	UFUNCTION(NetMulticast, Reliable)
	void InitVelosity_Multicast(float InitSpeed, float MaxSpeed);

	/** Update velocity - typically from ReplicatedMovement, not called for simulated physics! */
	virtual void PostNetReceiveVelocity(const FVector& NewVelocity) override;

};
