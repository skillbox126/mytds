#include "ProjectileDefault.h"
//#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Kismet/GameplayStatics.h"
//Для ИИ!
#include "Perception/AISense_Damage.h"
#include "Net/UnrealNetwork.h"
//#include "MyTDS/Interface/MyTDS_IGameActor.h"

// Sets default values
AProjectileDefault::AProjectileDefault()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//NET!!!
	//SetReplicates(true);
	bReplicates = true;

	BulletCollisionSphere = CreateDefaultSubobject<USphereComponent>(TEXT("Collision Sphere"));

	BulletCollisionSphere->SetSphereRadius(16.f);
	
	BulletCollisionSphere->bReturnMaterialOnMove = true; //hit event return physMaterial

	BulletCollisionSphere->SetCanEverAffectNavigation(false); //collision not affect navigation (P keybord on editor)

	RootComponent = BulletCollisionSphere;

	BulletMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Bullet Projectile Mesh"));
	BulletMesh->SetupAttachment(RootComponent);
	BulletMesh->SetCanEverAffectNavigation(false); //collision not affect navigation (P keybord on editor)

	BulletFX = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Bullet FX"));
	BulletFX->SetupAttachment(RootComponent);

	//BulletSound = CreateDefaultSubobject<UAudioComponent>(TEXT("Bullet Audio"));
	//BulletSound->SetupAttachment(RootComponent);

	
	BulletProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("Bullet ProjectileMovement"));
	BulletProjectileMovement->UpdatedComponent = RootComponent;

	//E,bhftv b ltkftv d ,k.ghbynt!
	// BulletProjectileMovement->InitialSpeed = 1.f;
	// BulletProjectileMovement->MaxSpeed = 0.f;

	BulletProjectileMovement->bRotationFollowsVelocity = true;//Должен ли он разворачиваться в сторону ускорения
	BulletProjectileMovement->bShouldBounce = true;//Должен ли он быть шариком -пригодится для гранатомета
}

// Called when the game starts or when spawned
void AProjectileDefault::BeginPlay()
{
	Super::BeginPlay();
	// когда мы делаем эти ивенты -мы создаем делегат, который относится к AProjectileDefault
	BulletCollisionSphere->OnComponentHit.AddDynamic(this, &AProjectileDefault::BulletCollisionSphereHit);
	BulletCollisionSphere->OnComponentBeginOverlap.AddDynamic(this, &AProjectileDefault::BulletCollisionSphereBeginOverlap);
	BulletCollisionSphere->OnComponentEndOverlap.AddDynamic(this, &AProjectileDefault::BulletCollisionSphereEndOverlap);
}

// Called every frame
void AProjectileDefault::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AProjectileDefault::InitProjectile(FProjectileInfo InitParam)
{
	
	
	//BulletProjectileMovement->InitialSpeed = InitParam.ProjectileInitSpeed;//задаем начальную скорость компоненту пули

	//Не работает, т.к InitialSpeed назначается еще в конструктре. А работает так: InitialSpeed берется, там, где мы//
	// выставили по дефолту (из блюпринта), отрабатывает на стороне клиента, а  на стороне сервера отрабатывает
	// дефолтная скорость (из блюпринта), и когда уже пуля начинает лететь - мы ее ограничиваем  - MaxSpeed, что не верно!!!
	//Поэтому в конце ф-ии вставляем:
	////InitVelosity_Multicast(InitParam.ProjectileInitSpeed, InitParam.ProjectileMaxSpeed);
	///
	///
	///!!!!Надо иметь ввиду, что вначале InitSpeed and MaxSpeed = 0! , а уже в 1-м кадре они берут свои значения из таблицы!

	//Тоже поэтому убираем:
	//BulletProjectileMovement->MaxSpeed = InitParam.ProjectileMaxSpeed;

	this->SetLifeSpan(InitParam.ProjectileLifeTime);
	if (InitParam.ProjectileStaticMesh)//BulletMesh и BulletFX -визуальные компоненты -убираются, если не use
	{
		// BulletMesh->SetStaticMesh(InitParam.ProjectileStaticMesh);
		// BulletMesh->SetRelativeTransform(InitParam.ProjectileStaticMeshOffset);
		InitVisualMeshProjectile_Multicast(InitParam.ProjectileStaticMesh, InitParam.ProjectileStaticMeshOffset);
	}
	else
		BulletMesh->DestroyComponent();

	if (InitParam.ProjectileTrailFx)
	{
		// BulletFX->SetTemplate(InitParam.ProjectileTrailFx);
		// BulletFX->SetRelativeTransform(InitParam.ProjectileTrailFxOffset);
		InitVisualTrailProjectile_Multicast(InitParam.ProjectileTrailFx, InitParam.ProjectileTrailFxOffset);
	}
	else
		BulletFX->DestroyComponent();

	InitVelosity_Multicast(InitParam.ProjectileInitSpeed, InitParam.ProjectileMaxSpeed);
	
	ProjectileSetting = InitParam;//к сожалению нельзя реплицировать -т.к. внутри структуры есть ТМапа - а она не реплицируется!
	//Можно конечно вытащить все ключи, вытащить все значения в ТМапе в виде массивов, передать их по РПЦ методу на сервер
	//или на клиент и там уже на той стороне - собрать эту структуру. Но не хочется - сделаем в виде 3-х функций.
}

//когда проходит какой-либо ивент на хит -мы куда-то попали
void AProjectileDefault::BulletCollisionSphereHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (OtherActor && Hit.PhysMaterial.IsValid())//проверяем, что мы во что-то попали и -есть ли физич. материал
    	{
    		EPhysicalSurface mySurfacetype = UGameplayStatics::GetSurfaceType(Hit);//берем тип материала
    
    		if (ProjectileSetting.HitDecals.Contains(mySurfacetype))//проверяем  - содержит ли наша Тмапа такой тип: меиалл, дерево итд
    		{
    			//если да -мы берем ключ на Т-мапу mySurfacetype
    			UMaterialInterface* myMaterial = ProjectileSetting.HitDecals[mySurfacetype];
    
    			if (myMaterial && OtherComp)//проверяем  -есть ли материал и  физический компонент у материала
    			{
					SpawnHitDecal_Multicast(myMaterial, OtherComp, Hit);
    				
    				// UGameplayStatics::SpawnDecalAttached(myMaterial, //спауним декаль из материала
    				// 	FVector(20.0f),
    				// 			OtherComp,//куда аттачим,
    				// 			NAME_None,// NAME_None -без кости,
    				// 	Hit.ImpactPoint,//-туда, куда попали,
    				// 	Hit.ImpactNormal.Rotation(), // разворачиваем ротацию по нормали
    				// 	EAttachLocation::KeepWorldPosition,//-условия аттача
    				// 	10.0f); //10.0 - сколько сек живет декаль
    			}
    		}
    		if (ProjectileSetting.HitFXs.Contains(mySurfacetype))//то же самое с частицами HitFXs
    		{
    			UParticleSystem* myParticle = ProjectileSetting.HitFXs[mySurfacetype];
    			if (myParticle)
    			{
    				 SpawnHitFX_Multicast(myParticle, Hit);
    				//UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), myParticle, FTransform(Hit.ImpactNormal.Rotation(), Hit.ImpactPoint, FVector(1.0f)));
    			}
    		}
    			
    		if (ProjectileSetting.HitSound)//если есть звук 
    		{
    			SpawnHitSound_Multicast(ProjectileSetting.HitSound, Hit);
    			//UGameplayStatics::PlaySoundAtLocation(GetWorld(), ProjectileSetting.HitSound, Hit.ImpactPoint);
    		}


		// IMyTDS_IGameActor* myInterface  = Cast<IMyTDS_IGameActor>(Hit.GetActor());
		// if(myInterface)
		// {
			//Логика: у того, куда мы попали есть интерфейс и тогда мы можем создавать эффекты,
			//если в этом интерфейсе мы молучаем корректный тип Surface

			//Функция из Tipes.cpp Интерфейс пока не используем!
			UTypes::AddEffectBySurfaceType(Hit.GetActor(), Hit.BoneName, ProjectileSetting.Effect, mySurfacetype);
		//}

		
    	}
		//ApplyDamage - для эксперимента, чтобы показать как это работает
		UGameplayStatics::ApplyPointDamage(OtherActor, ProjectileSetting.ProjectileDamage, Hit.TraceStart, Hit, GetInstigatorController(), this, NULL);	
    	//UGameplayStatics::ApplyDamage(OtherActor, ProjectileSetting.ProjectileDamage, GetInstigatorController(), this, nullptr);

	//Делаем для ИИ!
	//в функции в 1-й Hit.Location можно вставить локацию, откуда вылетела пуля - легко сделать на бегинПлей
	// Не забыть добавить такой же код на ShotGun для трейсов и на Grenade
	UAISense_Damage::ReportDamageEvent(GetWorld(), Hit.GetActor(), GetInstigator(), ProjectileSetting.ProjectileDamage, Hit.Location, Hit.Location);


		ImpactProjectile();	
    	//UGameplayStatics::ApplyRadialDamageWithFalloff()
    	//Apply damage cast to if char like bp? //OnAnyTakeDmage delegate
    	//UGameplayStatics::ApplyDamage(OtherActor, ProjectileSetting.ProjectileDamage, GetOwner()->GetInstigatorController(), GetOwner(), NULL);
    	//or custom damage by health component
}

void AProjectileDefault::BulletCollisionSphereBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
}

void AProjectileDefault::BulletCollisionSphereEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
}

void AProjectileDefault::ImpactProjectile()
{
	this->Destroy();
}


//Multicast

 void AProjectileDefault::InitVisualMeshProjectile_Multicast_Implementation(UStaticMesh* NewMesh,
                                                                            FTransform MeshRelative)
 {
 	BulletMesh->SetStaticMesh(NewMesh);
 	BulletMesh->SetRelativeTransform(MeshRelative);
 }

 void AProjectileDefault::InitVisualTrailProjectile_Multicast_Implementation(UParticleSystem* NewTamplate,
 	FTransform TemplateRelative)
 {
 	BulletFX->SetTemplate(NewTamplate);
 	BulletFX->SetRelativeTransform(TemplateRelative);
 }


 void AProjectileDefault::SpawnHitDecal_Multicast_Implementation(UMaterialInterface* DecalMateril,
 	UPrimitiveComponent* OtherComponent, FHitResult HitResult)
 {
 	UGameplayStatics::SpawnDecalAttached(DecalMateril, //спауним декаль из материала
 						FVector(20.0f),
 								OtherComponent,//куда аттачим,
 								NAME_None,// NAME_None -без кости,
 						HitResult.ImpactPoint,//-туда, куда попали,
 						HitResult.ImpactNormal.Rotation(), // разворачиваем ротацию по нормали
 						EAttachLocation::KeepWorldPosition,//-условия аттача
 						10.0f); //10.0 - сколько сек живет декаль
 }

 void AProjectileDefault::SpawnHitFX_Multicast_Implementation(UParticleSystem* FxTemplate, FHitResult HitResult)
 {
 	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(),
 						FxTemplate,
 						FTransform(HitResult.ImpactNormal.Rotation(),
 							HitResult.ImpactPoint,
 							FVector(1.0f)));

 }

 void AProjectileDefault::SpawnHitSound_Multicast_Implementation(USoundBase* HitSound, FHitResult HitResult)
 {
 	UGameplayStatics::PlaySoundAtLocation(GetWorld(), HitSound, HitResult.ImpactPoint);
 }

//Настраиваем получение скорости из таблицы - это RPC вызов!
void AProjectileDefault::InitVelosity_Multicast_Implementation(float InitSpeed, float MaxSpeed)
{
	if (BulletProjectileMovement)
	{
		//GetActorForwardVector - самой пули 
		BulletProjectileMovement->Velocity = GetActorForwardVector() * InitSpeed;
		BulletProjectileMovement->MaxSpeed = MaxSpeed;
		BulletProjectileMovement->InitialSpeed = InitSpeed;
	}
}

//Эпиковская -берем из Actor.h and Actor.cpp - та м они пустые, а мы делаем так:
void AProjectileDefault::PostNetReceiveVelocity(const FVector& NewVelocity)
{
	if (BulletProjectileMovement)
	{
		BulletProjectileMovement->Velocity = NewVelocity;
	}
}
