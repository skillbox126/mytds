// Fill out your copyright notice in the Description page of Project Settings.


#include "ProjectileDefault_Grenade.h"
#include "Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h"
#include "../../../../../../../../../Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX12.3.sdk/usr/include/sys/_types/_null.h"

//вводим консольнeую команду и вводим ее переменную "TPS.DebugExplode", и по этому флагу
//DebugExplodeShow будет что-то работать. ППЦ!!! 0- отkлючён, 1 -включён
int32 DebugExplodeShow = 0;
FAutoConsoleVariableRef CVareExplodeShow(
	TEXT ("MyTDS.DebugExplode"),
	DebugExplodeShow,
	TEXT ("Draw Debug for Explode"),
	ECVF_Cheat //ECVF_Cheat = 0x1,
	/**
	* Console variables cannot be changed by the user (from console).
	* Changing from C++ or ini is still possible.**/
	);

void AProjectileDefault_Grenade::BeginPlay()
{
	Super::BeginPlay();
	
}

void AProjectileDefault_Grenade::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	TimerExplose(DeltaTime);


}

void AProjectileDefault_Grenade::TimerExplose(float DeltaTime)
{
	if (TimerEnabled)
	{
		if (TimerToExplose > TimeToExplose)
		{
			//Explose
			Explose();
			
		}
		else
		{
			TimerToExplose += DeltaTime;
		}
	}
}

void AProjectileDefault_Grenade::BulletCollisionSphereHit(UPrimitiveComponent* HitComp, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	//Если TimerEnabled отключен, В тот момент, когда пуля врежется во что-либо - она взорвется,
	//если нет -будет вести себя как граната
	if (!TimerEnabled)
	{
		Explose();
	}
	Super::BulletCollisionSphereHit(HitComp, OtherActor, OtherComp, NormalImpulse, Hit);
}

void AProjectileDefault_Grenade::ImpactProjectile()
//В стандарте -функция для того, чтобы отработать тогда, когда пуля во что-то врезается -там просто дестрой
// а в гранате включаем таймер взрывателя
{
	//Init Grenade
	TimerEnabled = true;
}

void AProjectileDefault_Grenade::Explose()//когда таймер отрабатывает 5 сек -эффект взрыва
{
	if(DebugExplodeShow)
	{ 
		DrawDebugSphere(GetWorld(), GetActorLocation(), ProjectileSetting.ProjectileMinRadiusDamage, 12, FColor::Red,false, 12.0f);
		DrawDebugSphere(GetWorld(), GetActorLocation(), ProjectileSetting.ProjectileMaxRadiusDamage, 12, FColor::Green,false, 12.0f);
	}
	TimerEnabled = false;
	if (ProjectileSetting.ExploseFX)
	{
		//UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ProjectileSetting.ExploseFX, GetActorLocation(), GetActorRotation(), FVector(1.0f));
		SpawnExplodeFX_Multicast(ProjectileSetting.ExploseFX);
	}
	if (ProjectileSetting.ExploseSound)
	{
		//UGameplayStatics::PlaySoundAtLocation(GetWorld(), ProjectileSetting.ExploseSound, GetActorLocation());
		SpawnExplodeSound_Multicast(ProjectileSetting.ExploseSound);
	}


	TArray<AActor*> IgnoredActor; //массив Акторов, котрые должны заигнорить взрыв -Список актеров, которых следует игнорировать
	UGameplayStatics::ApplyRadialDamageWithFalloff(GetWorld(),//серверная функция, которая позволяет общаться всем со всеми
		ProjectileSetting.ExploseMaxDamage,
		ProjectileSetting.ExploseMaxDamage*0.2f,
		GetActorLocation(),//локация, где центр взрыва и далее внутренний Радиус зоны полного поражения и внешний Радиус минимальной зоны поражения - радиус взрыва
		ProjectileSetting.ProjectileMaxRadiusDamage/2, 
		ProjectileSetting.ProjectileMaxRadiusDamage,
		5,//коэф-т уменьшения урона
		nullptr, //Класс, описывающий нанесенный ущерб.
		IgnoredActor,//Список актеров, которых следует игнорировать
		this,//Актер, который на самом деле причинил ущерб (например, взорвавшаяся граната)
		nullptr//Контроллер, ответственный за нанесение этого урона (например, игрок, бросивший гранату)
		);

	this->Destroy();
}

void AProjectileDefault_Grenade::SpawnExplodeFX_Multicast_Implementation(UParticleSystem* FxTemplate)
{
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), FxTemplate, GetActorLocation(), GetActorRotation(), FVector(1.0f));
}

void AProjectileDefault_Grenade::SpawnExplodeSound_Multicast_Implementation(USoundBase* ExplodeSound)
{
	UGameplayStatics::PlaySoundAtLocation(GetWorld(), ExplodeSound, GetActorLocation());
}

