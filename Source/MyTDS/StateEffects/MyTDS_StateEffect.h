#pragma once

#include "CoreMinimal.h"
#include "Particles/ParticleSystemComponent.h"
#include "MyTDS_StateEffect.generated.h"

/**
 *Это простой OBJECT!
 * Бафы и дебафы будут состоять из файла StateEffect - это простой UObject - прародитель всех классов
 */
UCLASS(Blueprintable, BlueprintType)
class MYTDS_API UMyTDS_StateEffect : public UObject
{
	GENERATED_BODY()
public:
	//Системы эффектов:
	//1-я система отвечает за то, на что можно нацепить баф или дебаф -будет проверять возможность этого
	//скорее всегго по типу поверхности + проверку на стаковость - получение персонажем схожих эффектов.
	//2-я система будет описывать поведение того или иного эффекта -взаимодействие с жизнями.
	//Эффектами будут - проджектайл или трейс и взрыв от гранаты в момент, когда происходит AplayDamage

//Это говорит о том, что объект является реплицируемым
	bool IsSupportedForNetworking()const override {return true;};//означает, что на объект можно ссылаться по сети
	//создали объект - класс -будем проверять по классу
	virtual bool InitObject(AActor* Actor, FName NameBoneHit);

	//удаляем объект
	virtual void DestroyObject();

	//Массив EPhysicalSurface- тайпов способных наложиться туда, куда мы попали: Wood, Flash итд
	//Получается массив ТМап только  одними ключами
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
	TArray<TEnumAsByte<EPhysicalSurface>> PossibleInteractSurface;

	//Проверяем на стаковость
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
	bool bIsStakable = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting ExecuteTimer")
	UParticleSystem* ParticleEffect = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting ExecuteTimer")
	bool bIsAutoDestroyParticleEffect = false;

	// UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting ExecuteTimer")
	// TArray<UParticleSystem*> ParticleEffect;

	AActor* myActor = nullptr;
	
	UPROPERTY(Replicated)
	FName NameBone;

	UFUNCTION(NetMulticast, Reliable)
	void FXSpawnByStateEffect_Multicast(UParticleSystem* Effect, FName NameBoneHit);
};



// 1) ExecuteOnce - для АПТЕЧКИ?!!!

//Создаем класс, который наследуется от основного -он создает эффект, который
// будет создаваться 1 раз. Пока это делаем для АПТЕЧКИ!!! -1 раз дает здоровье
UCLASS()
class  MYTDS_API UMyTDS_StateEffect_ExecuteOnce : public  UMyTDS_StateEffect
{
	GENERATED_BODY()

public:
	bool InitObject(AActor* Actor, FName NameBoneHit) override;
	void DestroyObject() override;	

	//эта функция virtual - потому, что если в дальнейшем у нее появятся чайлды
	// мы сможем ее заовверрайдить!
	virtual void ExecuteOnce();

	//Power - если + то лечилка, если минус -то можно что еще придумать
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Execute Once")
	float Power = 20.0f;
};



// 2) эффект по таймеру ОГОНЬ!!!



UCLASS()
class MYTDS_API  UMyTDS_StateEffect_ExecuteTimer : public UMyTDS_StateEffect
{
	GENERATED_BODY()

public:
	bool InitObject(AActor* Actor, FName NameBoneHit) override;
	void DestroyObject() override;

	virtual void Execute();

	//можно было Power назватьпо другому
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting ExecuteTimer")
	float Power = 20.0f;
	//Каждую RateTime = 1 сек в течении Timer = 5 сек будет отрабатывать таймер - вызывать ф-ю Execute()
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting ExecuteTimer")
	float Timer = 5.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting ExecuteTimer")
	float RateTime = 1.0f;

	FTimerHandle TimerHandle_ExecuteTimer;
	FTimerHandle TimerHandle_EffectTimer;
	
	// UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting ExecuteTimer")
	// TArray<UParticleSystem*> ParticleEffect;

	//эта ссылка нужна, чтобы уничтожить эффект
	//UParticleSystemComponent* ParticleEmitter;
};



	// 3) эффект по таймеру Invulnerability - Неуяуязвимость



	 UCLASS()
	 class MYTDS_API  UMyTDS_StateEffect_Invulnerability : public UMyTDS_StateEffect
	 {
	 	GENERATED_BODY()
	 
	 public:
	 	bool InitObject(AActor* Actor, FName NameBoneHit) override;
	 	void DestroyObject() override;

	 	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Execute Once")
	 	float Health = 100.0f;
	 
	 	virtual void UInvulnerabilityExecute();
	 
	 	// UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting ExecuteTimer")
	 	// float Power = 20.0f;
	 	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting ExecuteTimer")
	 	float Timer = 5.0f;
	 	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting ExecuteTimer")
	 	float RateTime = 1.0f;
	 
	 	FTimerHandle TimerHandle_ExecuteTimer;
	 	FTimerHandle TimerHandle_EffectTimer;
	 
	 	// UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting ExecuteTimer")
	 	// TArray<UParticleSystem*> MyParticleEffects;
	 
	 	//эта ссылка нужна, чтобы уничтожить эффект
	 	// UParticleSystemComponent* ParticleEmitter;
	 };