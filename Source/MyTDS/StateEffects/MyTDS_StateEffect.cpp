// Fill out your copyright notice in the Description page of Project Settings.


#include "MyTDS/StateEffects/MyTDS_StateEffect.h"
#include "MyTDS/Character/MyTDSHealthComponent.h"
#include "MyTDS/Interface/MyTDS_IGameActor.h"
#include "Kismet/GameplayStatics.h"
#include "Net/UnrealNetwork.h"

//Родитель - инициализация
bool UMyTDS_StateEffect::InitObject(AActor* Actor, FName NameBoneHit)
{
	myActor = Actor;
	NameBone = NameBoneHit;
	UE_LOG(LogTemp, Warning, TEXT("InitObject Основной"));

	
	//Если каст на интерфейса на актор  проходит, то наш объет принимает эти эффекты и мы можем зарегистрировать себя в нём!
	IMyTDS_IGameActor* myInterface = Cast<IMyTDS_IGameActor>(myActor);
    	if (myInterface)
    	{
    		//меняем AddEffect(this) на Execute_AddEffect(myActor, this) - иначе ошибка
    		//т.е, передаем - в каком месте этот интерфейс должен вызываться - myActor
    		//отсюда приходит ивент - и идет в Чар!
    		myInterface->Execute_AddEffect(myActor, this);
    	//	myInterface->Execute_AddEffect(myActor,this);

    	}
	return true;
}

//Родитель уничтожено
void UMyTDS_StateEffect::DestroyObject()
{
	IMyTDS_IGameActor* myInterface = Cast<IMyTDS_IGameActor>(myActor);
	if (myInterface)
	{
		myInterface->Execute_RemoveEffect(myActor, this);
	}
	//Перед тем как передаем Актор -чистим все ссылки, которые он содержал
	myActor = nullptr;
	if (this && this->IsValidLowLevel())
	{
		//Вызывается перед уничтожением объекта. Это вызывается сразу после принятия решения
		//об уничтожении объекта, чтобы позволить объекту начать асинхронный процесс очистки.
		this->ConditionalBeginDestroy();
	}
}




// 1) ExecuteOnce - 1 раз АПТЕЧКА на клавишу P

//ExecuteOnce инициализация
bool UMyTDS_StateEffect_ExecuteOnce::InitObject(AActor* Actor, FName NameBoneHit)
{
	Super::InitObject(Actor, NameBoneHit);
	ExecuteOnce();
	UE_LOG(LogTemp, Warning, TEXT("InitObject-ExecuteOnce  "));

	return true;
}

//ExecuteOnce выполнено
void UMyTDS_StateEffect_ExecuteOnce::ExecuteOnce()
{
	if (myActor)
	{
		//здесь myActor - класс здоровья! Кастуемся на компонент класса здоровья!
		UMyTDSHealthComponent* myHealthComp = Cast<UMyTDSHealthComponent>(myActor->GetComponentByClass(UMyTDSHealthComponent::StaticClass()));
		if (myHealthComp)
		{
			myHealthComp->ChangeHealthValue_OnServer(Power);//На сЕрверЕ!!!Power  -на сколько увеличиваем здоровье если + (лечилка)
		}
	}
	DestroyObject();
}

//ExecuteOnce уничтожено
void UMyTDS_StateEffect_ExecuteOnce::DestroyObject()
{
	Super::DestroyObject();
}


// 2) ExecuteTimer - по таймеру -
// Для Чара появляется огонь на теле после попадания и через 5 сек гаснет



//ExecuteTimer Инициализация
bool UMyTDS_StateEffect_ExecuteTimer::InitObject(AActor* Actor, FName NameBoneHit)
{
	Super::InitObject(Actor, NameBoneHit);

	if (GetWorld())
	{
		//создаем таймеры. 1 -й незациклен, 2-й -да. Функции DestroyObject и Execute пишутся без скобок()!
		//Через Timer=5 сек он будет DestroyObject и в этот же момент создаст еще один таймер
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_EffectTimer, this, &UMyTDS_StateEffect_ExecuteTimer::DestroyObject, Timer, false);
		//каждые RateTime = 1 сек он будет вызывать Execute - изменять здоровье на Power
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_ExecuteTimer, this, &UMyTDS_StateEffect_ExecuteTimer::Execute, RateTime, true);
	}
		
		return true;
}

//ExecuteTimer Здесь лечилка для Чара на Power при нажатии P
void UMyTDS_StateEffect_ExecuteTimer::Execute()
{
	if (myActor)
	{
		//UGameplayStatics::ApplyDamage(myActor,Power,nullptr,nullptr,nullptr);	//можно и так
		UMyTDSHealthComponent* myHealthComp = Cast<UMyTDSHealthComponent>(myActor->GetComponentByClass(UMyTDSHealthComponent::StaticClass()));
		if (myHealthComp)
		{
			myHealthComp->ChangeHealthValue_OnServer(Power);
		//	myHealthComp->SetCurrentHealth(100);
		}
	}
}

//ExecuteTimer уничтожено
void UMyTDS_StateEffect_ExecuteTimer::DestroyObject()
{
	if (GetWorld())
	{
		//Должны унистожить таймер, потому что он живет отдельно и незвисимо от того, есть Стейтэффект или нет
		//и если мы уничтожим объект то ф-я  exucute, которая минусовала жизни у актора meActor не отрабатывала, т.к.
		//самого актора не существовало 
		GetWorld()->GetTimerManager().ClearAllTimersForObject(this);
	}
	
	 Super::DestroyObject();//супер потом- т.к. сначала уничтожим свое, а потом и головной ф-ии
}


// 3) UInvulnerability на клавишу L



//UInvulnerability Инициализация
 bool UMyTDS_StateEffect_Invulnerability::InitObject(AActor* Actor, FName NameBoneHit)
 {
 	Super::InitObject(Actor, NameBoneHit);
	UE_LOG(LogTemp, Warning, TEXT("Super InitObject UInvulnerability"));

 	//создаем таймеры. 1 -й незациклен, 2-й -да. Функции DestroyObject и Execute пишутся без скобок()!
 	//Через Timer=5 сек он будет DestroyObject
 	GetWorld()->GetTimerManager().SetTimer(TimerHandle_EffectTimer, this, &UMyTDS_StateEffect_Invulnerability ::DestroyObject, Timer, false);
 	//каждые RateTime = 1 сек он будет вызывать Execute - изменять здоровье на Power
 	GetWorld()->GetTimerManager().SetTimer(TimerHandle_ExecuteTimer, this, &UMyTDS_StateEffect_Invulnerability ::UInvulnerabilityExecute, RateTime, true);
	
 	//выбираем случайный эффект ParticleEffects - новая переменная!!!
 	// UParticleSystem* ParticleCurrentEffect = MyParticleEffects[FMath::RandRange(0, (MyParticleEffects.Num()-1))];
 	// if (ParticleCurrentEffect) 
 	// {
 	// 	// FName GetNameBone();
 	// 	//FName NameBoneToAttached; //прописываем -куда мы хотим прицепить эффект
 	// 	FVector Loc = FVector(0); //и локацию
  //
 	// 	// USkeletalMeshComponent* SkeletalMeshComp = Cast<USkeletalMeshComponent>(myActor->GetComponentByClass(USkeletalMeshComponent::StaticClass()));
 	// 	// SkeletalMeshComp* myMesh = GetMesh();
  //       if (myActor->GetRootComponent())
  //       {
  //       	ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(
		// 	 ParticleCurrentEffect,
		// 	 myActor->GetRootComponent(),
		// 	 NameBoneHit,
		// 	 Loc,
		// 	 FRotator::ZeroRotator,
		// 	 EAttachLocation::SnapToTarget,
		// 	 false);
		// }
  //   }
 	return true;
 }
 
 //UInvulnerability выполнено
 void UMyTDS_StateEffect_Invulnerability::UInvulnerabilityExecute()
 {
 	if (myActor)
 	{
 		//UGameplayStatics::ApplyDamage(myActor,Power,nullptr,nullptr,nullptr);	//можно и так
 		UMyTDSHealthComponent* myHealthComp = Cast<UMyTDSHealthComponent>(myActor->GetComponentByClass(UMyTDSHealthComponent::StaticClass()));
 		if (myHealthComp)
 		{
 			//myHealthComp->ChangeHealthValue_OnServer(Power);
 			myHealthComp->ChangeHealthValue_OnServer(Health);
 			//myHealthComp->SetCurrentHealth(Health);//сделать на сервере
 			//myHealthComp->SetCurrentHealth(Health);
 			UE_LOG(LogTemp, Warning, TEXT("SetCurrentHealth"));

 		}
 	}
 }
 
 //UInvulnerability уничтожено 
 void UMyTDS_StateEffect_Invulnerability::DestroyObject()
 {
 	// ParticleEmitter->DestroyComponent();
 	// ParticleEmitter = nullptr;
 	// Super::DestroyObject();
	if (GetWorld())
	{
		//Должны унистожить таймер, потому что он живет отдельно и незвисимо от того, есть Стейтэффект или нет
		//и если мы уничтожим объект то ф-я  exucute, которая минусовала жизни у актора meActor не отрабатывала, т.к.
		//самого актора не существовало 
		GetWorld()->GetTimerManager().ClearAllTimersForObject(this);
	}
	
	Super::DestroyObject();//супер потом- т.к. сначала уничтожим свое, а потом и головной ф-ии
 }




void UMyTDS_StateEffect::FXSpawnByStateEffect_Multicast_Implementation(UParticleSystem* Effect, FName NameBoneHit)
{
	//ToDo for object with interface create func return offset, Name Bones, 
	//ToDo Random init Effect with available array (For)
	/*if (Effect)
	{
		FName NameBoneToAttached = NameBoneHit;
		FVector Loc = FVector(0);


		USceneComponent* myMesh = Cast<USceneComponent>(myActor->GetComponentByClass(USkeletalMeshComponent::StaticClass()));
		if (myMesh)
		{
			ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(Effect, myMesh, NameBoneToAttached, Loc, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
		}
		else
		{
			ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(Effect, myActor->GetRootComponent(), NameBoneToAttached, Loc, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
		}
	}*/
}



void UMyTDS_StateEffect::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UMyTDS_StateEffect, NameBone);
}