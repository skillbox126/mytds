// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "MyTDS/FuncLibrary/Types.h"
#include "MyTDSInventoryComponent.generated.h"

//Делегат о том, что поменялось оружие
DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnSwitchWeapon, FName, WeaponIdName, FAdditionalWeaponInfo, WeaponAdditionalInfo, int32, NewCurrentIndexWeapon);
// Делегат о том, что патроны поменяли свое значение - EWeaponType - какие патроны и их число
 DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnAmmoChange, EWeaponType, TypeAmmo, int32, Cout);
//Делегат кот. будет обновлять инфу у каждого оружия при каждом выстреле
 DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnWeaponAdditionalInfoChange, int32, IndexSlot, FAdditionalWeaponInfo, AdditionalInfo);
//Делгат о том, что закончились патроны
 DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponAmmoEmpty, EWeaponType, WeaponType);
//Делгат о том, Оружие доступно - т.е. в оружии есть патроны
 DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponAmmoAviable, EWeaponType, WeaponType);
//Делегат о том, что что-то поменялось в слоте - в слот добавили новое оружие
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnUpdateWeaponSlots, int32, IndexSlotChange, FWeaponSlot, NewInfo);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponNotHaveRound, int32, IndexSlotWeapon);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponHaveRound, int32, IndexSlotWeapon);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class MYTDS_API UMyTDSInventoryComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UMyTDSInventoryComponent();

	// Важный делегат! Позволяет навесить все что угодно(оружие, меч) на все что угодно(на сундук например, врага или персонажа)
	//Этот делегат позволяет понять, что оружие начало меняться.
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Inventory")
	FOnSwitchWeapon OnSwitchWeapon;

	//Делегат говорит о том, что наши патроны в ИНВЕНТАРЕ! поменяли свое значение (перезарарядка прошла успешно итд)
	 UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Inventory")
	 FOnAmmoChange OnAmmoChange;
	
	//Делегат на каждый выстрел! -произошел выстрел и кол-во патронов ( в магазине) изменилось
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Inventory")
	 FOnWeaponAdditionalInfoChange OnWeaponAdditionalInfoChange;

	//ДЕЛЕГАТ о том, что в оружии кончились патроны
	UPROPERTY(BlueprintAssignable,  EditAnywhere, BlueprintReadWrite, Category = "Inventory")
	FOnWeaponAmmoEmpty OnWeaponAmmoEmpty;

	//ДЕЛЕГАТ о том, что Оружие доступно - т.е. в оружии есть патроны
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Inventory")
	FOnWeaponAmmoAviable OnWeaponAmmoAviable;

	//Делегат о том, что что-то поменялось в слоте - в слот добавили новое оружие
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Inventory")
	FOnUpdateWeaponSlots OnUpdateWeaponSlots;

	//Event current weapon not have additional_Rounds 
    	UPROPERTY(BlueprintAssignable, Category = "Inventory")
    	FOnWeaponNotHaveRound OnWeaponNotHaveRound;
    	//Event current weapon have additional_Rounds 
    	UPROPERTY(BlueprintAssignable, Category = "Inventory")
    	FOnWeaponHaveRound OnWeaponHaveRound;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;


public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	//NET Массивы реплицируются без проблем. ТМапы - проблема!
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "Weapons")//массив слотов- какое оружие и ск-ко патронов осталось
		TArray<FWeaponSlot> WeaponSlots;
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "Weapons")// массив слотов (структур) - тип оружия, остаток патронов, макс кол-во патронов
		TArray<FAmmoSlot> AmmoSlots;

	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapons") //-сделаем невидимой пока
	int32 MaxSlotsWeapon = 0;

	//TODO OMG Refactoring need!!!
	//Меняем индекс оружия, передаём старый индекс и перекидываем старую информацию об остатке патронов
	//bool SwitchWeaponToIndex(int32 ChangeToIndex, int32 OldIndex, FAdditionalWeaponInfo OldInfo, bool bIsForward);
	bool SwitchWeaponToIndexByNextPreviosIndex(int32 ChangeToIndex, int32 OldIndex, FAdditionalWeaponInfo OldInfo, bool bIsForward);
	bool SwitchWeaponByIndex(int32 IndexWeaponToChange, int32 PreviosIndex, FAdditionalWeaponInfo PreviosWeaponInfo);

	//bool SwitchWeaponToIndex(int32 ChangeToIndex, int32 OldIndex, FAdditionalWeaponInfo OldInfo, bool bIsForward);

	UFUNCTION(BlueprintCallable)
	FAdditionalWeaponInfo GetAdditionalInfoWeapon(int32 IndexWeapon);//вытаскивает инфу об остатке патронов по индексу оружия
	UFUNCTION(BlueprintCallable)
	int32 GetWeaponIndexSlotByName(FName IdWeaponName);//обратная инфа: я держу оружие -какой у него индекс?
	void SetAdditionalInfoWeapon(int32 IndexWeapon, FAdditionalWeaponInfo NewInfo);//записывает инфу об остатке патронов по индексу


	//функция возвращает имя оружия по слоту
	FName GetWeaponNameBySlotIndex(int32 indexSlot);


	//????
	//ищем тип оружия по IdWeaponName
	UFUNCTION(BlueprintCallable, Category = "Interface")
	bool GetWeaponTypeByNameWeapon(FName IdWeaponName, EWeaponType &WeaponType);

	//получаем тип оружия WeaponType по его слоту IndexSlot в WeaponSlots
	UFUNCTION(BlueprintCallable, Category = "Interface")
	bool GetWeaponTypeByIndexSlot(int32 IndexSlot, EWeaponType &WeaponType);
	
	//AmmoTaken -сколько патронов отобрали у инвентаря
	UFUNCTION(BlueprintCallable)
	void AmmoSlotChangeValue(EWeaponType TypeWeapon, int32 CoutChangeAmmo);

	//функция проверяет - есть ли патроны в ИНВЕНТАРЕ у  оружия типа TypeWeapon или нет. Есть - true.
	// и возвращает сссылку на остаток патронов
	UFUNCTION(BlueprintCallable, Category = "Interface")
	bool CheckAmmoForWeapon(EWeaponType TypeWeapon, int32 &AviableAmmoForWeapon);

	
	//Interface PickUp Actors
	//проверяем патроны на возможность поднять патроны -есть ли место для них в инвентаре
	UFUNCTION(BlueprintCallable, Category = "Interface")
	bool CheckCanTakeAmmo(EWeaponType AmmoType);

	//проверяем - есть ли свободные слоты
	UFUNCTION(BlueprintCallable, Category = "Interface")
	bool CheckCanTakeWeapon(int32 &FreeSlot);

	//поменять оружие у инвентаря
	UFUNCTION(BlueprintCallable, Category = "Interface")
	bool SwitchWeaponToInventory(FWeaponSlot NewWeapon, int32 IndexSlot, int32 CurrentIndexWeaponChar, FDropItem &DropItemInfo); 

	//записываем новое оружие
	UFUNCTION(Server, Reliable, BlueprintCallable, Category = "Interface")
	void TryGetWeaponToInventory_onServer(AActor* PickUpActor,  FWeaponSlot NewWeapon); 

	//Сброс оружия по индексу
	UFUNCTION(Server, Reliable, BlueprintCallable, Category = "Interface")
	void DropWeaponByIndex_OnServer(int32 ByIndex);
	
	UFUNCTION(BlueprintCallable, Category = "Interface")
	bool GetDropItemInfoFromInventory(int32 IndexSlot, FDropItem &DropItemInfo);

	UFUNCTION(BlueprintCallable, Category = "Inventory")
	TArray<FWeaponSlot>GetWeaponSlots();

	UFUNCTION(BlueprintCallable, Category = "Inventory")
	TArray<FAmmoSlot>GetAmmoSlots();

	//NET
	//Офигительная функция)) - просто присвоить новые значения из PlayerState
	UFUNCTION(Server, Reliable, BlueprintCallable, Category = "Inventory")
	//Обязательно const and & (операнд)
	void InitInventory_OnServer(const TArray<FWeaponSlot>& NewWeaponSlotsInfo, const TArray<FAmmoSlot>& NewAmmoSlotsInfo);

	//Функция нужна чтобы 1 раз на бегинплей бродкастить винтовку, чтобы выделить ее зеленым цыетом))
	UFUNCTION(Server, Reliable, BlueprintCallable, Category = "Inventory")
	void FirstInitInventory_OnServer(int32 IndexWeapon);

	//NET Делегаты
	UFUNCTION(NetMulticast, Reliable)
	void AmmoChangeEvent_Multicast(EWeaponType TypeWeapon, int32 Cout);

	//SwitchWeapon - делаем на сервере!!! - ни фига - так не работает -пределал на Multicast!!!
	UFUNCTION(NetMulticast, Reliable)
	void SwitchWeaponEvent_Multicast(FName WeaponName, FAdditionalWeaponInfo AdditionalInfo, int32 IndexSlot);//TODO Change FName Type

	UFUNCTION(NetMulticast, Reliable)
	void WeaponAdditionalInfoChangeEvent_Multicast(int32 IndexSlot, FAdditionalWeaponInfo AdditionalInfo);
	UFUNCTION(NetMulticast, Reliable)
	void WeaponAmmoEmptyEvent_Multicast(EWeaponType TypeWeapon);
	UFUNCTION(NetMulticast, Reliable)
	void WeaponAmmoAviableEvent_Multicast(EWeaponType TypeWeapon);
	UFUNCTION(NetMulticast, Reliable)
	void UpdateWeaponSlotsEvent_Multicast(int32 IndexSlotChange, FWeaponSlot NewInfo);
	UFUNCTION(NetMulticast, Reliable)
	void WeaponNotHaveRoundEvent_Multicast(int32 IndexSlotWeapon);
	UFUNCTION(NetMulticast, Reliable)
	void WeaponHaveRoundEvent_Multicast(int32 IndexSlotWeapon);
};
