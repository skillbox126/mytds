// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "MyTDSHealthComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnHealthChange, float, Health, float, Damage);//Объявляем делегат и тип переменной: FOnHealthChange
//DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnHealthChange, float, Health);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDead); //Объявляем делегат и тип переменной: FOnDead

USTRUCT(BlueprintType)
struct FStatsParam
{
	GENERATED_BODY()
	
};

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class MYTDS_API UMyTDSHealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UMyTDSHealthComponent();

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Health")
	FOnHealthChange OnHealthChange;
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Health")
	FOnDead OnDead;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;
// Health  и bIsAlive - protected
	UPROPERTY(Replicated)
	float Health = 100.0f;
	//Определять жив или мертв объект должен компонент! Вводим переменную
	UPROPERTY(Replicated)
	bool bIsAlive = true;

public:

	//Глобальный коэффициент для дамага
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
	float CoefDamage = 1.0f;

	// UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
	// float CoefDamage = 1.0f;
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	// UFUNCTION(BlueprintCallable, Category = "Health")
	// virtual void ResiveDamage(float Damage);

	//Меняем на блюпринтовый
	// UFUNCTION(BlueprintNativeEvent)
	// void DeadIvent();
	
	UFUNCTION(BlueprintCallable, Category = "Health")
	void SetCurrentHealth(float NewHealth);
	UFUNCTION(BlueprintCallable, Category = "Health")
	float GetCurrentHealth();

	//т.к. bIsAlive - protected нужна ф-я для вызова
	UFUNCTION(BlueprintCallable, Category = "Health")
	bool GetIsAlive();
	
	UFUNCTION(Server, Reliable, BlueprintCallable, Category = "Health")
	virtual void ChangeHealthValue_OnServer(float ChangeValue);

	////NET
	UFUNCTION(NetMulticast, Reliable)
	void HealthChangeEvent_Multicast(float newHealth, float Value);

	UFUNCTION(NetMulticast, Reliable)
	void DeadEvent_Multicast();
};
