// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "MyTDS/Interface/MyTDS_IGameActor.h"
#include "MyTDS/StateEffects/MyTDS_StateEffect.h"
#include "MyTDS_EnemyCharacter.generated.h"

UCLASS()
class MYTDS_API AMyTDS_EnemyCharacter : public ACharacter, public IMyTDS_IGameActor
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AMyTDS_EnemyCharacter();

protected:
	//NET
	bool ReplicateSubobjects(class UActorChannel* Channel, class FOutBunch* Bunch, FReplicationFlags* RepFlags) override;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	//EPhysicalSurface GetSurfuceType() override;	
    	
	//TArray<UMyTDS_StateEffect*> GetAllCurrentEffects() override;
	// void RemoveEffect(UMyTDS_StateEffect* RemoveEffect) override;
	// void AddEffect(UMyTDS_StateEffect* newEffect) override;
	
	//Удаляем блюпринт с эффектом или БП эфекта и добавляем
	// добавляем UFUNCTION(BlueprintCallable, BlueprintNativeEvent) приэтом убираем override
	//создаём дубль ф-ии но уже с override, а срр добавлям _Implementation
	//_Implementation: Так мы говорим движку, что помимо блюпринтовой реализации есть еще и и в срр
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void AddEffect(UMyTDS_StateEffect* newEffect);
	void AddEffect_Implementation(UMyTDS_StateEffect* newEffect) override;
	
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void RemoveEffect(UMyTDS_StateEffect* RemoveEffect);
	void RemoveEffect_Implementation(UMyTDS_StateEffect* RemoveEffect) override;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
	TArray<UParticleSystemComponent*> ParticleSystemEffects;
	
	//Effect массив эффектов - дублируем у чара!
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "Setting")
	TArray<UMyTDS_StateEffect*> Effects;
	
	//Переменные для RepNotify - обязательно функции для них!
	UPROPERTY(ReplicatedUsing = EffectAdd_OnRep) 
	UMyTDS_StateEffect* EffectAdd = nullptr;
	UPROPERTY(ReplicatedUsing = EffectRemove_OnRep)
	UMyTDS_StateEffect* EffectRemove = nullptr;
	UFUNCTION()
	void EffectAdd_OnRep();
	UFUNCTION()
	void EffectRemove_OnRep();

	UFUNCTION(Server, Reliable)
	void ExecuteEffectAdded_OnServer(UParticleSystem* ExecuteFX);
	UFUNCTION(Server, Reliable) 
	void ExecuteEffectAdded_Multicast(UParticleSystem* ExecuteFX);

	UFUNCTION()
	void SwitchEffect(UMyTDS_StateEffect* Effect, bool bIsAdd);//bIsAdd -стоит добавлять или нет
};
