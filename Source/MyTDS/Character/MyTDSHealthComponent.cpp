// Fill out your copyright notice in the Description page of Project Settings.


#include "MyTDS/Character/MyTDSHealthComponent.h"
#include "Net/UnrealNetwork.h"

// Sets default values for this component's properties
UMyTDSHealthComponent::UMyTDSHealthComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	//NET - ставим такую -советуют эпики
	SetIsReplicatedByDefault(true);

	// ...
}

// Called when the game starts
void UMyTDSHealthComponent::BeginPlay()
{
	Super::BeginPlay();
	// ...
}

// Called every frame
void UMyTDSHealthComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

float UMyTDSHealthComponent::GetCurrentHealth()
{
	return Health;
}

bool UMyTDSHealthComponent::GetIsAlive()
{
	return bIsAlive; 
}

//Меняем ResiveDamage на ChangeHealthValue_OnServer. ChangeValue будет приходить с минусом
//void UMyTDSHealthComponent::ResiveDamage(float Damage)
//Работает только на сервере!
void UMyTDSHealthComponent::ChangeHealthValue_OnServer_Implementation(float ChangeValue) 
{
	if (bIsAlive)
	{
		ChangeValue = ChangeValue * CoefDamage; //Вввели коэф-т урона
	
		//Ну и самое главное - меняем здоровье:
		Health += ChangeValue;//Плюсум! Т.к. ChangeValue с минусом из блюпринта
		//Сразу бродкастим изменение здоровья
	
		//NET
		//OnHealthChange.Broadcast(Health, ChangeValue);
		HealthChangeEvent_Multicast(Health, ChangeValue);

		if (Health > 100.0f)
		{
			Health = 100.0f;
		}
		else
		{
			if (Health < 0.0f)
			{
				bIsAlive = false; //на сервере поменяли -на всех клиентах - тоже.
				//OnDead.Broadcast();
				DeadEvent_Multicast();
			}
		}
	}
}


void UMyTDSHealthComponent::SetCurrentHealth(float NewHealth)
{
	Health = NewHealth;
}

//NET

void UMyTDSHealthComponent::HealthChangeEvent_Multicast_Implementation(float newHealth, float Value)
{
	OnHealthChange.Broadcast(newHealth, Value);
}

void UMyTDSHealthComponent::DeadEvent_Multicast_Implementation()
{
	OnDead.Broadcast();
}

void UMyTDSHealthComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	//Макросы
	//Говорят о том что (например) в нашем объекте в классе AMyTDSCharacter - MovementState должна быть реплицируемой
	DOREPLIFETIME(UMyTDSHealthComponent, Health);
	DOREPLIFETIME(UMyTDSHealthComponent, bIsAlive);
}