// Fill out your copyright notice in the Description page of Project Settings.


#include "MyTDS/Character/MyTDS_EnemyCharacter.h"
#include "Kismet/GameplayStatics.h"
#include "Net/UnrealNetwork.h"
#include "Engine/ActorChannel.h"

// Sets default values
AMyTDS_EnemyCharacter::AMyTDS_EnemyCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AMyTDS_EnemyCharacter::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AMyTDS_EnemyCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AMyTDS_EnemyCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void AMyTDS_EnemyCharacter::RemoveEffect_Implementation(UMyTDS_StateEffect* RemoveEffect)
{
	Effects.Remove(RemoveEffect);
	if (!RemoveEffect->bIsAutoDestroyParticleEffect)//если не бесконечный -внутрь ф-ии
		{
		//Поэтому вызываем SwitchEffect 
		SwitchEffect(RemoveEffect, false);
		//Это происходит только на сервере. И дальше, поскольку знаем, что RepNotify работает только на клиенте
		//Если мы поменяем эту переменную на новую:
		EffectRemove = RemoveEffect; 
		//соответственно отработает нам нужный RepNotify
		}
}

//Для чегу нужно RPC? Когда сервер добавляет како-то эфект, Repnotify AddEffect_Implementation должен сработать,
//но на клиенте он не успевает сработать, потому что на сервере это эффект уже уничтожится -RemoveEffect_Implementation!
//и просто этот Repnotify не отработает! Поэтому для эффекта, который сам уничтожится такая логика.
void AMyTDS_EnemyCharacter::AddEffect_Implementation(UMyTDS_StateEffect* newEffect)
{
	//Добавляем ээфект в массив -  их будет больше одного c каждым выстрелом
	Effects.Add(newEffect);
	//делаем для тех эффектов -которые не бесконечные, (как огонь!) для них bIsAutoDestroyParticleEffect = true
	if (!newEffect->bIsAutoDestroyParticleEffect)
	{
		//старая логика для огня
		
		//Поэтому вызываем SwitchEffect
		SwitchEffect(newEffect, true); 
		//Это происходит только на сервере. И дальше, поскольку знаем, что RepNotify работает только на клиенте
		//Если мы поменяем эту переменную на новую:
		//В момент вызова этой переменной запускается RepNotify ф-я EffectAdd_OnRep() на клиенте
		//Самое главное волшебство:
		EffectAdd = newEffect;
	}
	else //для небесконечных эффектов
	{
		if (newEffect->ParticleEffect)
		{
			ExecuteEffectAdded_OnServer(newEffect->ParticleEffect);
		}
	}
}

void AMyTDS_EnemyCharacter::EffectAdd_OnRep()
{
	//Мы уже знаем, что в RepNotify EffectAdd корректно появлась, обновилась и эта функция отработает только тогда,
	//когда на клиенте эта EffectAdd поменяется
	if (EffectAdd)
	{
		SwitchEffect(EffectAdd, true);
	}
}

void AMyTDS_EnemyCharacter::EffectRemove_OnRep()
{
	if (EffectRemove)
	{
		SwitchEffect(EffectRemove, false);
	}
}

void AMyTDS_EnemyCharacter::ExecuteEffectAdded_OnServer_Implementation(UParticleSystem* ExecuteFX)
{
	//1 вызов -какой эффект надо заспаунить
	ExecuteEffectAdded_Multicast(ExecuteFX);
}

void AMyTDS_EnemyCharacter::ExecuteEffectAdded_Multicast_Implementation(UParticleSystem* ExecuteFX)
{
	//а ан клиенте определяем - какой объект итд
	UTypes::ExecuteEffectAdded(ExecuteFX, this, FVector(0), FName ("Spine_01"));
}




void AMyTDS_EnemyCharacter::SwitchEffect(UMyTDS_StateEffect* Effect, bool bIsAdd)
{
	if (bIsAdd)
	{
		if (Effect && Effect->ParticleEffect)
		{
			FName NameBoneToAttached = NAME_None; //прописываем -куда мы хотим прицепить эффект
			FVector Loc = FVector(0.0f);; //и локацию
			USceneComponent* mySceneComponent = GetRootComponent();
			if (mySceneComponent)
			{
				//спавним
				UParticleSystemComponent* NewParticleSystem = UGameplayStatics::SpawnEmitterAttached
					(
					Effect->ParticleEffect,
					mySceneComponent,
					NameBoneToAttached,
					Loc,
					FRotator::ZeroRotator,
					EAttachLocation::SnapToTarget,
					false
					);
				//Добавляем заспавненный UParticleSystemComponent в массив компонентов
				ParticleSystemEffects.Add(NewParticleSystem);
			}
		}
	}
	else //теперь удаляем эффекты. Если бы был спец по эффектам, который был сделал эффекты по времени,
		//это бы вообще не было нужно, но, т.к. наш огонь горит постоянно, эффект надо удалять.
		//Для этого ввели bIsAdd
		{
		if (Effect && Effect->ParticleEffect)
		{
			int32 i = 0;
			bool bIsFind = false;
			if (ParticleSystemEffects.Num() > 0) 
			{
				//пробегаемся по массиву партиклов-систем-компонентов 
				while (i < ParticleSystemEffects.Num() && !bIsFind)
				{
					//Когда мы назначаем какой-то эффект мы как раз назаначаем переменную Template
					if (ParticleSystemEffects[i] && ParticleSystemEffects[i]->Template
							&& Effect->ParticleEffect && Effect->ParticleEffect == ParticleSystemEffects[i]->Template)
					{
						bIsFind = true;
						ParticleSystemEffects[i]->DeactivateSystem();
						ParticleSystemEffects[i]->DestroyComponent();
						ParticleSystemEffects.RemoveAt(i);
					}
					i++;
				}
			}			
		}
		}
}

//Позволяет компоненту реплицировать другой подобъект на актере
bool AMyTDS_EnemyCharacter::ReplicateSubobjects(UActorChannel* Channel, FOutBunch* Bunch, FReplicationFlags* RepFlags)
{
	//Wrote - это битовое сравнение
	bool Wrote = Super::ReplicateSubobjects(Channel,Bunch,RepFlags);

	for (int32 i = 0; i < Effects.Num(); i++)
	{
		if (Effects[i])
		{
			Wrote |= Channel->ReplicateSubobject(Effects[i], *Bunch, *RepFlags);
		}
	}
	return Wrote;
}

//Для UPROPERTY(Replicated)
void AMyTDS_EnemyCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	//Макросы
	//Говорят о том что (например) в нашем объекте в классе AMyTDSCharacter - MovementState должна быть реплицируемой
	DOREPLIFETIME(AMyTDS_EnemyCharacter, Effects);
	DOREPLIFETIME(AMyTDS_EnemyCharacter, EffectAdd);
	DOREPLIFETIME(AMyTDS_EnemyCharacter, EffectRemove);
}
