// Fill out your copyright notice in the Description page of Project Settings.


#include "MyTDS/Character/MyTDSCharacterHealthComponent.h"

void UMyTDSCharacterHealthComponent::ChangeHealthValue_OnServer(float ChangeValue)
{
	//Важно! -сначала мы идем в чайлдовский компонент и, если это нужно, идем в родительский Super!
	//т.о. мы режем возможный и невозможный функционал для наших жизней
	float CurrentDamage = ChangeValue * CoefDamage;
	//Health += ChangeValue;//Плюсум! Т.к. УРОН - ChangeValue с минусом
	//Проверка на присутствие щита
	if (Shield > 0.0f && CurrentDamage < 0.0f)
	{
		//Shield -= CurrentDamage; Меняем на:
		ChangeShieldValue(CurrentDamage);

		if (Shield == 0.0f)
		{
			// FX
			UE_LOG(LogTemp, Warning, TEXT("UTPSCharacterHealthComponent::ChangeHealthValue_OnServer - Shield %f > 0.0f && CurrentDamage %f< 0.0f"),Shield, CurrentDamage);
		}
	}
	else
		//когда
	{
		GetWorld()->GetTimerManager().ClearTimer(TimerHandle_CoolDownShieldTimer);
		Super::ChangeHealthValue_OnServer(CurrentDamage);	
	}
	// OnHealthChange.Broadcast(Health, ChangeValue);
	// if (Health > 100.0f)
	// {
	// 	Health = 100.0f;
	// }
	// else
	// {
	// 	if (Health < 0.0f)
	// 	{
	// 		OnDead.Broadcast();
	// 	}
	// }
}

float UMyTDSCharacterHealthComponent::GetCurrentShield()
{
	return Shield;
}

void UMyTDSCharacterHealthComponent::ChangeShieldValue(float ChangeValue)
{
	//Получем урон, минусуем щит, запускаем CoolDown и очищаем таймер, если у нас сработала регенерация щита - RecoveryShield()
	//После того, как CoolDown отработает - CoolDownShieldEnd(), запускается следующий таймер, который запускает
	//постепенное увеличение жизней для нашего щита
	Shield += ChangeValue;

	//NET
	//OnShieldChange.Broadcast(Shield, ChangeValue);
	ShieldChangeEvent_Multicast(Shield, ChangeValue);
	
	if (Shield > 100.0f)
	{
		Shield = 100.0f;
	}
	else
	{
		if (Shield <= 0.0f)
			Shield = 0.0f;
	}
	if (GetWorld())
	{
		// Когда мы наносим урон  -у нас есть CoolDown = 5 сек. Как только заканчивается CoolDown у нас начинают восстанавливаться наши щиты
		// и у нас по сути будет 2 таймера.
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_CoolDownShieldTimer, this, &UMyTDSCharacterHealthComponent::CoolDownShieldEnd, CoolDownShieldRecoverTime, false);
		//Чистим таймер восстановления
		GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoveryRateTimer);
	}	
}

//Запускает следующий тамер -который уже проверяет то, как часто он будет обновлять наш щит
void UMyTDSCharacterHealthComponent::CoolDownShieldEnd()
{
	if (GetWorld())
    	{
		//Таймер оставнавливаем в лупе
    		GetWorld()->GetTimerManager().SetTimer(TimerHandle_ShieldRecoveryRateTimer, this, &UMyTDSCharacterHealthComponent::RecoveryShield, ShieldRecoverRate, true);
    	}	
}

void UMyTDSCharacterHealthComponent::RecoveryShield()
{
		float tmp = Shield;
    	tmp = tmp + ShieldRecoverValue;
    	if (tmp > 100.0f)
    	{
    		Shield = 100.0f;
    		if (GetWorld())
    		{
    			//Убираем таймер
    			//GetTimerManager возвращает ссылку! Поэтому не стрелка, а точка!
    			//Не GetTimerManager()->ClearTime, а GetTimerManager().ClearTimer
    			GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoveryRateTimer);
    		}
    	}
    	else
    	{
    		Shield = tmp;
    	}
//NET
	//OnShieldChange.Broadcast(Shield, ShieldRecoverValue);
	ShieldChangeEvent_Multicast(Shield, ShieldRecoverValue);
}

float UMyTDSCharacterHealthComponent::GetShieldValue()
{
	return Shield;
}

//NET
void UMyTDSCharacterHealthComponent::ShieldChangeEvent_Multicast_Implementation(float newShield, float Damage)
{
	OnShieldChange.Broadcast(newShield, Damage);
}
