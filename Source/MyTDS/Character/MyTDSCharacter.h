// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "MyTDS/FuncLibrary/Types.h"
#include "MyTDS/Weapons/WeaponDefault.h"
#include "MyTDS/Character/MyTDSInventoryComponent.h"
#include "MyTDS/Character/MyTDSCharacterHealthComponent.h"
//инклудим интерфейс
#include "MyTDS/StateEffects/MyTDS_StateEffect.h"
#include "MyTDS/Interface/MyTDS_IGameActor.h"
#include "MyTDSCharacter.generated.h"


UCLASS(Blueprintable)
//Добавили класс интерфейса IMyTDS_IGameActor - I!!! 
class AMyTDSCharacter : public ACharacter, public IMyTDS_IGameActor
{
	GENERATED_BODY()
protected:
	//NET
	//Позволяет компоненту реплицировать другой подобъект на актере
	bool ReplicateSubobjects(class UActorChannel* Channel, class FOutBunch* Bunch, FReplicationFlags* RepFlags) override;

	void BeginPlay() override;
	
public:
	AMyTDSCharacter();

	FTimerHandle TimerHandle_RagDollTimer;

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;
	

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	// Закоментили  -потому что будем менять материал курсора в зависимости от оружия
	/** Returns CursorToWorld subobject **/
	// FORCEINLINE class UDecalComponent* GetCursorToWorld() { return CursorToWorld; }

private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	// /** A decal that projects to the cursor location. */
	// UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	// class UDecalComponent* CursorToWorld;

	//Инвентарь
		UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Inventory", meta = (AllowPrivateAccess = "true"))
		class UMyTDSInventoryComponent* InventoryComponent;

		UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Health", meta = (AllowPrivateAccess = "true"))
		class UMyTDSCharacterHealthComponent* HealthComponent;
	//	class UMyTDSHealthComponent* HealthComponentTest;

public:
	//Cursor
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
		UMaterialInterface* CursorMaterial = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
		FVector CursorSize = FVector(20.0f, 40.0f, 40.0f);

	//Weapon -переменная для класса оружия
	//Replicated!
	UPROPERTY(Replicated)
	AWeaponDefault* CurrentWeapon = nullptr;
	
	//Movement
	//Replicated добавили в EMovementState
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "Movement")
		EMovementState MovementState = EMovementState::Run_State;
	UFUNCTION(BlueprintCallable, BlueprintPure)
		EMovementState GetMovementState();
	UFUNCTION(BlueprintCallable, BlueprintPure)
		UDecalComponent* GetCursorToWord();
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		FCharacterSpeed MovementSpeedInfo; //Меняем MovementInfo на MovementSpeedInfo ???
	

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	bool SprintRunEnabled =false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	bool WalkEnabled = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	bool AimEnabled = false;

	//Смерть -добавляем переменную для жизнь/смерть и анимацию смперти
	// UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	// bool bIsAlive = true;
	//массив  -т.к. у нас есть 3 анимации
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	TArray<UAnimMontage*> DeadsAnim;

	//Для эффекта Aid
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ability")
	TSubclassOf<UMyTDS_StateEffect> AbilityEffect;
	//Для эффекта Неуязвимость
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ability")
	TSubclassOf<UMyTDS_StateEffect> InvulnerabilityEffect;
	
	//Выносливость
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	int32 Stamina = 5;
	// Кэффициент выносливости
	int32 CoefStam = Stamina;
    
	//for demo 
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Demo")
	//7.2 Меняем TSubclassOf<AWeaponDefault> InitWeaponClass = nullptr;  на
	FName InitWeaponName;
	
	UDecalComponent* CurrentCursor = nullptr;

	//Inputs
	void InputAxisY(float Value);
	void InputAxisX(float Value);
	//Inputs for weapon
	void InputAttackPressed();
	void InputAttackReleased();
	void InputWalkPressed(); 
	void InputWalkReleased();

	void InputSprintPressed();
	void InputSprintReleased();

	void InputAimPressed();
	void InputAimReleased();

	
	
	//Inventory Inputs
	void TrySwitchNextWeapon();
	void TrySwitchPreviosWeapon();

	//Ability Inputs - инпуты для эффектов
	UFUNCTION(Server,Reliable)
	void TryAbilityEnabled_OnServer();
	UFUNCTION(NetMulticast, Unreliable)
	void TryAbilityEnabled_Multicast();
	
	UFUNCTION(Server,Reliable)
	void TryInvulnerabilityEnabled_onServer();

	//Инпут Для сброса оружия
	void DropCurrentWeapon();
	
//NET
	UFUNCTION(Server,Reliable)
	void TrySwitchWeaponToIndexByKeyInput_OnServer(int32 ToIndex);
	//Шаблон
	template<int32 Id>
    	void TKeyPressed()
    	{
			TrySwitchWeaponToIndexByKeyInput_OnServer(Id);
    	}
	
    	//Inputs End

	float AxisX = 0.0f;
	float AxisY = 0.0f;

	//Tick function
	UFUNCTION()
	void MovementTick(float DeltaTime);
	bool SwitchCursorRotator = true;

	//Func
	UFUNCTION(BlueprintCallable)
	void AttackCharEvent(bool bIsFiring);
	UFUNCTION(BlueprintCallable, BlueprintPure)
	AWeaponDefault* GetCurrentWeapon();
	//добавляем NewCurrentIndexWeapon -для подбора оружия -говорит о том, что инициализация прошла
	UFUNCTION(BlueprintCallable)
	void InitWeapon(FName IDWeaponName, FAdditionalWeaponInfo WeaponAdditionalInfo, int32 NewCurrentIndexWeapon);

	UFUNCTION(BlueprintCallable)
	void TryReloadWeapon();

	UFUNCTION(BlueprintCallable, BlueprintPure)
	bool GetIsAlive();

	//Делегаты
	UFUNCTION()
	void WeaponFireStart(UAnimMontage* Animat);
	UFUNCTION()
	void WeaponReloadStart(UAnimMontage* Anim);
	UFUNCTION()
	void WeaponReloadEnd(bool bIsSuccess, int32 AmmoSafe);
	UFUNCTION(BlueprintNativeEvent)
	void WeaponFireStart_BP(UAnimMontage* Animat);
	UFUNCTION(BlueprintNativeEvent)
	void WeaponReloadStart_BP(UAnimMontage* Anim);
	UFUNCTION(BlueprintNativeEvent)
	void WeaponReloadEnd_BP(bool bIsSuccess);
	
	//Будем использовать для изменения курсора в зависимости от оружия - будем менять материал курсора
	UFUNCTION(BlueprintCallable)
	UDecalComponent* GetCursorToWorld();
	
	//Функция для изменения состояния персонажа: например, если мы находимся в состоянии прицеливания
	// мы меняем скорость
	UFUNCTION(BlueprintCallable)
	void CharacterUpdate();
	//Функция состояния скорости!
	UFUNCTION(BlueprintCallable)
//	void ChangeMovementState(EMovementState NewMovementState); Меняем на:
	//Функция вызывается в блюпринте!
	void ChangeMovementState();

	float BuferTime = 0.f;
	float CoefTimer = 0.5f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Effect")
	TArray<UParticleSystemComponent*> ParticleSystemEffects;
	
	UPROPERTY(Replicated, BlueprintReadOnly,EditDefaultsOnly)//индекс элемента в массиве оружия: для Rifle_v1 -0, итд.
	int32 CurrentIndexWeapon = 0;//какое оружие сейчас в руках
	

	//interface - это для теории
	//Берем из интерфеса и обязательно Implementation и override
	// bool AviableForEffects_Implementation() override;

	EPhysicalSurface GetSurfuceType() override;
	
	//берем и оверрайдим из class UMyTDS_IGameActor
	//1)получаем массив навешанных эффектов
	//Это например - блюпринт MyTDS_StateEffect_Fire - причем у нас не массив, а он один
	UFUNCTION(BlueprintCallable, BlueprintPure)
	TArray<UMyTDS_StateEffect*> GetAllCurrentEffects() override;



	
	//Effect - массив эффектов
	UPROPERTY(Replicated)
	TArray<UMyTDS_StateEffect*>Effects;
	
	//Переменные для RepNotify - обязательно функции для них!
	UPROPERTY(ReplicatedUsing = EffectAdd_OnRep) 
	UMyTDS_StateEffect* EffectAdd = nullptr;
	UPROPERTY(ReplicatedUsing = EffectRemove_OnRep)
	UMyTDS_StateEffect* EffectRemove = nullptr;

	UFUNCTION()
	void EffectAdd_OnRep();
	UFUNCTION()
	void EffectRemove_OnRep();

	//Удаляем блюпринт с эффектом или БП эфекта и добавляем BlueprintNativeEvent
	//- Если есть описание UFUNCTION - не требует реализации в СРР
	// добавляем UFUNCTION(BlueprintCallable, BlueprintNativeEvent) приэтом убираем override!!!
	//создаём дубль ф-ии но уже с override, а срр добавлям _Implementation
	//_Implementation: Так мы говорим движку, что помимо блюпринтовой реализации есть еще и в срр
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void AddEffect(UMyTDS_StateEffect* newEffect);
	//Говорим движку, что кроме блюпринтового вызова (BlueprintNativeEvent) у нас еще есть
	//реализация в в плюсах - _Implementation
	void AddEffect_Implementation(UMyTDS_StateEffect* newEffect) override;
	
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void RemoveEffect(UMyTDS_StateEffect* RemoveEffect);
	void RemoveEffect_Implementation(UMyTDS_StateEffect* RemoveEffect) override;

	// из Types.h
	//Когда мы создаем ститическую ф-ю static - нельзя использовать Server или NetMulticast!!!
	UFUNCTION(Server, Reliable)
	void ExecuteEffectAdded_OnServer(UParticleSystem* ExecuteFX);
	UFUNCTION(Server, Reliable) 
	void ExecuteEffectAdded_Multicast(UParticleSystem* ExecuteFX);
	//end interface

	

	UFUNCTION(BlueprintCallable, BlueprintPure)
	int32 GetCurrentWeaponIndex();

	UFUNCTION()
	void CharDead();

	//Должа работать только на сервере, поэтому
	UFUNCTION(NetMulticast, Reliable)
	void EnableRagDoll_Multicast();  
	//Берем функцию у эпиков и овверрайдим её!
	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;

	UFUNCTION(BlueprintNativeEvent)
	void CharDead_BP();

	//Мультиплейер
	UFUNCTION(Server, Unreliable)
	void SetActorRotationByYaw_OnServer(float Yaw);

	UFUNCTION(NetMulticast, Unreliable)
	void SetActorRotationByYaw_Multicast(float Yaw);

	UFUNCTION(Server, Reliable)
	void SetMovementstate_OnServer(EMovementState NewState);

	UFUNCTION(NetMulticast, Reliable)
	void SetMovementstate_Multicast(EMovementState NewState);

	UFUNCTION(Server, Reliable)
	void TryReloadWeapon_OnServer();

	UFUNCTION(NetMulticast, Reliable)
	void PlayAnim_Multicast(UAnimMontage* Anim);
	

	// UFUNCTION(Server, Reliable)
	// void ExecuteEffectAdded_OnServer(UParticleSystem* ExecuteFX);
//
// 	UFUNCTION(NetMulticast, Reliable)
// 	void ExecuteEffectAdded_Multicast(UParticleSystem* ExecuteFX);
// 	
	UFUNCTION()
	void SwitchEffect(UMyTDS_StateEffect* Effect, bool bIsAdd);//bIsAdd -стоит добавлять или нет
};