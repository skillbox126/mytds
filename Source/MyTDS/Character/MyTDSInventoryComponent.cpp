// Fill out your copyright notice in the Description page of Project Settings.

#include "MyTDS/Character/MyTDSInventoryComponent.h"
#include "MyTDS/Game/MyTDSGameInstance.h"
#include "MyTDS/Interface/MyTDS_IGameActor.h"
#include "Net/UnrealNetwork.h"
//#pragma optimize ("", off)

// Sets default values for this component's properties
UMyTDSInventoryComponent::UMyTDSInventoryComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	//NET - ставим такую -советуют эпики
	SetIsReplicatedByDefault(true);

	// ...
}


// Called when the game starts
void UMyTDSInventoryComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
}


// Called every frame
void UMyTDSInventoryComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}


//TODO OMG Refactoring need!!!
//меняем индекс оружия: старый OldIndex на ChangeToIndex. bIsForward - направление сортировки
bool UMyTDSInventoryComponent::SwitchWeaponToIndexByNextPreviosIndex(int32 ChangeToIndex, int32 OldIndex, FAdditionalWeaponInfo OldInfo, bool bIsForward)
{
	bool bIsSuccess = false;
	int8 CorrectIndex = ChangeToIndex;
	if (ChangeToIndex > WeaponSlots.Num()-1)
		CorrectIndex = 0;	
	else
		if(ChangeToIndex < 0)
			CorrectIndex = WeaponSlots.Num()-1;

	FName NewIdWeapon;
	FAdditionalWeaponInfo NewAdditionalInfo; // остаток патронов в оружии в данный момент
	int32 NewCurrentIndex = 0;

	//проверяем, есть ли патроны в оружии (в магазине)
	if (WeaponSlots.IsValidIndex(CorrectIndex))//если есть слот оруЖИЯ
	{
		if (!WeaponSlots[CorrectIndex].NameItem.IsNone())//если есть имя оружия
		{
			if (WeaponSlots[CorrectIndex].AdditionalInfo.Round > 0)//если остались патроны в магазине - в самом оружии
			{
				//good weapon have ammo start change - начинаем смену индекса
				bIsSuccess = true;
			}
			else
			{
				UMyTDSGameInstance* myGI = Cast<UMyTDSGameInstance>(GetWorld()->GetGameInstance());
				if (myGI)
				{
					//проверяем  -остались ли патроны в инвентаре 
					//check ammoSlots for this weapon - т.к. патронов в магазине нет - сначала получаем всю инфу об оружии по имени (индексу)
					FWeaponInfo myInfo;
					myGI->GetWeaponInfoByName(WeaponSlots[CorrectIndex].NameItem, myInfo);

					bool bIsFind = false;
					int8 j = 0;
					while (j < AmmoSlots.Num() && !bIsFind)
					{
						//если тип j- того оружия совпадает с нашим и у него остались патроны в инвентаре AmmoSlots
						if (AmmoSlots[j].WeaponType == myInfo.WeaponType && AmmoSlots[j].Cout > 0)
						{
							//good weapon have ammo start change
							bIsSuccess = true;
							bIsFind = true;
						}
						j++;
					}
				}
			}
			if (bIsSuccess)
			{
				NewCurrentIndex = CorrectIndex;
				NewIdWeapon = WeaponSlots[CorrectIndex].NameItem;
				NewAdditionalInfo = WeaponSlots[CorrectIndex].AdditionalInfo;
			}
		}
	}

	//если нет патронов ни в оружии ни в инвентаре
	if (!bIsSuccess)
	{		
		int8 iteration = 0;
		int8 Seconditeration = 0;
		int8 tmpIndex = 0;
		while (iteration < WeaponSlots.Num() && !bIsSuccess)
		{
			iteration++; //iteration == 1

			if (bIsForward) //направление сортировки вперед - true
			{
				//Seconditeration = 0;

				tmpIndex = ChangeToIndex + iteration; //берем следующий индекс оружия, например 1+1 = 2
			}
			else //направление сортировки назад - false
			{
				Seconditeration = WeaponSlots.Num() - 1;//если всего 5 слотов, то WeaponSlots.Num() = 5-1 =4

				tmpIndex = ChangeToIndex - iteration;//= текущий индекс, например 1-4 = -3
			}

			if (WeaponSlots.IsValidIndex(tmpIndex))
			{
				if (!WeaponSlots[tmpIndex].NameItem.IsNone())
				{
					if (WeaponSlots[tmpIndex].AdditionalInfo.Round > 0) //есть патроны в оружии
					{
						//WeaponGood - переключаемся на на это оружие
						bIsSuccess = true;
						NewIdWeapon = WeaponSlots[tmpIndex].NameItem;
						NewAdditionalInfo = WeaponSlots[tmpIndex].AdditionalInfo;
						NewCurrentIndex = tmpIndex;
					}
					else //нет патронов в оружии - проверяем в инвентаре
					{
						FWeaponInfo myInfo;
						UMyTDSGameInstance* myGI = Cast<UMyTDSGameInstance>(GetWorld()->GetGameInstance());

						myGI->GetWeaponInfoByName(WeaponSlots[tmpIndex].NameItem, myInfo);

						bool bIsFind = false;
						int8 j = 0;
						while (j < AmmoSlots.Num() && !bIsFind)//ищем патроны в инвентаре
						{
							if (AmmoSlots[j].WeaponType == myInfo.WeaponType && AmmoSlots[j].Cout > 0)
							{
								//WeaponGood если есть патроны в инвентаре -переключаемся
								bIsSuccess = true;
								NewIdWeapon = WeaponSlots[tmpIndex].NameItem;
								NewAdditionalInfo = WeaponSlots[tmpIndex].AdditionalInfo;
								NewCurrentIndex = tmpIndex;
								bIsFind = true;
							}
							j++;
						}
					}
				}
			}
			else// нет патронов ни в оружии, ни в инвентаре
			{
				//go to end of LEFT of array weapon slots
				if (OldIndex != Seconditeration)//индекс оружия не равен 4? - чтобы не сбросить последнее торужие? хз!
				{
					if (WeaponSlots.IsValidIndex(Seconditeration))
					{
						if (!WeaponSlots[Seconditeration].NameItem.IsNone())
						{
							if (WeaponSlots[Seconditeration].AdditionalInfo.Round > 0)
							{
								//WeaponGood
								bIsSuccess = true;
								NewIdWeapon = WeaponSlots[Seconditeration].NameItem;
								NewAdditionalInfo = WeaponSlots[Seconditeration].AdditionalInfo;
								NewCurrentIndex = Seconditeration;
							}
							else
							{
								FWeaponInfo myInfo;
								UMyTDSGameInstance* myGI = Cast<UMyTDSGameInstance>(GetWorld()->GetGameInstance());

								myGI->GetWeaponInfoByName(WeaponSlots[Seconditeration].NameItem, myInfo);

								bool bIsFind = false;
								int8 j = 0;
								while (j < AmmoSlots.Num() && !bIsFind)
								{
									if (AmmoSlots[j].WeaponType == myInfo.WeaponType && AmmoSlots[j].Cout > 0)
									{
										//WeaponGood
										bIsSuccess = true;
										NewIdWeapon = WeaponSlots[Seconditeration].NameItem;
										NewAdditionalInfo = WeaponSlots[Seconditeration].AdditionalInfo;
										NewCurrentIndex = Seconditeration;
										bIsFind = true;
									}
									j++;
								}
							}
						}
					}
				}
				else
				{
					//go to same weapon when start
					if (WeaponSlots.IsValidIndex(Seconditeration))
					{
						if (!WeaponSlots[Seconditeration].NameItem.IsNone())
						{
							if (WeaponSlots[Seconditeration].AdditionalInfo.Round > 0)
							{
								//WeaponGood, it same weapon do nothing
							}
							else
							{
								FWeaponInfo myInfo;
								UMyTDSGameInstance* myGI = Cast<UMyTDSGameInstance>(GetWorld()->GetGameInstance());

								myGI->GetWeaponInfoByName(WeaponSlots[Seconditeration].NameItem, myInfo);

								bool bIsFind = false;
								int8 j = 0;
								while (j < AmmoSlots.Num() && !bIsFind)
								{
									if (AmmoSlots[j].WeaponType == myInfo.WeaponType)
									{
										if (AmmoSlots[j].Cout > 0)
										{
											//WeaponGood, it same weapon do nothing
										}
										else
										{
											//Not find weapon with ammo need init Pistol with infinity ammo
											UE_LOG(LogTemp, Error, TEXT("UTPSInventoryComponent::SwitchWeaponToIndex - Init PISTOL - NEED"));
										}
									}
									j++;
								}
							}
						}
					}
				}
				if (bIsForward)
				{
					Seconditeration++;
				}
				else
				{
					Seconditeration--;
				}
				
			}
		}
	}	
	if (bIsSuccess)
	{
		SetAdditionalInfoWeapon(OldIndex,OldInfo);
		//NET
		//OnSwitchWeapon.Broadcast(NewIdWeapon, NewAdditionalInfo, NewCurrentIndex);
		SwitchWeaponEvent_Multicast(NewIdWeapon, NewAdditionalInfo, NewCurrentIndex);
	}		

	return bIsSuccess;
}




bool UMyTDSInventoryComponent::SwitchWeaponByIndex(int32 IndexWeaponToChange, int32 PreviosIndex, FAdditionalWeaponInfo PreviosWeaponInfo)
{
	bool bIsSuccess = false;
	FName ToSwitchIdWeapon;
	FAdditionalWeaponInfo ToSwitchAdditionalInfo;

	ToSwitchIdWeapon = GetWeaponNameBySlotIndex(IndexWeaponToChange);//result = WeaponSlots[indexSlot].NameItem; 
	ToSwitchAdditionalInfo = GetAdditionalInfoWeapon(IndexWeaponToChange);

	if (!ToSwitchIdWeapon.IsNone())
	{
		SetAdditionalInfoWeapon(PreviosIndex, PreviosWeaponInfo);
		//NET
		//OnSwitchWeapon.Broadcast(ToSwitchIdWeapon, ToSwitchAdditionalInfo, IndexWeaponToChange);
		SwitchWeaponEvent_Multicast(ToSwitchIdWeapon, ToSwitchAdditionalInfo, IndexWeaponToChange);


		//check ammo slot for event to player		
		EWeaponType ToSwitchWeaponType;
		if (GetWeaponTypeByNameWeapon(ToSwitchIdWeapon, ToSwitchWeaponType))
		{
			int32 AviableAmmoForWeapon = -1;
			if (CheckAmmoForWeapon(ToSwitchWeaponType, AviableAmmoForWeapon))
			{
				
			}								
		}		
		bIsSuccess = true;
	}
	return bIsSuccess;
}


//остаток патронов в инвентаре по индексу оружия
FAdditionalWeaponInfo UMyTDSInventoryComponent::GetAdditionalInfoWeapon(int32 IndexWeapon)
{
	FAdditionalWeaponInfo result;
	if (WeaponSlots.IsValidIndex(IndexWeapon))
	{
		bool bIsFind = false;
		int8 i = 0;
		while (i < WeaponSlots.Num() && !bIsFind)
		{
			if (/*WeaponSlots[i].IndexSlot*/i == IndexWeapon)
			{
				result = WeaponSlots[i].AdditionalInfo;
				bIsFind = true;
			}
			i++;
		}
		if(!bIsFind)
			UE_LOG(LogTemp, Warning, TEXT("UMyTDSInventoryComponent::SetAdditionalInfoWeapon - No Found Weapon with index - %d"), IndexWeapon);
	}
	else
		UE_LOG(LogTemp, Warning, TEXT("UMyTDSInventoryComponent::SetAdditionalInfoWeapon - Not Correct index Weapon - %d"), IndexWeapon);

	return result;
}






//функция получения индекса оружия по его имени
//ПРОБЛЕМА ВОЗНИКАЕТ, КОГДА У НАС 2 ОДИНАКОВЫХ ОРУЖИЯ В ВЕАПОНСЛОТЕ - ЗДЕСЬ ИЩЕМ ТОЛЬКО 1-Е ПОПАВШЕЕСЯ!!!
int32 UMyTDSInventoryComponent::GetWeaponIndexSlotByName(FName IdWeaponName)
{
	//UE_LOG(LogTemp, Warning, TEXT("FName IdWeaponName - %s"), *IdWeaponName.ToString());

	int32 result = -1; //устанавливаем индекс слота оружия = -1, если у нас НЕТ ОРУЖИЯ!
	int8 i = 0;
	bool bIsFind = false;
	while (i < WeaponSlots.Num() && !bIsFind) //Когда в блюпринте сделали пустой слот -5-й
	{
		if (WeaponSlots[i].NameItem == IdWeaponName)
		{
			bIsFind = true;
			result = i/*WeaponSlots[i].IndexSlot*/; 
		}
		i++;
	}
	return result;
	//Когда есть пустой слот индексы будут: 0, 1, 2, 3, 4, -1!
}



//функция возвращает имя оружия по слоту 
FName UMyTDSInventoryComponent::GetWeaponNameBySlotIndex(int32 indexSlot) 
{
	//сразу result будет пустым
	FName result;

	if (WeaponSlots.IsValidIndex(indexSlot))
	{
		result = WeaponSlots[indexSlot].NameItem;
	}
	return result;
}

//???
//получем тип WeaponType оружия по IdWeaponName
bool UMyTDSInventoryComponent::GetWeaponTypeByNameWeapon(FName IdWeaponName, EWeaponType& WeaponType)
{
	bool bIsFind = false;
    	FWeaponInfo OutInfo;
    	WeaponType = EWeaponType::RifleType;//по умолчанию -типа обнуляем
    	UMyTDSGameInstance* myGI = Cast<UMyTDSGameInstance>(GetWorld()->GetGameInstance());
    	if (myGI)
    	{
    		myGI->GetWeaponInfoByName(IdWeaponName, OutInfo);
    		WeaponType = OutInfo.WeaponType;
    		bIsFind = true;		
    	}
    	return bIsFind;
}

//???
//получаем тип оружия WeaponType по его слоту IndexSlot в WeaponSlots 
bool UMyTDSInventoryComponent::GetWeaponTypeByIndexSlot(int32 IndexSlot, EWeaponType& WeaponType)
{
	bool bIsFind = false;
    	FWeaponInfo OutInfo;
    	WeaponType = EWeaponType::RifleType;
    	UMyTDSGameInstance* myGI = Cast<UMyTDSGameInstance>(GetWorld()->GetGameInstance());
    	if (myGI)
    	{
    		if (WeaponSlots.IsValidIndex(IndexSlot))
    		{
    			myGI->GetWeaponInfoByName(WeaponSlots[IndexSlot].NameItem, OutInfo);
    			WeaponType = OutInfo.WeaponType;
    			bIsFind = true;
    		}		
    	}	
    	return bIsFind;
}


//функция сохраняет кол=во патронов в слот с оружием, когда стреляем (и когда меняется оружие)
void UMyTDSInventoryComponent::SetAdditionalInfoWeapon(int32 IndexWeapon, FAdditionalWeaponInfo NewInfo)
{
	if (WeaponSlots.IsValidIndex(IndexWeapon))
	{
		bool bIsFind = false;
		int8 i = 0;
		while (i<WeaponSlots.Num() && !bIsFind) //цикл по слотам оружия
		{
			if (/*WeaponSlots[i].IndexSlot*/i == IndexWeapon)//находим оружие по индексу
			{
				WeaponSlots[i].AdditionalInfo = NewInfo;//записываем в таблицу остаток патронов = новая инфа
				bIsFind = true;
				//Remove to ammoCout change
				//Вродкастим событие на смену оружия и передаем ивент в виджет InventoryStartWidjet в функцию InventoryStartWidgetBindFunc
				//уже здесь не нужен 8.4 Инвентарь. Часть 4  - 22'50" перeносим в AmmoSlotChangeValue
				
				//OnAmmoChange.Broadcast(EWeaponType::ShotGanType,32);
				//OnWeaponAdditionalInfoChange.Broadcast(IndexWeapon,NewInfo)

				//Делегат на каждый выстрел
				//NET
				//OnWeaponAdditionalInfoChange.Broadcast(IndexWeapon, NewInfo);
				WeaponAdditionalInfoChangeEvent_Multicast(IndexWeapon, NewInfo);
			}
			i++;
		}
		if(!bIsFind)
			UE_LOG(LogTemp, Warning, TEXT("UMyTDSInventoryComponent::SetAdditionalInfoWeapon - No Found Weapon with index - %d"), IndexWeapon);
	}
	else
		UE_LOG(LogTemp, Warning, TEXT("UMyTDSInventoryComponent::SetAdditionalInfoWeapon - Not Correct index Weapon - %d"), IndexWeapon);
}
 //делаем функцию универсальной -когда мы перезаряжаемся и когда будем находить патроны
//функция удаляет патроны из инвентаря при перезарядке и добавляет патроны в инвентарь - когда находим
void UMyTDSInventoryComponent::AmmoSlotChangeValue(EWeaponType TypeWeapon, int32 CoutChangeAmmo)
{
	bool bIsFind = false;
	int8 i = 0;
	while (i < AmmoSlots.Num() && !bIsFind)
	{
		if (AmmoSlots[i].WeaponType == TypeWeapon)
		{
			AmmoSlots[i].Cout += CoutChangeAmmo;//при перезарядке приходит с минусом!
			if (AmmoSlots[i].Cout > AmmoSlots[i].MaxCout)
				AmmoSlots[i].Cout = AmmoSlots[i].MaxCout;
			//to AmmoCout change
			//	OnAmmoChange.Broadcast(AmmoSlots[i].WeaponType, AmmoSlots[i].Cout);
			//NET
			AmmoChangeEvent_Multicast(AmmoSlots[i].WeaponType, AmmoSlots[i].Cout);

			bIsFind = true;
		}
		i++;
	}
}

//функция проверяет - есть патроны в ИНВЕНТАРЕ у  оружия типа TypeWeapon или нет. Есть - true.
// и возвращает сссылку на остаток патронов
bool UMyTDSInventoryComponent::CheckAmmoForWeapon(EWeaponType TypeWeapon, int32 &AviableAmmoForWeapon)
{
	AviableAmmoForWeapon = 0;
	bool bIsFind = false;
	int32 i = 0;
	while (i < AmmoSlots.Num() && !bIsFind)
	{
		if (AmmoSlots[i].WeaponType == TypeWeapon)
		{
			bIsFind = true;
			AviableAmmoForWeapon = AmmoSlots[i].Cout;
			if(AmmoSlots[i].Cout > 0)
			{
				//OnWeaponAmmoAviable.Broadcast(TypeWeapon); //перенесли В Чар в InitWeapon() - непонятно почему!
				//remove not here, only when pickUp ammo this type, or swithc weapon 
				return true;	
			}
		}
		i++;
	}

	if (AviableAmmoForWeapon <= 0)
	{
		//NET
		//OnWeaponAmmoEmpty.Broadcast(TypeWeapon); //visual empty ammo slot
		WeaponAmmoEmptyEvent_Multicast(TypeWeapon);
	}
	else
	{
		//NET
		//OnWeaponAmmoEmpty.Broadcast(TypeWeapon);
		WeaponAmmoAviableEvent_Multicast(TypeWeapon);
	}
	return false;
}

//Проверка -можем ли мы взять эти патроны -есть ли еще место для них в инвентаре
//например отсутствует хотя бы 1 патрон в инвентаре
bool UMyTDSInventoryComponent::CheckCanTakeAmmo(EWeaponType AmmoType)
{
	bool result = false;
	int32 i = 0;
	while (i < AmmoSlots.Num() && !result)
	{
		if(AmmoSlots[i].WeaponType == AmmoType && AmmoSlots[i].Cout < AmmoSlots[i].MaxCout)
			result = true;
		i++;
	}
	return result;
}

//проверяем наличие свободных слотов -отдельная функция -потому что вдруг потом понадобится
//int8 -нельзя использовать в блюпринтах!
//Амперсанд &FreeSlot ->функция возвращает сама bool и + ссылку на адрес слота &FreeSlot
bool UMyTDSInventoryComponent::CheckCanTakeWeapon(int32 &FreeSlot)//если FreeSlot = -1, элемннтов массива = 5
{
	bool bIsFreeSlot = false;//если не будет свободного слота
	int8 i = 0;
	while (i < WeaponSlots.Num() && !bIsFreeSlot)//если нет слота: массив из 4-х, 
	{
		if (WeaponSlots[i].NameItem.IsNone())//если будет хоть одна пустая строка 
		{
			bIsFreeSlot = true; //передаем true т.е. - мы можем поднять оружие и еще
			FreeSlot = i;//делаем индекс свободного слота (== 5) Массив слотов был 1,2,3,4,-1, стал 1,2,3,4.5
		}
		i++;
	}
	return bIsFreeSlot;
}

//заполняем этот свободный слот
// я хочу в TryGetWeaponToInventory_onServer выбрасывать данные, которые должен выбросить объект
//т.е. сделать дроп непосредственно из блупринтов -потому что некрасиво и не выгодно
//вытаскивать информацию класса, который мы хотим заспаунить, а он у нас описан в блупринте
// -придется делать FindObjet - а это опасно, когда мы что-то переносим в блупринты,
// и наши ссылки -пути -могут слететь - поэтому GetDropItemInfoFromInventory 
bool UMyTDSInventoryComponent::SwitchWeaponToInventory(FWeaponSlot NewWeapon, int32 IndexSlot, int32 CurrentIndexWeaponChar, FDropItem &DropItemInfo)  
{
	bool result = false;
	//валиден!  когда нет свободного слота -все слоты заполнены - массив состоит из 5-ти элементов
	if (WeaponSlots.IsValidIndex(IndexSlot) && GetDropItemInfoFromInventory(IndexSlot, DropItemInfo))
		// после удачного дропа -должны сделать замену
	{
		//если мы хотим поменять именно то оружие, которе держим в руках
		WeaponSlots[IndexSlot] = NewWeapon;//IndexSlot -задаем руками в блюпринте, NewWeapon - новое оружие

		//Пришлось поставить явно IndexSlot = -1. Потому, что в SwitchWeaponToIndex есть функция SetAdditionalInfoWeapon(OldIndex,OldInfo)
		//в которой он старую информацию всё-таки ХАВАЛ -то есть старый индекс и туда записывал патроны.
		// а так - мы практически отключили эту функцию - потому что в ней вначале  проверка: if (WeaponSlots.IsValidIndex(IndexWeapon))
		// и WeaponSlots.IsValidIndex(-1) - не валидна
		// иначе бы в ней  срабатывал Делегат на каждый выстрел  OnWeaponAdditionalInfoChange.Broadcast(IndexWeapon, NewInfo)
		//в каком-то слоте оружия
		
		SwitchWeaponToIndexByNextPreviosIndex(CurrentIndexWeaponChar,-1,NewWeapon.AdditionalInfo,true);	
	
		//NET
		//OnUpdateWeaponSlots.Broadcast(IndexSlot, NewWeapon);
		UpdateWeaponSlotsEvent_Multicast(IndexSlot, NewWeapon); 
		result = true;
	}
	return result;
}


//подбираем оружие -на сервере!!!
// поскольку на сервере ф-я не может вернуть bool - только void (ф-ии асинхронные) извращаемся - вводим  AActor* PickUpActor
// а у нас PickUpActor сделан в блюпринте и  просто выставляются в мир - поэтому так:
void UMyTDSInventoryComponent::TryGetWeaponToInventory_onServer_Implementation(AActor* PickUpActor,  FWeaponSlot NewWeapon)
{//В GetWeaponIndexSlotByName установили, что если нет оружия (имени в слоте) то индекс оружия = -1!
	//а здесь проверяем, если = -1 -значит один слот свободный!
	int32 indexSlot = -1;// чтобы получить свободный слот! (10.2 Часть 2 16'30")
	if (CheckCanTakeWeapon(indexSlot))//если такой слот существует!
	{
		if (WeaponSlots.IsValidIndex(indexSlot))//понятно, что валидный)
		{
			WeaponSlots[indexSlot] = NewWeapon;

			//Костыль - ддля того чтобы вставить маасив в делегат FOnUpdateWeaponSlots
			// FWeaponSlotTest newSlots;
			// newSlots.Slots = WeaponSlots;
			// OnUpdateWeaponSlots.Broadcast(newSlots);

			//Бродкастим событие подбора оржия - запихиваем в индкс слот новое оружие
			//NET
			//OnUpdateWeaponSlots.Broadcast(indexSlot, NewWeapon);
			UpdateWeaponSlotsEvent_Multicast(indexSlot, NewWeapon);

			//После того, как мы считаем, что оружие можно взять, слот свободный и прошел ивент, что слот поменялся,
			//PickUpActor достаточно удалить -Destroy(); - котрая отработает на сервере!
			if (PickUpActor)
			{
				PickUpActor->Destroy();
			}

			// // Добавляем бродкаст для обновления патронов при возрождении после смерти
			// OnWeaponAdditionalInfoChange.Broadcast(indexSlot, NewWeapon.AdditionalInfo);
		//	return true;
		}			
	}
	//return false; //когда нет свободного слота
}

//Сброс оружия по текущуму индексу ByIndex == CurrentIndexWeapon
void UMyTDSInventoryComponent::DropWeaponByIndex_OnServer_Implementation(int32 ByIndex)
{
	FDropItem DropItemInfo;
	// ищем на какое оружие мы переключимся (имя оружия и остаток патронов)
	FWeaponSlot EmptyWeaponSlot; 
	//Выясняем, есть ли доступное оружие в слоте
	bool bIsCanDrop = false;
	int8 i = 0;
	int8 AviableWeaponNum = 0;
	while (i < WeaponSlots.Num() && !bIsCanDrop)
	{
		if (!WeaponSlots[i].NameItem.IsNone()) //если есть оружие с именем
		{
			AviableWeaponNum++; 
			if(AviableWeaponNum > 1)
				bIsCanDrop = true; // есть доступное оружие в слоте
		}
		i++; 
	}

	//если есть оружие, на котрое можем переключиться оружие и индекс оружия, которое держим в руках есть(валиден)
	//и можем записать в нем остаток патронов
	if (bIsCanDrop && WeaponSlots.IsValidIndex(ByIndex) && GetDropItemInfoFromInventory(ByIndex, DropItemInfo))
	{
		//GetDropItemInfoFromInventory(ByIndex, DropItemInfo);//в текущем оружии записали число оставшихся патронов в обойме - грубая ошибка!!!
		// в условии мы уже вызывали эту функцию!!!

		//перключаем оружие на валидный слот от начала в массив слотов оружия
		//switch weapon to valid slot weapon from start weapon slots array
		bool bIsFindWeapon = false;
		int8 j = 1;/////////////////////////////////////////////// а не 0!!!
		while (j < WeaponSlots.Num() && !bIsFindWeapon)
		{
			if (!WeaponSlots[j].NameItem.IsNone()) //если есть оружие с именем
			{
				//вызываем ивент, что у нас поменялось оружие. Потому что, когда поменялось оружие
				//мы не хотим оставаться с голыми руками
				//Ни фига - бродкастим то, что МЫ МОЖЕМ СБРОСИТЬ ОРУЖИЕ(есть на что поменять и остаток патронов записали)
				//Меняем уже в блинринте!
				//В бродкаст приходит инфа о ПОЛУЧАЕМОМ ОРУЖИИ вместо сброшенного
				//NET
				//OnSwitchWeapon.Broadcast(WeaponSlots[j].NameItem, WeaponSlots[j].AdditionalInfo, j); //j -новый индекс оружия
				SwitchWeaponEvent_Multicast(WeaponSlots[j].NameItem, WeaponSlots[j].AdditionalInfo, j);

				bIsFindWeapon = true;
			}
			j++;
		}
// Подключаем интерфейс
		WeaponSlots[ByIndex] = EmptyWeaponSlot; //ByIndex = CurrentIndexWeapon - Обнуляем текущее оружие!!!

		// WeaponSlots[ByIndex].NameItem = "None"; //ByIndex = CurrentIndexWeapon - Обнуляем текущее оружие!!!
		// WeaponSlots[ByIndex].AdditionalInfo.Round = 0; //ByIndex = CurrentIndexWeapon - Обнуляем текущее оружие!!!


		///Запомни: UMyTDS - основной класс. IMyTDS_IGameActor - подкласс!!!
		if (GetOwner()->GetClass()->ImplementsInterface(UMyTDS_IGameActor::StaticClass()))
		{
			//DropWeaponToWorld берет GetOwner и берет то, что ему надо выкидывать! Идем в BP!
			IMyTDS_IGameActor::Execute_DropWeaponToWorld(GetOwner(), DropItemInfo);
		}
		//Бродкаст о том, что текущий слот с индексом ByIndex == CurrentIndexWeapon обнулен
		//NET
		//OnUpdateWeaponSlots.Broadcast(ByIndex, EmptyWeaponSlot); 
		UpdateWeaponSlotsEvent_Multicast(ByIndex, EmptyWeaponSlot);
	}
}


//Здесь мы должны дропать предмет: для этого - заспаунить его от владельца - мира, взять  координаты нашего персонажа 
//-владельца данного инвентаря и спихнуть это вперед с помощью этих данных
bool UMyTDSInventoryComponent::GetDropItemInfoFromInventory(int32 IndexSlot, FDropItem &DropItemInfo)

{
	bool result = false;
	//Получаем имя оружия через функцию - функция возвращает имя оружия по слоту
	FName DropItemName = GetWeaponNameBySlotIndex(IndexSlot);
	
	UMyTDSGameInstance* myGI = Cast<UMyTDSGameInstance>(GetWorld()->GetGameInstance());
	if (myGI)
	{
		//получаем структуру, в которой вся инфа об оружии. GetDropItemInfoByWeaponName - возвращает bool
		result = myGI->GetDropItemInfoByWeaponName(DropItemName, DropItemInfo);
		if (WeaponSlots.IsValidIndex(IndexSlot)) //паранойя)) Это уже проверялось выше
		{
			//чтобы в выброшенном оружии записать коррктное число оставшихся патронов в обойме
			DropItemInfo.WeaponInfo.AdditionalInfo = WeaponSlots[IndexSlot].AdditionalInfo;
			//DropItemInfo.WeaponInfo.NameItem = DropItemName;
		}		
	}
	
	return result;
}


//Офигительная функция)) - просто присвоить новые значения из PlayerState и всё!!
void UMyTDSInventoryComponent::InitInventory_OnServer_Implementation(const TArray<FWeaponSlot>& NewWeaponSlotsInfo, const TArray<FAmmoSlot>& NewAmmoSlotsInfo)
{
	//Find init weaponsSlots and First Init Weapon
	//Цикл чистит незаполненные слоты -будет их удалять и заполняет FAddicionalWeaponInfo по MaxRound
	//Потом будет удален!
	//UE_LOG(LogTemp, Warning, TEXT("WeaponSlots.Num() : %d"),  WeaponSlots.Num());

	//Перенесли из BeginPlay 16.2
	WeaponSlots = NewWeaponSlotsInfo;
	AmmoSlots = NewAmmoSlotsInfo;
	
// 	for (int8 i = 0; i < WeaponSlots.Num(); i++)
// 	{
// 		//UE_LOG(LogTemp, Warning, TEXT("WeaponSlots.Num() : %d"),  WeaponSlots[i]);
//






	//
	// MaxSlotsWeapon = WeaponSlots.Num();
	//
	// if (WeaponSlots.IsValidIndex(0))//проверяем, что если самый 1-й элемент массива (нулевой) - валидный
	// 	{
	// 	if(!WeaponSlots[0].NameItem.IsNone())//и не пустой и бродкастим. Бродкаст полетит в Character!
	// 		//здесь для самой 1-й инициализации  оружия NewCurrentIndexWeapon = 0 - TODO где применяется????
	// 		//Это для того, чтобы убрать баг - когда у нас есть оружие и мы подбираем то же самое
	// 		
	// 		//NET
	// 		//OnSwitchWeapon.Broadcast(WeaponSlots[0].NameItem, WeaponSlots[0].AdditionalInfo, 0);
	// 		SwitchWeaponEvent_Multicast(WeaponSlots[0].NameItem, WeaponSlots[0].AdditionalInfo, 0);
	// 	//Этот делегат позволяет понять, что оружие начало меняться - передаем имя и кол-во патронов
	// 	}




	
}

//Функция нужна чтобы 1 раз на бегинплей бродкастить винтовку, чтобы выделить ее зеленым цыетом))
void UMyTDSInventoryComponent::FirstInitInventory_OnServer_Implementation(int32 IndexWeapon)
{
	if (WeaponSlots.IsValidIndex(IndexWeapon))//проверяем, что если самый 1-й элемент массива (нулевой) - валидный 
		{
		if(!WeaponSlots[3].NameItem.IsNone())//и не пустой и бродкастим. Бродкаст полетит в Character!
			//здесь для самой 1-й инициализации  оружия NewCurrentIndexWeapon = 0 - TODO где применяется????
			//Это для того, чтобы убрать баг - когда у нас есть оружие и мы подбираем то же самое
			//NET
			//OnSwitchWeapon.Broadcast(WeaponSlots[0].NameItem, WeaponSlots[0].AdditionalInfo, 0);
			SwitchWeaponEvent_Multicast(WeaponSlots[IndexWeapon].NameItem, WeaponSlots[IndexWeapon].AdditionalInfo, IndexWeapon);

		//Этот делегат позволяет понять, что оружие начало меняться - передаем имя и кол-во патронов
		}
}

TArray<FWeaponSlot> UMyTDSInventoryComponent::GetWeaponSlots()
{
	return WeaponSlots;
}

TArray<FAmmoSlot> UMyTDSInventoryComponent::GetAmmoSlots()
{
	return AmmoSlots;
}

//NET Делегаты
void UMyTDSInventoryComponent::AmmoChangeEvent_Multicast_Implementation(EWeaponType TypeWeapon, int32 Cout)
{
	OnAmmoChange.Broadcast(TypeWeapon, Cout);
}

void UMyTDSInventoryComponent::SwitchWeaponEvent_Multicast_Implementation(FName WeaponName, FAdditionalWeaponInfo AdditionalInfo, int32 IndexSlot)
{
	OnSwitchWeapon.Broadcast(WeaponName,AdditionalInfo,IndexSlot);
}

void UMyTDSInventoryComponent::WeaponAdditionalInfoChangeEvent_Multicast_Implementation(int32 IndexSlot, FAdditionalWeaponInfo AdditionalInfo)
{
	OnWeaponAdditionalInfoChange.Broadcast(IndexSlot,AdditionalInfo);
}

void UMyTDSInventoryComponent::WeaponAmmoEmptyEvent_Multicast_Implementation(EWeaponType TypeWeapon)
{
	OnWeaponAmmoEmpty.Broadcast(TypeWeapon);
}

void UMyTDSInventoryComponent::WeaponAmmoAviableEvent_Multicast_Implementation(EWeaponType TypeWeapon)
{
	OnWeaponAmmoAviable.Broadcast(TypeWeapon);
}

void UMyTDSInventoryComponent::UpdateWeaponSlotsEvent_Multicast_Implementation(int32 IndexSlotChange, FWeaponSlot NewInfo)
{
	OnUpdateWeaponSlots.Broadcast(IndexSlotChange,NewInfo);
}

//НЕ ИСПОЛЬЗУЕТСЯ!!!!!!!
void UMyTDSInventoryComponent::WeaponNotHaveRoundEvent_Multicast_Implementation(int32 IndexSlotWeapon)
{
	OnWeaponNotHaveRound.Broadcast(IndexSlotWeapon);
}
//НЕ ИСПОЛЬЗУЕТСЯ!!!!!!!
void UMyTDSInventoryComponent::WeaponHaveRoundEvent_Multicast_Implementation(int32 IndexSlotWeapon)
{
	OnWeaponHaveRound.Broadcast(IndexSlotWeapon);
}


void UMyTDSInventoryComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	//Макросы
	//Говорят о том что (например) в нашем объекте в классе AWeaponDefault - AdditionalWeaponInfo должна быть реплицируемой
	DOREPLIFETIME(UMyTDSInventoryComponent, WeaponSlots);
	DOREPLIFETIME(UMyTDSInventoryComponent, AmmoSlots);
}

#pragma optimize ("", on)
