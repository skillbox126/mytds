// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MyTDS/Character/MyTDSHealthComponent.h"
#include "MyTDSCharacterHealthComponent.generated.h"

/**
 * 
 */
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnShieldChange, float, Shield, float, Damage);//Объявляем делегат и тип переменной: FOnHealthChange

//UCLASS()
UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class MYTDS_API UMyTDSCharacterHealthComponent : public UMyTDSHealthComponent
{
	GENERATED_BODY()

public:
	//Делегат на изменение щита. ОБЯЗАТЕЛЬНО: BlueprintAssignable!!!
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Health")
	FOnShieldChange OnShieldChange;
	//Таймер cool down  - остывать
	FTimerHandle TimerHandle_CoolDownShieldTimer;
	//Таймер - как часто он будет обновлять наш щит
	FTimerHandle TimerHandle_ShieldRecoveryRateTimer;
	
protected:
	//Делаем параметр щита
	float Shield =100.0f;

public:
	//Починка щита  - Repair. Меняем на Recover - восстановление
	// UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
	// float ShieldRecover = 10.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
	float CoolDownShieldRecoverTime = 5.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
	float ShieldRecoverValue = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
	float ShieldRecoverRate = 0.1f;
	
	//Оверрайдим функцию изменения значения здоровья
//	UFUNCTION(BlueprintCallable, Category = "Health") -убираем!
	void ChangeHealthValue_OnServer(float ChangeValue) override;
//Получаем текущее значение щита
//-оказывается если есть что-то в protected!!!! (- float Shield =100.0f;) -  то нужна функция для его получения
	float GetCurrentShield();
//Изменение значения щита
	void ChangeShieldValue(float ChangeValue);
//Запускает следующий тамер
	void CoolDownShieldEnd();

	void RecoveryShield();

	UFUNCTION(BlueprintCallable)
	float GetShieldValue();

	////NET
	UFUNCTION(NetMulticast, Reliable)
	void ShieldChangeEvent_Multicast(float newShield, float Damage);
};
