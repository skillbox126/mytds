// Copyright Epic Games, Inc. All Rights Reserved.

#include "MyTDSCharacter.h"
//#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "MyTDSInventoryComponent.h"
//#include "Materials/Material.h"
//#include "HeadMountedDisplayFunctionLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Engine/World.h"
#include "MyTDS/Game/MyTDSGameInstance.h"
#include "MyTDS/Weapons/WeaponDefault.h"
#include "MyTDS/MyTDS.h"
#include "Net/UnrealNetwork.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "MyTDS/Weapons/Projectiles/ProjectileDefault.h"
#include "Engine/ActorChannel.h"
// #include "MyTDS/FuncLibrary/Types.h"


AMyTDSCharacter::AMyTDSCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	//Можно не прикасаться к мыши и работать только на клавиатуре
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	//Инвентарь
	//Когда запускаем игру -Сначала отрабатывает конструктор у Чара (выше) и доходит до этого момента,
	//когда начинает инициилизироваться инвентарь и пойдет в конструктор инвентаря 
	InventoryComponent = CreateDefaultSubobject<UMyTDSInventoryComponent>(TEXT ("InventoryComponent"));
	//-потом вернется сюда! И только потом пойдет BeginPlay! ВАЖНО: 1-м отработает BeginPlay у Чара, потом у инвентаря!!! Хотя Странно!!!???? НЕТ!!!
	if (InventoryComponent)
	{//приходит сюда от бродкаста инвентаря-  о том, что оружие изменилось
		//В конструкторе можно добавлять делегаты!!! т.е. добавлять AddDynamic!
		//NET
		InventoryComponent->OnSwitchWeapon.AddDynamic(this, &AMyTDSCharacter::InitWeapon);
		//InventoryComponent->SwitchWeaponEvent_Multicast(this, &AMyTDSCharacter::InitWeapon);
	}
	
	//Компонет Здоровья
	HealthComponent = CreateDefaultSubobject<UMyTDSCharacterHealthComponent>(TEXT ("HealthComponent"));
//	HealthComponentTest = CreateDefaultSubobject<UMyTDSHealthComponent>(TEXT ("HealthComponent"));
	if (HealthComponent) //если да -умер! Странно!
	{//приходит сюда от бродкаста инвентаря-  о том, что оружие изменилось
		//В конструкторе можно добавлять делегаты!!! т.е. добавлять AddDynamic!
		HealthComponent->OnDead.AddDynamic(this, &AMyTDSCharacter::CharDead);
		//этот ивент идет через NetMulticast и будет отрабатывать везде
	}
	
	// Закоммититили стандартный курсор
	// Create a decal in the world to show the cursor's location
	// CursorToWorld = CreateDefaultSubobject<UDecalComponent>("CursorToWorld");
	// CursorToWorld->SetupAttachment(RootComponent);
	// static ConstructorHelpers::FObjectFinder<UMaterial> DecalMaterialAsset(TEXT("Material'/Game/Blueprint/Character/M_Cursor_Decal.M_Cursor_Decal'"));
	// if (DecalMaterialAsset.Succeeded())
	// {
	// 	CursorToWorld->SetDecalMaterial(DecalMaterialAsset.Object);
	// }
	// CursorToWorld->DecalSize = FVector(16.0f, 32.0f, 32.0f);
	// CursorToWorld->SetRelativeRotation(FRotator(90.0f, 0.0f, 0.0f).Quaternion());

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;

	//Network
	//не забыть -должен быть реплицирован!!! Хотя Чар по дефолту реплицирован, но каждый новый объект -обязательно!!!
	bReplicates = true;
}

void AMyTDSCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

	// if (CursorToWorld != nullptr)
	// {
	// 	if (UHeadMountedDisplayFunctionLibrary::IsHeadMountedDisplayEnabled())
	// 	{
	// 		if (UWorld* World = GetWorld())
	// 		{
	// 			FHitResult HitResult;
	// 			FCollisionQueryParams Params(NAME_None, FCollisionQueryParams::GetUnknownStatId());
	// 			FVector StartLocation = TopDownCameraComponent->GetComponentLocation();
	// 			FVector EndLocation = TopDownCameraComponent->GetComponentRotation().Vector() * 2000.0f;
	// 			Params.AddIgnoredActor(this);
	// 			World->LineTraceSingleByChannel(HitResult, StartLocation, EndLocation, ECC_Visibility, Params);
	// 			FQuat SurfaceRotation = HitResult.ImpactNormal.ToOrientationRotator().Quaternion();
	// 			CursorToWorld->SetWorldLocationAndRotation(HitResult.Location, SurfaceRotation);
	// 		}
	// 	}
	// 	else if (APlayerController* PC = Cast<APlayerController>(GetController()))
	// 	{
	// 		FHitResult TraceHitResult;
	// 		PC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
	// 		FVector CursorFV = TraceHitResult.ImpactNormal;
	// 		FRotator CursorR = CursorFV.Rotation();
	// 		CursorToWorld->SetWorldLocation(TraceHitResult.Location);
	// 		CursorToWorld->SetWorldRotation(CursorR);
	// 	}
	// }

	//Делаем новый курсор
	if(CurrentCursor) 
	{
		APlayerController* myPC = Cast<APlayerController>(GetController());
		//Добавили - если наш контроллер является локальным!
		//IsLocalController -проверяет только NetMode == NM_Standalone
		// мы подключились как клиент: (NetMode == NM_Client && GetLocalRole() == ROLE_AutonomousProxy)
		// -мы этим персонажем управляем -мы им командуем и на сервере:
		//(GetRemoteRole() != ROLE_AutonomousProxy && GetLocalRole() == ROLE_Authority)
		if (myPC && myPC->IsLocalPlayerController())
		{
			FHitResult TraceHitResult;
			myPC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
			FVector CursorFV = TraceHitResult.ImpactNormal; //устанавливаем в нужную позицию и ротацию
			FRotator CursorR = CursorFV.Rotation();

			CurrentCursor->SetWorldLocation(TraceHitResult.Location);
			CurrentCursor->SetWorldRotation(CursorR);
		}
	}
	
	MovementTick(DeltaSeconds);
}

void AMyTDSCharacter::BeginPlay()
{
	Super::BeginPlay();

//	InitWeapon(InitWeaponName, WeaponAdditionalInfo);
//	UE_LOG(LogTemp, Warning, TEXT("BeginPlay::InitWeapon - Yes"));
	// if(CurrentWeapon)
	// {
	// 	CurrentWeapon->CheckCanWeaponReload();
	// 		UE_LOG(LogTemp, Warning, TEXT("CheckCanWeaponReload(); - Yes"));
	// 	
	// }

	//Проверяем, что это не удаленный сервер -он нам по барабану
	//Когда у нас Листен сервер -мы же на сервере отображаем все данные
	if (GetWorld() && GetWorld()->GetNetMode() != NM_DedicatedServer)
	{
		//Делаем новый курсор
		//Проверяем -если есть материал для курсора -спавним: с нужным материалом и размером
		//проверяем локальную роль ROLE_AutonomousProxy - ЖИВОЙ ИГРОК
		if (CursorMaterial && GetLocalRole() == ROLE_AutonomousProxy || GetLocalRole() == ROLE_Authority)
			// Где спавнится -не важно -он подхватится по тику. И ПРОВЕРЯЕМ, ЧТО ЭТО ЖИВОЙ ИГРОК ИЛИ AUTORITY
			{
			CurrentCursor = UGameplayStatics::SpawnDecalAtLocation(GetWorld(), CursorMaterial, CursorSize, FVector(0));
			}	
	}
}

void AMyTDSCharacter::SetupPlayerInputComponent(UInputComponent* NewInputComponent)
{
	Super::SetupPlayerInputComponent(NewInputComponent);

	NewInputComponent->BindAxis(TEXT("MoveForward"), this, &AMyTDSCharacter::InputAxisX);
	NewInputComponent->BindAxis(TEXT("MoveRight"), this, &AMyTDSCharacter::InputAxisY);

	//Добавили инпуты для оружия -не забыть прописать в UE4 input FireEvent -ЛКМ
	NewInputComponent->BindAction(TEXT("ChangeToSprint"), EInputEvent::IE_Pressed, this, &AMyTDSCharacter::InputSprintPressed);
	NewInputComponent->BindAction(TEXT("ChangeToWalk"), EInputEvent::IE_Pressed, this, &AMyTDSCharacter::InputWalkPressed);
	NewInputComponent->BindAction(TEXT("AimEvent"), EInputEvent::IE_Pressed, this, &AMyTDSCharacter::InputAimPressed);
	NewInputComponent->BindAction(TEXT("ChangeToSprint"), EInputEvent::IE_Released, this, &AMyTDSCharacter::InputSprintReleased);
	NewInputComponent->BindAction(TEXT("ChangeToWalk"), EInputEvent::IE_Released, this, &AMyTDSCharacter::InputWalkReleased);
	NewInputComponent->BindAction(TEXT("AimEvent"), EInputEvent::IE_Released, this, &AMyTDSCharacter::InputAimReleased);
	NewInputComponent->BindAction(TEXT("FireEvent"), EInputEvent::IE_Pressed, this, &AMyTDSCharacter::InputAttackPressed);
	NewInputComponent->BindAction(TEXT("FireEvent"), EInputEvent::IE_Released, this, &AMyTDSCharacter::InputAttackReleased);
	NewInputComponent->BindAction(TEXT("ReloadEvent"), EInputEvent::IE_Released, this, &AMyTDSCharacter::TryReloadWeapon);

	//Инпуты для переключателя типов оружия
	NewInputComponent->BindAction(TEXT("SwitchNextWeapon"), EInputEvent::IE_Pressed, this, &AMyTDSCharacter::TrySwitchNextWeapon);
	NewInputComponent->BindAction(TEXT("SwitchPreviosWeapon"), EInputEvent::IE_Pressed, this, &AMyTDSCharacter::TrySwitchPreviosWeapon);

	//Инпут для Эффектов AbilityAction
	NewInputComponent->BindAction(TEXT("AbilityAction"), EInputEvent::IE_Pressed, this, &AMyTDSCharacter::TryAbilityEnabled_OnServer);
	//Инпут для Эффектов InvulnerabilityAction
	NewInputComponent->BindAction(TEXT("InvulnerabilityAction"), EInputEvent::IE_Pressed, this, &AMyTDSCharacter::TryInvulnerabilityEnabled_onServer);

	//Инпут для сброса оружия
	NewInputComponent->BindAction(TEXT("DropCurrentWeapon"), EInputEvent::IE_Pressed, this, &AMyTDSCharacter::DropCurrentWeapon);

	TArray<FKey> HotKeys;
    	HotKeys.Add(EKeys::One);
    	HotKeys.Add(EKeys::Two);
    	HotKeys.Add(EKeys::Three);
    	HotKeys.Add(EKeys::Four);
    	HotKeys.Add(EKeys::Five);
    	HotKeys.Add(EKeys::Six);
    	HotKeys.Add(EKeys::Seven);
    	HotKeys.Add(EKeys::Eight);
    	HotKeys.Add(EKeys::Nine);
    	HotKeys.Add(EKeys::Zero);
    
    	NewInputComponent->BindKey(HotKeys[1], IE_Pressed, this, &AMyTDSCharacter::TKeyPressed<1>);
    	NewInputComponent->BindKey(HotKeys[2], IE_Pressed, this, &AMyTDSCharacter::TKeyPressed<2>);
    	NewInputComponent->BindKey(HotKeys[3], IE_Pressed, this, &AMyTDSCharacter::TKeyPressed<3>);
    	NewInputComponent->BindKey(HotKeys[4], IE_Pressed, this, &AMyTDSCharacter::TKeyPressed<4>);
    	NewInputComponent->BindKey(HotKeys[5], IE_Pressed, this, &AMyTDSCharacter::TKeyPressed<5>);
    	NewInputComponent->BindKey(HotKeys[6], IE_Pressed, this, &AMyTDSCharacter::TKeyPressed<6>);
    	NewInputComponent->BindKey(HotKeys[7], IE_Pressed, this, &AMyTDSCharacter::TKeyPressed<7>);
    	NewInputComponent->BindKey(HotKeys[8], IE_Pressed, this, &AMyTDSCharacter::TKeyPressed<8>);
    	NewInputComponent->BindKey(HotKeys[9], IE_Pressed, this, &AMyTDSCharacter::TKeyPressed<9>);
    	NewInputComponent->BindKey(HotKeys[0], IE_Pressed, this, &AMyTDSCharacter::TKeyPressed<0>);
}


void AMyTDSCharacter::InputAxisY(float Value)
{
	AxisY = Value;
}

void AMyTDSCharacter::InputAxisX(float Value)
{
	AxisX = Value;
}

void AMyTDSCharacter::InputAttackPressed()
{
	if (HealthComponent->GetIsAlive())//к переменной нет доступа - она Protected, поэтому ф-я!
	{
		AttackCharEvent(true);
	}
}

void AMyTDSCharacter::InputAttackReleased()
{
	AttackCharEvent(false);
}

void AMyTDSCharacter::InputWalkPressed()
{
	WalkEnabled = true;
	ChangeMovementState();
}

void AMyTDSCharacter::InputWalkReleased()
{
	WalkEnabled = false;
	ChangeMovementState();
}

void AMyTDSCharacter::InputSprintPressed()
{
	SprintRunEnabled = true;
	ChangeMovementState();
}

void AMyTDSCharacter::InputSprintReleased()
{
	SprintRunEnabled = false;
	ChangeMovementState();
}

void AMyTDSCharacter::InputAimPressed()
{
	AimEnabled = true;
	ChangeMovementState();
}

void AMyTDSCharacter::InputAimReleased()
{
	AimEnabled = false;
	ChangeMovementState();
}



void AMyTDSCharacter::MovementTick(float DeltaTime)
{
	//Проверка на то, что наш перс  жив
	if (HealthComponent && HealthComponent->GetIsAlive())
	{
		// Эта логика работает на всех экземплярах - и надо от этого избавиться
	// - делаем только на локальном контроллере
		if( GetController() && GetController()->IsLocalPlayerController())
		{
			AddMovementInput(FVector(1.0f,0.0f,0.0f), AxisX);
			AddMovementInput(FVector(0.0f,1.0f,0.0f), AxisY);

		// Делаем выносливость Stamina
		BuferTime += DeltaTime;
		if (FMath::IsNearlyZero(GetVelocity().Size(), 0.5f)) //если скорость игрока = 0
			//ErrorTolerance	Максимально допустимая разница для рассмотрения Value как «почти нулевого»
		{
			if (!SprintRunEnabled) // и не нажата Shift - увеличиваем Stamina до максимума
			{
				if (BuferTime > CoefTimer && Stamina < CoefStam)
				{
					BuferTime =0.f;
					Stamina = ++Stamina;
					GEngine->AddOnScreenDebugMessage(-1, 12.f, FColor::Green, FString::Printf(TEXT("Stamina plus: %d"), Stamina));
				}
			}
		}
		else
		{
			HealthComponent->GetCurrentShield(); //вот хз зачем это здесь
			if(SprintRunEnabled) // - SprintRun_State = 800 - если нажата только Shift
			{
				//	GEngine->AddOnScreenDebugMessage(-1, 12.f, FColor::White, FString::Printf(TEXT("BuferTimme %f"), BuferTime));
				if (BuferTime > CoefTimer)
				{
					BuferTime = 0.f;

					//т.е, если стамина кончилась и у нас еще и здоровье поплохело - переходим на простой бег?
					if (Stamina <= 0 || HealthComponent->GetCurrentShield() < 100.0f)//если и текущее значение щита  меньша 100
					{
						GEngine->AddOnScreenDebugMessage(-1, 12.f, FColor::White, FString::Printf(TEXT("GetCurrentShield %f"), HealthComponent->GetCurrentShield()));
						MovementState = EMovementState::Run_State;

						//Если передавать ротацию серверу, а потом клиентам -будут дрожания
						//в BP_Caracter -Movement - надо отключить галку Current Rotation to Movement - т.к. ротация не реплицируется
							//Поэтому будем костылить свое
						FVector myRotationVector = FVector(AxisX, AxisY, 0.0f); 
						FRotator myRotator = myRotationVector.ToOrientationRotator();

						//Сначала повернем на клиенте
						SetActorRotation((FQuat(myRotator)));
						//А дальше он отправит эту инфу всем остальным, кроме нас -т.к. мы локально!
						//и мы не хотим получать инфу от сервера
						SetActorRotationByYaw_OnServer(myRotator.Yaw);


						
						GEngine->AddOnScreenDebugMessage(-1, 12.f, FColor::White, FString::Printf(TEXT("Stamina = 0: %d"), Stamina));
					//	CharacterUpdate();
					}
					else
					{
						Stamina = --Stamina;
						GEngine->AddOnScreenDebugMessage(-1, 12.f, FColor::Red, FString::Printf(TEXT("Stamina minus: %d"), Stamina));
					}
				}
			}
		}
		if (!SprintRunEnabled)
		{
			if (BuferTime > CoefTimer && Stamina < CoefStam)
			{
				BuferTime =0.f;
				Stamina = ++Stamina;
				GEngine->AddOnScreenDebugMessage(-1, 12.f, FColor::Green, FString::Printf(TEXT("Stamina plus: %d"), Stamina));
			}
		}
		if (SwitchCursorRotator == true)
			{
				//Добавим разворот нашей пешки курсором
				APlayerController* MyController = UGameplayStatics::GetPlayerController(GetWorld(),0);
				if(MyController)
				{
					FHitResult ResultHit; //Переменная для записи результата столкновения
					
					//Результат взаимодействия нашего курсора с полом, игнорируя всякие блоки и мы посылаем этот результат на оружие 
					MyController->GetHitResultUnderCursor(ECC_GameTraceChannel1, true, ResultHit);
					float FindRotationResultYaw =	UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), ResultHit.Location).Yaw;
					//ResultHit.Location -координаты, куда мы ткнули курсором

					
					//Сначала на клиенете повернём
					SetActorRotation(FQuat(FRotator(0.0f, FindRotationResultYaw, 0.0f)));
					//Кватернион с плавающей запятой, который может представлять вращение вокруг оси в трехмерном пространстве.
					
					//Потом на сервере -и он отправит инфу всем остальным -кроме нас!
					SetActorRotationByYaw_OnServer(FindRotationResultYaw);

					

					if (CurrentWeapon) //если у нас есть каое-то оружие
					{
						FVector Displacement = FVector(0);
					
						bool bIsReduceDispersion = false; //	//для NET - создали и поменяли все дальше
						switch (MovementState)
						{
						case EMovementState::Aim_State:
							Displacement = FVector(0.f,0.f,160.f);
							//CurrentWeapon->ShouldReduceDispersion = true;
							//прицелились и оружие не перезаряжается и не стреляет -  дисперсия уменьшается каждый тик на 0.1
							//, если бежим - увеличивается до максимума
							bIsReduceDispersion = true;
							break;
						case EMovementState::AimWalk_State:
							Displacement = FVector(0.f,0.f,160.f);
							//CurrentWeapon->ShouldReduceDispersion = true;
							bIsReduceDispersion = true;
							break;
						case EMovementState::Walk_State:
							Displacement = FVector(0.f,0.f,120.f);
							//CurrentWeapon->ShouldReduceDispersion = false;
							break;
						case EMovementState::Run_State:
							Displacement = FVector(0.f,0.f,120.f);
							//CurrentWeapon->ShouldReduceDispersion = false;
							break;
						case EMovementState::SprintRun_State:
							break;
						default:
							break;
						}
						
						//На каждый кадр - в зависимости от стейтов - туда , куда у нас настроен курсор
						//Когда в прицеливании или пешком -дисперсия уменьшается
						//CurrentWeapon->ShootEndLocation = ResultHit.Location + Displacement; //куда должна упасть пуля
						CurrentWeapon->UpdateWeaponByCharacterMovementState_OnServer(ResultHit.Location + Displacement, bIsReduceDispersion);
						//aim cursor like 3D Widget?
					}
				}
			}
			// if (CurrentWeapon) //если у нас есть каое-то оружие
			// {
			// 	if (FMath::IsNearlyZero(GetVelocity().Size(), 0.5f)) //и персонаж имеет какую-то скорость !=0
			// 		CurrentWeapon->ShouldReduceDispersion = true; //если =0 - сведение уменьшается
			// 	else
			//		CurrentWeapon->ShouldReduceDispersion = false; //если  !=0 сведение увеличивается - в тике оружия отрабатывает
			// }
			}
		}
	
	}


void AMyTDSCharacter::AttackCharEvent(bool bIsFiring)
{
	AWeaponDefault* myWeapon = nullptr;
	myWeapon = GetCurrentWeapon();
	if (myWeapon)
	{
		//ToDo Check melee or range
		myWeapon->SetWeaponStateFire_OnServer(bIsFiring); //Устанвливаем триггер состояния оружия: в стрельбе или нет: если бежим -нет
	}
	else
		UE_LOG(LogTemp, Warning, TEXT("ATPSCharacter::AttackCharEvent - CurrentWeapon -NULL"));
}

void AMyTDSCharacter::CharacterUpdate()
{
	float ResSpeed = 600.f;
	switch (MovementState)
	{
	case EMovementState::Aim_State:
		ResSpeed = MovementSpeedInfo.AimSpeedNormal; // - Aim_State = 300 нажата  только ЛКМ
		break;
	case EMovementState::AimWalk_State:
		ResSpeed = MovementSpeedInfo.AimSpeedWalk; // - AimWalk_State = 100 нажата  Option и ЛКМ
		break;
	case EMovementState::Walk_State:
		ResSpeed = MovementSpeedInfo.WalkSpeedNormal; // - Walk_State = 200 нажата  только Option
		break;
	case EMovementState::Run_State:
		ResSpeed = MovementSpeedInfo.RunSpeedNormal; // - RunSpeedNormal = 600 по умолчанию - Ничего не нажато
		break;
	case EMovementState::SprintRun_State:
		ResSpeed = MovementSpeedInfo.SprintRunSpeedRun; // - SprintRun_State = 800 - нажата только Shift
		break;
	default:
		break;
	}
	GetCharacterMovement()->MaxWalkSpeed = ResSpeed;
}


EMovementState AMyTDSCharacter::GetMovementState()
{
	return MovementState; 
}

UDecalComponent* AMyTDSCharacter::GetCursorToWord()
{
	return CurrentCursor; 
}


// Логика движения
void AMyTDSCharacter::ChangeMovementState()
{
	//сначала отрабатывает на стороне клиента, затем на сервере
	EMovementState NewState = EMovementState::Run_State; //Для мультиплейера
	
	if(!WalkEnabled && !SprintRunEnabled && !AimEnabled) // = RunSpeedNormal = 600 по умолчанию - Ничего не нажато
	{
		NewState = EMovementState::Run_State;
		SwitchCursorRotator = true;
	}
	else
	{
		if(SprintRunEnabled) // - SprintRun_State = 800 - нажата только Shift (SprintRunEnabled)
		{
			WalkEnabled = false;
			AimEnabled = false;
			SwitchCursorRotator = false;
			
			if (Stamina > 1)
			{
				NewState = EMovementState::SprintRun_State;
			}
		}
		else
		{
			if (WalkEnabled && !SprintRunEnabled && AimEnabled) // = 100 нажата  Option и ЛКМ (WalkEnabled и AimEnabled)
				{
				NewState = EMovementState::AimWalk_State; 
				SwitchCursorRotator = true;
				}
			else
			{
				if (WalkEnabled && !SprintRunEnabled && !AimEnabled) // - Walk_State = 200 нажата  только Option (WalkEnabled)
					{
					NewState = EMovementState::Walk_State;
					SwitchCursorRotator = true;
					}
				else
				{
					if (!WalkEnabled && !SprintRunEnabled && AimEnabled)
					{
						NewState = EMovementState::Aim_State;
						SwitchCursorRotator = true;
					}
				}
			}
		}
	}

	SetMovementstate_OnServer(NewState);
	//CharacterUpdate(); - теперь не нужен


	//GEngine->AddOnScreenDebugMessage(-1, 12.f, FColor::Blue, FString::Printf(TEXT("MovementState: %d"), MovementState));
	//Weapon state update
	//Если перосонаж начинает целиться -  Нажата Шифт, то он взял доступное ему оружие, и попытался вызвать функцию
	//UpdateStateWeapon_OnServer в которорую передается MovementState - меняем дисперсию при прицеливании!
    	AWeaponDefault* myWeapon = GetCurrentWeapon();
    	if (myWeapon)
    	{
    		myWeapon->UpdateStateWeapon_OnServer(NewState);
    	}
	//+пример -как первести ENum в строковый!!!
	FString SEnum = UEnum::GetValueAsString(NewState);
	UE_LOG(LogMyTDS_Net, Warning, TEXT("Movement state on SERVER - %s"), *SEnum);
}


//Это создано для удобства, чтобы нельзя было получать доступ к этой переменной из блюпринтов
AWeaponDefault* AMyTDSCharacter::GetCurrentWeapon()
{
	return CurrentWeapon;
}

//Инициализация оружия
//NET Работает только на сервере!
//добавляем NewCurrentIndexWeapon -для подбора оружия -говорит о том, что инициализация прошла ---не понятно  где используется?????
void AMyTDSCharacter::InitWeapon(FName IdWeaponName, FAdditionalWeaponInfo WeaponAdditionalInfo, int32 NewCurrentIndexWeapon) //To do init by ID row by table
{	//работает на обе стороны -когда просто меняем и когда игра запускается
	
	if (CurrentWeapon)//т.е.если бы было какое-то оружие ( а в начале игры его нет)
	{
		CurrentWeapon->Destroy();
		CurrentWeapon =nullptr;
	}

	//Отладка!!
	// if (GetNetMode() == NM_Client)
	// {
	// 	UE_LOG()
	// }
	
	UMyTDSGameInstance* const myGI = Cast<UMyTDSGameInstance>(GetGameInstance()); //подключаем GameInstance чтобы получить имя оружия -надо заинклудить!
	//получаем наш BP_GameInstance с таблицей оружия
	FWeaponInfo myWeaponInfo;
	
	if(myGI)
	{
		//UE_LOG(LogTemp, Warning, TEXT("UTPSGameInstance::initWeapon - Weapon myGI -Yes"));

		 if (myGI->GetWeaponInfoByName(IdWeaponName, myWeaponInfo))//если: GetWeaponInfoByName возвращает true - получаем оружие
		 {
		 	if (myWeaponInfo.WeaponClass) //Проверяем, но сначала  В BP_Character помещаем в нее BP_WeaponDefault_Riifle
		 	{
		 	FVector SpawnLocation = FVector(0);
		 	FRotator SpawnRotation = FRotator(0);
		 	FActorSpawnParameters SpawnParams; //Парметры спавна оружия
		 	SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn; // Игнорить какие либо коллизии
		 	SpawnParams.Owner = this; //GetOwner(); - Уставливаем, что нашим объектом владеет не контроллер GetOwner();, а сам Character
		 	SpawnParams.Instigator = GetInstigator();//Возвращает инициатора - подстрекателя для этого актора или nullptr, если его нет.
		 	//Сначала спавним ААктор и пытаемся его кастовать на наш новый класс AWeaponDefault
			AWeaponDefault* myWeapon = Cast<AWeaponDefault>(GetWorld()->SpawnActor(myWeaponInfo.WeaponClass, &SpawnLocation, &SpawnRotation, SpawnParams));
		 			if (myWeapon)
		 			{
		 				FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
		 				myWeapon->AttachToComponent(GetMesh(), Rule, FName("WeaponSocketRightHand")); //аттачим к нашему мешу
		 				CurrentWeapon = myWeapon;	
		 				//Инициализация оружия
		 				//Сделаем так, чтобы оружие можно было инициализировать на любом объекте
		 				myWeapon->WeaponSetting = myWeaponInfo; //хз зачем - это и так одно и тоже


		 				myWeapon->IdWeaponName = IdWeaponName; //ЭТО ЖЕСТЬ -Я ПРОПУСТИЛ!!!

		 				//при дропе оружия выяснилось - что здесь у нас лишняя инициализация MaxRound
		 				//myWeapon->AdditionalWeaponInfo.Round = myWeaponInfo.MaxRound;

		 				myWeapon->ReloadTime = myWeaponInfo.ReloadTime;

		 				myWeapon->UpdateStateWeapon_OnServer(MovementState);//наша дисперсия будет меняться на парметр из таблицы состояний перса
		 			
		 				
						//меняется динамически - на сервере и на клиенте будет одинаковое
		 				myWeapon-> AdditionalWeaponInfo = WeaponAdditionalInfo;//берется на входе, т.е. по бродкасту

		 				//При дропе оружия -когда мы дропаем например винтовку на винтовку надо это исправить:
		                //if (InventoryComponent)
		                // {
			               //   CurrentIndexWeapon = InventoryComponent->GetWeaponIndexSlotByName(IdWeaponName);
		                // }
		                	CurrentIndexWeapon = NewCurrentIndexWeapon;
		 				
		 				//Делегаты
		 				//AddDynamic -даем ссылку на себя this -кто у нас слушатель этого делегата (чар),
		 				//Когда происходит Broadcast -оружие говорит, что я перезарядился -"я говорю ВСЕМ", т.е. бродкастю
		 				//то в этот момент, тот, кто его слушает, то есть наш чар должен выполнить эту функцию
		 				myWeapon->OnWeaponReloadStart.AddDynamic(this, &AMyTDSCharacter::WeaponReloadStart);
		 				myWeapon->OnWeaponReloadEnd.AddDynamic(this, &AMyTDSCharacter::WeaponReloadEnd);
		 				
		 				myWeapon->OnWeaponFireStart.AddDynamic(this, &AMyTDSCharacter::WeaponFireStart);

		 				//после переключения на другое оружие попытаемся перезарядить оружие, если надо
		 				if(CurrentWeapon->GetWeaponRound() <= 0 && CurrentWeapon->CheckCanWeaponReload())
		 					CurrentWeapon->InitReload();
		 				
						//бродкаст о том, что мы оружие поменяли и в нем достатоно патронов
		 				if(InventoryComponent)
		 				InventoryComponent->OnWeaponAmmoAviable.Broadcast(myWeapon->WeaponSetting.WeaponType);
		 				//Странно, что мы здесь бродкастим наличие патронов! Хотя по идее это надо в инвентаре проверять
		 			} 
			}
		    else
		    {
		    	UE_LOG(LogTemp, Warning, TEXT("UMyTDSGameInstance::InitWeapon - Weapon not Found - NULL"));
		    }
		 }
	}	
}

void AMyTDSCharacter::TryReloadWeapon()
{
	if (HealthComponent && HealthComponent->GetIsAlive() && CurrentWeapon && !CurrentWeapon->WeaponReloading) //если есть оружие и он не в состоянии перезарядки
	{
		//NET  - делаем ТОЛЬКО НА СЕРВЕРЕ!
		TryReloadWeapon_OnServer();
	}
	// 		GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, TEXT("InitReload"));
}

void AMyTDSCharacter::TryReloadWeapon_OnServer_Implementation()
{
	//Если кол-во патронов в оружии меньше максимального и оружие может перезарядится, т.е. есть патроны в инвентаре
	if(CurrentWeapon->GetWeaponRound() < CurrentWeapon->WeaponSetting.MaxRound && CurrentWeapon->CheckCanWeaponReload())
		//все эти данные есть на сервере
		//Добавили CurrentWeapon->CheckCanWeaponReload() 9.3 ч.7 5'07"
		CurrentWeapon->InitReload();//перезаряжаемся
}


bool AMyTDSCharacter::GetIsAlive()
{
	bool Result = false;
	if (HealthComponent)
	{
		Result =  HealthComponent->GetIsAlive();
	}
	return Result;
}


void AMyTDSCharacter::WeaponFireStart(UAnimMontage* Animat)
{
	if (InventoryComponent && CurrentWeapon)
	{
		InventoryComponent->SetAdditionalInfoWeapon(CurrentIndexWeapon, CurrentWeapon->AdditionalWeaponInfo);
	}
	WeaponFireStart_BP(Animat);
//	PlayAnimMontage(Animat, CurrentWeapon->WeaponSetting.ReloadAnimTime, NAME_None);//анимация монтажа Anim. NAME_None -имя секции
}

void AMyTDSCharacter::WeaponFireStart_BP_Implementation(UAnimMontage* Animat)
{
	//in BP
}

void AMyTDSCharacter::WeaponReloadStart(UAnimMontage* Anim)
{
	WeaponReloadStart_BP(Anim);
//	PlayAnimMontage(Anim, CurrentWeapon->WeaponSetting.ReloadAnimTime, NAME_None);//анимация монтажа Anim. NAME_None -имя секции
}

//Функция вызывается  когда оружие перезарядилось успешно. Берем из инвентаря:
//AmmoTake -ск-ко патронов осталось
void AMyTDSCharacter::WeaponReloadEnd(bool bIsSaccess, int32 AmmoTake)
{
	if (InventoryComponent && CurrentWeapon)
	{
		InventoryComponent->AmmoSlotChangeValue(CurrentWeapon->WeaponSetting.WeaponType, AmmoTake);
		InventoryComponent->SetAdditionalInfoWeapon(CurrentIndexWeapon, CurrentWeapon->AdditionalWeaponInfo);
	}
	WeaponReloadEnd_BP(bIsSaccess);
}

void AMyTDSCharacter::WeaponReloadStart_BP_Implementation(UAnimMontage* Anim)
{
	//in BP
}

void AMyTDSCharacter::WeaponReloadEnd_BP_Implementation(bool bIsSuccess)
{
	//in BP
}

UDecalComponent* AMyTDSCharacter::GetCursorToWorld()
{
	return CurrentCursor;
}

//Функции для инпута
//ToDO in one func TrySwitchPreviosWeapon && TrySwicthNextWeapon
//need Timer to Switch with Anim, this method stupid i must know switch success for second logic inventory
//now we not have not success switch/ if 1 weapon switch to self
//функция переключения на следующее оружие
void AMyTDSCharacter::TrySwitchNextWeapon()
{
	if (CurrentWeapon && !CurrentWeapon->WeaponReloading && InventoryComponent->WeaponSlots.Num() > 1)
	{
		//Если мы имеем больше, чем одно оружие делаем свитч
		int8 OldIndex = CurrentIndexWeapon; //текущий индекс оружия
		FAdditionalWeaponInfo OldInfo; //остаток патронов в ОРУЖИИ 
		if (CurrentWeapon)
		{
			OldInfo = CurrentWeapon->AdditionalWeaponInfo; // кол-во патронов, которое осталось в ОРУЖИИ
			if (CurrentWeapon->WeaponReloading) // если не в состоянии перезарядки
					CurrentWeapon->CancelReload();//если перезаряжается WeaponReloading = true - останавливаем перезарядку
		}

		if (InventoryComponent)
		{
			//нужен таймер для переключения вместе с анимацией. Этот метод тупой -я должен знать успешное переключение для 2-го логического инвентаря
			//сейчас мы не имеем такого переключателя. Если одно оружие -переключается на себя
			if (InventoryComponent->SwitchWeaponToIndexByNextPreviosIndex(CurrentIndexWeapon + 1, OldIndex, OldInfo, true))
			{ }
		}
	}
}

//ToDO in one func TrySwitchPreviosWeapon && TrySwicthNextWeapon
//need Timer to Switch with Anim, this method stupid i must know switch success for second logic inventory
//now we not have not success switch/ if 1 weapon switch to self
void AMyTDSCharacter::TrySwitchPreviosWeapon()
{
	if (CurrentWeapon && !CurrentWeapon->WeaponReloading && InventoryComponent->WeaponSlots.Num() > 1)
	{
		//Если мы имеем больше, чем одно оружие делаем свитч
		int8 OldIndex = CurrentIndexWeapon;
		FAdditionalWeaponInfo OldInfo;
		if (CurrentWeapon)
		{
			OldInfo = CurrentWeapon->AdditionalWeaponInfo;
			if (CurrentWeapon->WeaponReloading)
				CurrentWeapon->CancelReload();
		}

		if (InventoryComponent)
		{
			//нужно знать - успешно ли мы поменяли оружие
			//нужен таймер для переключения вместе с анимацией. Этот метод тупой -я должен знать успешное переключение для 2-го логического инвентаря
			//сейчас мы не имеем такого переключателя. Если одно оружие -переключается на себя
			if (InventoryComponent->SwitchWeaponToIndexByNextPreviosIndex(CurrentIndexWeapon - 1, OldIndex, OldInfo, false))
			{ }
		}
	}
}



//Создаем эффект Aid
void AMyTDSCharacter::TryAbilityEnabled_OnServer_Implementation() //клавиша P
{
	TryAbilityEnabled_Multicast();
}

void AMyTDSCharacter::TryAbilityEnabled_Multicast_Implementation()
{
	if (AbilityEffect)//TODO CoolDown - охлаждение
		{
		// создаётся объект
		UMyTDS_StateEffect* NewEffect = NewObject<UMyTDS_StateEffect>(this, AbilityEffect);
		if (NewEffect)
		{
			NewEffect->InitObject(this,NAME_None);
		}
		}
}

//Создаем эффект Неуязвимости
void AMyTDSCharacter::TryInvulnerabilityEnabled_onServer_Implementation() //клавиша L
{
	if (InvulnerabilityEffect)
		{
		UMyTDS_StateEffect* NewEffect = NewObject<UMyTDS_StateEffect>(this, InvulnerabilityEffect);

		if (NewEffect)
			{
			NewEffect->InitObject(this, NAME_None);
			//Сначала выполняется родительский Super::InitObject и вызывается P_Fire на Чаре,
			//потом выполняется ExecuteOnce -прибавляет здоровье и уничтожается
			//и потом уже возвращается сюда
			}
		}
}

//NET - переводим на сервер!!!
//Переключаем оружие в зависимости от нажатой клавиши ToIndex = 1,2,3,4,5
void AMyTDSCharacter::TrySwitchWeaponToIndexByKeyInput_OnServer_Implementation(int32 ToIndex)
{
	bool bIsSuccess = false;
	if (CurrentWeapon && !CurrentWeapon->WeaponReloading && InventoryComponent->WeaponSlots.IsValidIndex(ToIndex))
	{
		
		if (CurrentIndexWeapon != ToIndex && InventoryComponent)
		{
			
			int32 OldIndex = CurrentIndexWeapon;
			FAdditionalWeaponInfo OldInfo;

			if (CurrentWeapon)
			{
				OldInfo = CurrentWeapon->AdditionalWeaponInfo;
				if (CurrentWeapon->WeaponReloading)
					CurrentWeapon->CancelReload();
			}

			bIsSuccess = InventoryComponent->SwitchWeaponByIndex(ToIndex, OldIndex, OldInfo);
		}
	}	
//	return bIsSuccess;
}

//дропаем текущее оружие -  когда нажимаем соотв-ю кнопку DropCurrentWeapon
void AMyTDSCharacter::DropCurrentWeapon()
{
	if(InventoryComponent)
	{
		//берется текущий индекс оружия, которое держит наш чар
		InventoryComponent->DropWeaponByIndex_OnServer(CurrentIndexWeapon);
	}
}


//Создаем имплементацию интерфейса
// bool AMyTDSCharacter::AviableForEffects_Implementation()
// {
// 	UE_LOG(LogTemp, Warning, TEXT("AMyTDSCharacter::AviableForEffects_Implementation()"));
//
// 	return true;
// }

//копируем метод из AMyTDS_EnvironmentStructure только меняем myMesh на GetMesh()
//И добавляем условие - если щит живой, то никакой эффект на него не наложится! 
EPhysicalSurface AMyTDSCharacter::GetSurfuceType()
{
		EPhysicalSurface Result = EPhysicalSurface::SurfaceType_Default;
		if (HealthComponent)
		{
			//если щит <= 0, только тогда возвращаем flash(плоть) -0-й материал
			//т.е. пока есть шит -никой эффект на него не наложится!
			if (HealthComponent->GetCurrentShield() <= 0.0f)
			{
				if (GetMesh()) //изюминка - здесь берем скелетал меш!
				{
					UMaterialInterface* myMaterial = GetMesh()->GetMaterial(0);
					if (myMaterial)
					{
						Result = myMaterial->GetPhysicalMaterial()->SurfaceType;
					}
				}
			}
		}
    	return Result;
}




//Функция для перезарядки для блюпринта чара
int32 AMyTDSCharacter::GetCurrentWeaponIndex()
{
	return CurrentIndexWeapon;
}


void AMyTDSCharacter::CharDead()
{
	//пернесли в самое начало, чтобы -когда во время игры отключался 2-1 игрок - не минусовались жизни
	CharDead_BP(); //Сделали ивент на дестрой оружия и чара и вывод виджета EndGame (когда кончились фазы)- реализация в Чаре в BP!

	if (HasAuthority())//Только на сервере эту часть!
	{
		float TimeAnim = 0.0f; //длина анимации
		//RandHelper возвращает рандомно между 0 и DeadsAnim.Num() - т.к. у нас есть 3 анимации смерти -выбираем рандомно
		int32 rnd = FMath::RandHelper(DeadsAnim.Num());
		if (DeadsAnim.IsValidIndex(rnd) && DeadsAnim[rnd] && GetMesh() && GetMesh()->GetAnimInstance())
		{
			//получем длину анимации
			TimeAnim = DeadsAnim[rnd]->GetPlayLength();
		
			//GetMesh()->GetAnimInstance()->Montage_Play(DeadsAnim[rnd]);
			//NET
			PlayAnim_Multicast(DeadsAnim[rnd]);
		}
		//Отправим событие в контроллер о смерти и том, что дальше должно произойти что-то в контроллере - сделаем это в BP
		// AMyTDSPlayerController* myPC = Cast<AMyTDSPlayerController>(GetController());
		// if (myPC)
		// {
		// 	myPC->PawnDead();
		// }
		
		// UnPossessed Позволяет выселить контроллер из пешки и все управление отберется, камера останется на месте
		//когда вызывается UnPossessed - вызывается делегат из контроллера 	virtual void OnUnPossess() override;

		//Когда Чар умирает:
		if (GetController())
		{
			GetController()->UnPossess();
		}
	
		//Надо сделать таймер на Rag Doll чтобы его потом включить
		//Когда наша анимация будет заканчиваться, будет включаться RagDoll
		//GetWorldTimerManager - позволяет создавать таймеры как в блюпринте
		//TimerHandle_RagDollTimer -создаем переменную в хедере
		//&AMyTDSCharacter::EnableRagDoll -функция, которую нам надо вызвать. EnableRagDoll - создаём!

		//Таймер должен отрабытывать 1 раз, работать для всех и находится на сервере!
		GetWorldTimerManager().SetTimer(TimerHandle_RagDollTimer,this, &AMyTDSCharacter::EnableRagDoll_Multicast, TimeAnim, false);


		//Перенесли из BP
		SetLifeSpan(20.f);
		if (GetCurrentWeapon())
		{
			GetCurrentWeapon()->SetLifeSpan(20.f);
		}

	}
	else
	{
		//Прячем курсор -когда перс умирает
		//Крашится проект!!!!
		//На клиенте есть, А у нас один из клиентов является хостом, а мв сделали так, курсора на ее
		//серверной части нет - поэтому так: на клиенте отключили -на сервере отключится автоматом
		if (GetCursorToWord())
		{
			GetCursorToWorld()->SetVisibility(false);
		}
	//Убираем возможность автоматической срельбы
	AttackCharEvent(false);
	}

	if (GetCapsuleComponent())
	{
		GetCapsuleComponent()->SetCollisionResponseToChannel(ECC_Pawn, ECR_Ignore);
	}

	//CharDead_BP(); //Сделали ивент на дестрой оружия и чара и вывод виджета EndGame (когда кончились фазы)- реализация в Чаре в BP!
}

//NET
void AMyTDSCharacter::PlayAnim_Multicast_Implementation(UAnimMontage* Anim)
{
	if (GetMesh()&&GetMesh()->GetAnimInstance())
	{
		GetMesh()->GetAnimInstance()->Montage_Play(Anim);
	}
}

void AMyTDSCharacter::EnableRagDoll_Multicast_Implementation()
{
		if (GetMesh())
		{
			GetMesh()->SetCollisionObjectType(ECC_PhysicsBody);
			GetMesh()->SetCollisionResponseToChannel(ECC_Pawn, ECR_Block);//чтобы когда бежим по трупу - его толкать
			GetMesh()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
			GetMesh()->SetSimulatePhysics(true);
		}
}

//Берем функцию у эпиков -получаем значение урона
float AMyTDSCharacter::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator,
                                  AActor* DamageCauser)
{
	//DamageAmount - Сколько урона нанести
	//DamageEvent - Пакет данных, полностью описывающий полученный ущерб.
	//EventInstigator - Контролер несет ответственность за ущерб.
	// DamageCauser -Актер, непосредственно причинивший ущерб (например, взорвавшийся снаряд, упавший на вас камень)

	//Super -для того, чтобы вся верхняя логика отработала так же!
	float ActualDamage = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
	// А УЖЕ ПОСЛЕ - ТАК!
	
	//Чтобы менять здоровье только пока наш перс живой
	if (HealthComponent && HealthComponent->GetIsAlive())
 	{
    		HealthComponent->ChangeHealthValue_OnServer(-DamageAmount);//идет с минусом!
 	}

	//К бафам и дебафам - Если дамаг идет от радиалдэмэджа, то мы попытаемся добавить эффект, если есть у гранаты нужный эффект
	//Определяем тип ивента - IsOfType - FRadialDamageEvent
	if(DamageEvent.IsOfType(FRadialDamageEvent::ClassID))
	{
		//Projectile кастуем к Актеру, непосредственно причинивший ущерб
		AProjectileDefault* myProjectile = Cast<AProjectileDefault>(DamageCauser);
		if (myProjectile)
		{
			//todo Name_None to RadialDamage
			UTypes::AddEffectBySurfaceType(this, NAME_None, myProjectile->ProjectileSetting.Effect, GetSurfuceType());
		}
	}
	return ActualDamage;
}

// //Реализация в BP в Чаре - ставим последним, когда все отработает в CharDead()
void AMyTDSCharacter::CharDead_BP_Implementation()
{
	//IN BP
}

//Эффекты
TArray<UMyTDS_StateEffect*> AMyTDSCharacter::GetAllCurrentEffects()
{
	return Effects;
}

//_Implementation: Так мы говорим движку, что помимо блюпринтовой реализации есть ещеи и в срр
void AMyTDSCharacter::RemoveEffect_Implementation(UMyTDS_StateEffect* RemoveEffect)
{
	//В момент, когда мы создаем все стейтэффекты, а мы знаем, что они создаются на сервере и их нет на клиенте,
	//соотвественно все эти AddEffect и RemoveEffect  - они существуют и отрабатывают на сервере.
	//Тогда, по идее, Можно было бы по идее вставить сюда Мультикаст с кодом из SwitchEffect.
	//т.е. мы передаем новый эффект, который создался, передаем RemoveEffect, получаем новые данные, вытягиваем
	//оттуда новый эффект и в виде мультикаста спауним нужный эффект на чаре, либо на структурках либо потом на врагах.
	//Но!!! В момент, когда объект появлятся на сервере, даже если мы будем этот объект реплицировать,
	//он еще не успеет на клиенте создасться, а мы уже будем требовать от него мультикаст. 
	Effects.Remove(RemoveEffect);

	//Мы спрашиваем у эффекта, который мы уничтожаем - он является самоуничтожающимся?
	if (!RemoveEffect->bIsAutoDestroyParticleEffect)//если не бесконечный -внутрь ф-ии
	{
		//Поэтому вызываем SwitchEffect 
		SwitchEffect(RemoveEffect, false);
		//Это происходит только на сервере. И дальше, поскольку знаем, что RepNotify работает только на клиенте
		//Если мы поменяем эту переменную на новую:
		EffectRemove = RemoveEffect; //EffectRemove - RepNotify!
		//соответственно отработает нам нужный RepNotify
	}
}


void AMyTDSCharacter::AddEffect_Implementation(UMyTDS_StateEffect* newEffect)
{
	//Добавляем ээфект в массив -  их будет больше одного c каждым выстрелом
	Effects.Add(newEffect);
//делаем для тех эффектов -которые не бесконечные, (как огонь!) для них bIsAutoDestroyParticleEffect = true
	if (!newEffect->bIsAutoDestroyParticleEffect)
	{
		//старая логика для огня
		//Поэтому вызываем SwitchEffect
		SwitchEffect(newEffect, true); 
		//Это происходит только на сервере. И дальше, поскольку знаем, что RepNotify работает только на клиенте
		//Если мы поменяем эту переменную на новую:
		//В момент вызова этой переменной запускается RepNotify ф-я EffectAdd_OnRep() на клиенте
		//Самое главное волшебство:
		EffectAdd = newEffect;//EffectAdd - RepNotify!
	}
	else //для небесконечных эффектов
	{
		if (newEffect->ParticleEffect)
		{
			ExecuteEffectAdded_OnServer(newEffect->ParticleEffect);
		}
	}
}

void AMyTDSCharacter::ExecuteEffectAdded_OnServer_Implementation(UParticleSystem* ExecuteFX)
{
	//Передаётся только 1 вызов -какой эффект надо заспаунить
	ExecuteEffectAdded_Multicast(ExecuteFX);
}

void AMyTDSCharacter::ExecuteEffectAdded_Multicast_Implementation(UParticleSystem* ExecuteFX)
{
	//Здесь функция выполняется только на клиентских машинах: определяем - какой эффект, объект итд
	UTypes::ExecuteEffectAdded(ExecuteFX, this, FVector(0), FName ("Spine_01"));
}

//РепНотифай
void AMyTDSCharacter::EffectAdd_OnRep()
{
	//Мы уже знаем, что в RepNotify EffectAdd корректно появлась, обновилась и эта функция отработает только тогда,
	//когда на клиенте эта EffectAdd поменяется
	if (EffectAdd)
	{
		SwitchEffect(EffectAdd, true);
	}
}

void AMyTDSCharacter::EffectRemove_OnRep()
{
	if (EffectRemove)
	{
		SwitchEffect(EffectRemove, false);
	}
}


void AMyTDSCharacter::SwitchEffect(UMyTDS_StateEffect* Effect, bool bIsAdd)
{
	//Хотел сделать массив партиклов - но они рандомно выбираются разные на сервере и на клиенте. Вернул обратно
	//UParticleSystem* ParticleCurrentEffect = Effect->ParticleEffect[FMath::RandRange(0, (Effect->ParticleEffect.Num()-1))];
	//Это можно исправить, но дело не в этом! У нас разделение логики на конечные и бесконечные эффекты.
	//Если будет массив -надо будет искать туда только бесконечные - нафиг!

	if (bIsAdd)
	{
		if (Effect && Effect->ParticleEffect)
		{
			FName NameBoneToAttached = Effect->NameBone; //прописываем -куда мы хотим прицепить эффект
			FVector Loc = FVector(0); //и локацию
			USkeletalMeshComponent* myMesh = GetMesh();
			if (myMesh)
			{
				//спавним
				UParticleSystemComponent* NewParticleSystem = UGameplayStatics::SpawnEmitterAttached
					(
					Effect->ParticleEffect,
					myMesh,
					NameBoneToAttached,
					Loc,
					FRotator::ZeroRotator,
					EAttachLocation::SnapToTarget,
					false
					);
				//Добавляем заспавненный UParticleSystemComponent в массив компонентов
				ParticleSystemEffects.Add(NewParticleSystem);
			}
		}
	}
	else //теперь удаляем эффекты. Если бы был спец по эффектам, который был сделал эффекты по времени,
			//это бы вообще не было нужно, но, т.к. наш огонь горит постоянно, эффект надо удалять.
			//Для этого ввели bIsAdd
	{
		if (Effect && Effect->ParticleEffect)
		{
			int32 i = 0;
			bool bIsFind = false;
			if (ParticleSystemEffects.Num() > 0) 
			{
				//пробегаемся по массиву партиклов-систем-компонентов 
				while (i < ParticleSystemEffects.Num() && !bIsFind)
				{
					//Когда мы назначаем какой-то эффект мы как раз назаначаем переменную Template
					if (ParticleSystemEffects[i] && ParticleSystemEffects[i]->Template
							&& Effect->ParticleEffect && Effect->ParticleEffect == ParticleSystemEffects[i]->Template)
					{
						bIsFind = true;
						ParticleSystemEffects[i]->DeactivateSystem();
						ParticleSystemEffects[i]->DestroyComponent();
						ParticleSystemEffects.RemoveAt(i);
					}
				i++;
				}
			}			
		}
	}
}



void AMyTDSCharacter::SetActorRotationByYaw_OnServer_Implementation(float Yaw)
{
	SetActorRotationByYaw_Multicast(Yaw);
}
void AMyTDSCharacter::SetActorRotationByYaw_Multicast_Implementation(float Yaw)
{
	//А дальше он отправит эту инфу всем остальным, кроме нас -т.к. мы локально!
	//и мы не хотим получать инфу от сервера
	//Сделали исключение для нашей игры -ротейшн нашего клиента является ИСТИНОЙ!
	if (Controller && !Controller->IsLocalPlayerController()) //если НЕ локальный
	{
		//рабиваем на кватринион
		SetActorRotation(FQuat(FRotator(0.0f, Yaw, 0.0f)));
	}
}



void AMyTDSCharacter::SetMovementstate_OnServer_Implementation(EMovementState NewState)
{
	SetMovementstate_Multicast(NewState);
}
void AMyTDSCharacter::SetMovementstate_Multicast_Implementation(EMovementState NewState)
{
	MovementState = NewState;
	//в момент когда на стороне клиента всё уже поменяется, мы нажали правую кнопку мыши. либо Шифт, либо Альт
	//Поменяли состояние - он пошел на сервер, отработал мулькаст и уже на каждом клиенте, на каждой копии
	//у нас отработает CharacterUpdate
	CharacterUpdate(); 
}

//Позволяет компоненту реплицировать другой подобъект на актере
bool AMyTDSCharacter::ReplicateSubobjects(UActorChannel* Channel, FOutBunch* Bunch, FReplicationFlags* RepFlags)
{
	//Wrote - это битовое сравнение
	bool Wrote = Super::ReplicateSubobjects(Channel,Bunch,RepFlags);

	for (int32 i = 0; i < Effects.Num(); i++)
	{
		if (Effects[i])
		{
			Wrote |= Channel->ReplicateSubobject(Effects[i], *Bunch, *RepFlags);
		}
	}
	return Wrote;
}

//Для UPROPERTY(Replicated)
void AMyTDSCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	//Макросы
	//Говорят о том что (например) в нашем объекте в классе AMyTDSCharacter - MovementState должна быть реплицируемой
	DOREPLIFETIME(AMyTDSCharacter, MovementState);
	DOREPLIFETIME(AMyTDSCharacter, CurrentWeapon);
	DOREPLIFETIME(AMyTDSCharacter, CurrentIndexWeapon);
	DOREPLIFETIME(AMyTDSCharacter, Effects);
	DOREPLIFETIME(AMyTDSCharacter, EffectAdd);
	DOREPLIFETIME(AMyTDSCharacter, EffectRemove);
}



